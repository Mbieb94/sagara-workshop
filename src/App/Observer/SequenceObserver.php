<?php

namespace App\Observer;

use Norm\Norm;

class SequenceObserver {
    protected $options = array();
    protected $collection;

    public function __construct($options = array()) {
        $this->options = $options;
    }

    public function saving($model) {
        $table = $model->getClass();
        if (!$model['$id']) {
            $model['code'] = $this->generateNumber($table);
        }
    }

    public function generateNumber($table){
        $currentData = Norm::factory('Sequence');
        $check = $currentData->findOne(array('table_name' => $table));

        if ($check) {
            $number = $check['sequence'] + 1;
            $check->set('sequence', $number);
            $check->save();
        } else {
            $number = 1;
            $collection = Norm::factory('Sequence')->newInstance();
            $collection->set('table_name', $table);
            $collection->set('year', date('Y'));
            $collection->set('sequence', $number);
            $collection->save();
        }

        $pieces = preg_split('/(?=[A-Z])/',$table);

        $string = "W";
        // for($i = 0; $i < count($pieces); $i++) {
        //     if (!empty($pieces[$i])) {
        //         $string .= substr($pieces[$i], 0, 1);
        //     }
        // }

        return $string.'-'.str_pad($number, 3, '0', STR_PAD_LEFT);
    }

}
