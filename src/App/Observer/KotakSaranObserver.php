<?php 
namespace App\Observer;

Class KotakSaranObserver {

    protected $absensi;

    public function saving($model){
        if(!$model['$id']) {
            $model['pengirim'] = $_SESSION['user']['$id'];
        }
        
        return $model;
    }
}