<?php 
namespace App\Observer;

Class AbsensiObserver {

    protected $absensi;

    public function saving($model){
        if($model['$id']) {
            $this->absensi = $model['data'];
            unset($model['data']);
        }
        
        return $model;
    }

    public function saved($model){
        if(!empty($this->absensi)) {
            $this->removeAbsensi($model['$id']);
            $this->saveAbsensi($this->absensi, $model);
        }
    }

    private function removeAbsensi($id_absensi) {
        $absensi = \Norm::factory('AbsensiParticipants')->find(array('id_absensi' => $id_absensi));
        if($absensi->count() > 0) {
            foreach ($absensi as $key => $value) {
                $value->remove();
            }
        }
    }

    private function saveAbsensi ($data, $model) {
        foreach ($data as $key => $value) {
            $absensi = \Norm::factory('AbsensiParticipants')->newInstance();
            $absensi->set('id_absensi', $model['$id']);
            $absensi->set('periode', $model['periode']);
            $absensi->set($value);
            $absensi->save();
        }
    }
}