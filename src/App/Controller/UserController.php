<?php

namespace App\Controller;

use \Norm\Controller\NormController;
use Norm\Norm;

class UserController extends AppController
{
    public function update($id)
    {   
        if ($_SESSION['user']['$id'] == 5) {
            try {
                $entry = $this->collection->findOne($id);
            } catch (Exception $e) {
                // noop
            }
        } else {
            if ($id != $_SESSION['user']['$id']) {
                h('notification.info', ' Sorry cannot access other user');
                return $this->app->redirect('user');
            }
            try {
                $entry = $this->collection->findOne($id);
            } catch (Exception $e) {
                // noop
            }
        }

        if (is_null($entry)) {
            return $this->app->notFound();
        }

        if ($this->request->isPost() || $this->request->isPut()) {
            try {
                $merged = array_merge(
                    isset($entry) ? $entry->dump() : array(),
                    $this->request->getBody() ?: array()
                );

                $entry->set($merged)->save();

                h('notification.info', $this->clazz.' updated');

                h('controller.update.success', array(
                    'model' => $entry,
                ));
            } catch (Stop $e) {
                throw $e;
            } catch (Exception $e) {
                h('notification.error', $e);

                if (empty($entry)) {
                    $model = null;
                }

                h('controller.update.error', array(
                    'error' => $e,
                    'model' => $entry,
                ));
            }
        }

        $this->data['entry'] = $entry;
    }

}