<?php

namespace App\Controller;

use \Norm\Controller\NormController;
use Norm\Norm;

class TableJadwalController extends AppController
{
    public function mapRoute(){
        parent::mapRoute();
        $this->map('/change_title/:id', 'changeTitle')->via('GET', 'POST');
        $this->map('/random_peserta/:periode', 'random_peserta')->via('GET', 'POST');
    }

    public function create()
    {
        $entry = $this->collection->newInstance()->set($this->getCriteria());

        $this->data['entry'] = $entry;

        if ($this->request->isPost()) {
            try {
                $post = $this->request->getBody();
                $cek = $this->cekurUtan($post['tahun'], $post['periode']);
                $post['no_urut'] = $cek;

                $cek = Norm::factory('TableJadwal')->findOne(array('nama' => $post['nama'], 'tahun' => $post['tahun'], 'periode' => $post['periode']));
                if (!empty($cek)) {
                    h('notification.error', 'maaf data sudah ada');
                    return false;
                } else {
                    $entry->set($post)->save();
                    h('notification.info', $this->clazz.' created.');
                    h('controller.create.success', array(
                        'model' => $entry
                    ));
                }
            } catch (Stop $e) {
                throw $e;
            } catch (Exception $e) {
                // no more set notification.error since notificationmiddleware will
                // write this later
                // h('notification.error', $e);

                h('controller.create.error', array(
                    'model' => $entry,
                    'error' => $e,
                ));

                // rethrow error to make sure notificationmiddleware know what todo
                throw $e;
            }
        }

    }

	public function update($id)
    {   
        try {
            $entry = $this->collection->findOne($id);
            $data = $this->sqlQuery("
                SELECT tj.no_urut AS no, CONCAT(u.first_name,' ', u.last_name) AS nama FROM table_jadwal tj
                INNER JOIN user u ON u.id = tj.nama
                WHERE tj.periode = '".$entry['periode']."'
                ORDER BY tj.no_urut
            ");
            
            if ($_SESSION['user']['$id'] != 5) {
                if ($entry['nama'] != $_SESSION['user']['$id']) {
                    h('notification.info', ' Sorry cannot access other user');
                    return $this->app->redirect('table_jadwal');
                }
            }
        } catch (Exception $e) {
            // noop
        }

        if (is_null($entry)) {
            return $this->app->notFound();
        }

        if ($this->request->isPost() || $this->request->isPut()) {
            try {
                $merged = array_merge(
                    isset($entry) ? $entry->dump() : array(),
                    $this->request->getBody() ?: array()
                );

                if ($merged['no_urut'] == '') {
                    $merged['no_urut'] = $merged['x_urut'];
                }
                
                if ($merged['x_urut'] != $merged['no_urut']) {
                    $this->gantiUrutan($merged['no_urut'], $merged['x_urut'], $merged['periode']);
                }
                unset($merged['x_urut']);
                $entry->set($merged)->save();

                h('notification.info', $this->clazz.' updated');

                h('controller.update.success', array(
                    'model' => $entry,
                ));
            } catch (Stop $e) {
                throw $e;
            } catch (Exception $e) {
                h('notification.error', $e);

                if (empty($entry)) {
                    $model = null;
                }

                h('controller.update.error', array(
                    'error' => $e,
                    'model' => $entry,
                ));
            }
        }
        $this->data['data'] = $data;
        $this->data['entry'] = $entry;
    }

    public function search()
    {
        $entry = $this->collection->newInstance()->set($this->getCriteria());

        $this->data['entry'] = $entry;
    }

    public function changeTitle($id) {
        $collection = $this->collection->findOne($id);

        if($_SESSION['user']['role'][0] != 5 && $collection['nama'] != $_SESSION['user']['$id']) {
            $this->data['status'] = 401;
            $this->data['message'] = 'Unautorize !';
            $this->data['text'] = "You're not allowed to change title for other participant";
            return;
        }

        if (is_null($collection)) {
            $this->data['status'] = 404;
            $this->data['message'] = 'Not found !';
            $this->data['text'] = 'Model not found';
            return;
        }

        if ($this->request->isPost() || $this->request->isPut()) {
            try {
                $post = $this->request->post();
                $collection->set($post)->save();
            } catch (Stop $e) {
                //throw $th;
            } catch (Exception $e) {
                $this->data['status'] = 500;
                $this->data['message'] = 'Bad request !';
                $this->data['text'] = $e;
                return;
            }
        }

        $this->data['status'] = 200;
        $this->data['message'] = 'Success !';
        $this->data['text'] = 'Title has changed';
        return;
    }

    public function random_peserta($periode)
    {
        $periodeWorkshop = Norm::factory('Periode')->findOne(array('code' => $periode));
        
        if($periodeWorkshop['workshop_status'] == 2) {
            $this->data['pesan'] = "Periode '".$periodeWorkshop['name']."' sudah selesai";   
            $this->data['status'] = 201;
            return true;
        }

        $dataExist = Norm::factory('TableJadwal')->find(array('periode' => $periode, 'status' => 1));
        $dataUser = Norm::factory('User')->find(array('status' => 1, 'id!ne' => 5));

        if($dataExist->count() == $dataUser->count()) {
            $this->data['pesan'] = "Pengundian Selesai !";   
            $this->data['status'] = 202;
            return true;   
        }

        $dataSelected = "AND us.id NOT IN (5";
        if($dataExist->count() > 0) {
            $counter = 1;
            foreach($dataExist as $key => $value) {
                if($counter != $dataExist->count()) $dataSelected .= ',';
                $dataSelected .= $value['nama'];
            }
        }
        $dataSelected .= ") ";

        $query = "SELECT * FROM user AS us WHERE us.status = 1 $dataSelected ORDER BY RAND() LIMIT 1";
        $user = $this->rowsArray($query);
        
        $data = array(
            'no_urut' => $this->cekUrutan($periode),
            'nama' => $user[0]['id'],
            'tahun' => date('Y'),
            'periode' => $periode
        );

        $userPhoto = $user[0]['photo'];
        $user[0]['photo'] = \Theme::base('images/no-image.png');
        if(!empty($userPhoto) && $userPhoto != '[]') {
            $photo = json_decode($userPhoto);
            $dir = dirname(dirname(dirname(__DIR__))) . '/www/' . $photo->bucket .'/'. $photo->filename;
            if(file_exists($dir)) {
                $user[0]['photo'] = \Theme::base($photo->bucket .'/'. $photo->filename);
            }
        }

        $newSchedule = Norm::factory('TableJadwal')->newInstance();
        $newSchedule->set($data);
        $newSchedule->save();
        
        $this->data['data'] = $user[0];   
        $this->data['pesan'] = 'sukses';   
        $this->data['status'] = 200;   
    }

    private function cekUrutan ($periode) {
        $cek = $this->sqlQuery("SELECT tj.no_urut FROM table_jadwal tj WHERE tj.status = 1 AND tj.periode = '$periode' ORDER BY tj.no_urut DESC LIMIT 1");
        if (!empty($cek)) {
            $no = $cek[0]['no_urut'] + 1;
        } else {
            $no = 1;
        }
        return (int) $no;
    }

    private function gantiUrutan ($findId, $changeId, $periode) {
        $findData = Norm::factory('TableJadwal')->findOne(array('no_urut' => $findId, 'periode' => $periode));
        if(!empty($findData)) {
            $findData->set('no_urut', (int) $changeId);
            $findData->save();
        }
    }

    private function sqlQuery ($sql) {
        $connection = \Norm::getConnection('mysql')->getRaw();
        $statement = $connection->prepare($sql);

        if (empty($params)) {
            $statement->execute();
        } else {
            $statement->execute($params);
        }

        $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $results;
    }

}