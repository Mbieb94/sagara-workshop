<?php

namespace App\Controller;

use \Norm\Controller\NormController;
use Norm\Norm;

class TableKeuanganController extends AppController
{
	public function create()
    {
        $entry = $this->collection->newInstance()->set($this->getCriteria());

        $last = $this->sqlQuery("SELECT tk.saldo FROM table_keuangan tk ORDER BY tk.id DESC LIMIT 1");

        $this->data['saldo'] = $last[0]['saldo'];
        $this->data['entry'] = $entry;

        if ($this->request->isPost()) {
            try {
                $post = $this->request->getBody();

                $entry->set($post)->save();

                h('notification.info', $this->clazz.' created.');

                h('controller.create.success', array(
                    'model' => $entry
                ));
            } catch (Stop $e) {
                throw $e;
            } catch (Exception $e) {
                // no more set notification.error since notificationmiddleware will
                // write this later
                // h('notification.error', $e);

                h('controller.create.error', array(
                    'model' => $entry,
                    'error' => $e,
                ));

                // rethrow error to make sure notificationmiddleware know what todo
                throw $e;
            }
        }
    }

    private function sqlQuery ($sql) {
        $connection = \Norm::getConnection('mysql')->getRaw();
        $statement = $connection->prepare($sql);

        if (empty($params)) {
            $statement->execute();
        } else {
            $statement->execute($params);
        }

        $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $results;
    }

}