<?php

namespace App\Controller;

use \Norm\Controller\NormController;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class AppController extends NormController
{
    // public function __construct($request) {
        // echo '<pre>';
        // print_r($request->session);
        // exit();
    // }

    public function mapRoute(){
        parent::mapRoute();
        $this->map('/null/export_csv', 'export_csv')->via('GET', 'POST');
        $this->map('/:id/trash', 'trash')->via('GET', 'POST');
        $this->map('/:id/rollback', 'rollback')->via('GET', 'POST');
        $this->map('/null/data_table', 'dataTable')->via('GET', 'POST');
        $this->map('/null/trashed', 'trashed')->via('GET', 'POST');
        $this->map('/null/export_excel', 'exportExcel')->via('GET', 'POST');
    }

    public function getLimitExport(){
        $limit = $this->request->get('!limit');

        return $limit;
    }

    public function dataTable () {
        $schemas =  $this->collection->schema();
        $request = $this->searchRequest();
        $advance = $this->request->get('param');
        $status = $this->request->get('status');
        $param = array();
        if(!empty($advance)) {
            foreach($advance as $key => $value) {
                if(!empty($value)) {
                    $param[$key] = $value;
                }
            }
        }
        
        $criteria = array_merge($param, $request, array('status' => $status));

        $collection = $this->collection->find($criteria);
        $totalSearch = $collection->count();
        $collection->match($this->getMatch());
        $collection->sort($this->orderRequest());
        $collection->skip($this->request->get('start'));
        $collection->limit($this->request->get('length'));

        $schema = [];
        foreach ($schemas as $key => $field) {
            if($field->get('list-column')) {
                $schema[$key] = $field;
            }
        }

        $data = [];
        foreach ($collection as $key => $entry) {
            $data[$key][] = $entry->getId();
            foreach ($schema as $name => $field) {
                try { 
                    $str = $entry->format($name);
                    if (!(strcmp( $str, strip_tags($str) ) == 0)) {
                        $data[$key][] = $str;
                    } else {
                        $string = strip_tags($entry->format($name)); 
                        $data[$key][] = substr($string, 0, 48);
                    }
                } catch(\Exception $e) {
                    $data[$key][] = '-';
                }
            }
            $data[$key][] = '';
        }

        $draw = 1;
        if (!empty($this->request->get('draw'))) {
            $draw = $this->request->get('draw');
        }
        
        $this->data['draw'] = $draw;
        $this->data['recordsTotal'] = $totalSearch;
        $this->data['recordsFiltered'] = $totalSearch;
        $this->data['data'] = $data;
    }

    public function getSearch(){

        $search = $this->request->get('!search');

        if($search){
            $schema =  $this->collection->schema();
            $dtsearch = array();
            foreach ($schema as $value) {
                if($value->get('name')[0] !='$'){
                    if($value->get('list-column')){
                        $dtsearch[] = array($value->get('name').'!like' => $search);
                    }
                }
            }

            $search = array('!or' => $dtsearch);

        }else{
            $search = array();
        }

        return $search;
    }

    public function searchRequest(){

        $search = $this->request->get('search');

        if($search){
            $schema =  $this->collection->schema();
            $dtsearch = array();
            foreach ($schema as $value) {
                if($value->get('name')[0] !='$'){
                    if($value->get('list-column')){
                        $dtsearch[] = array($value->get('name').'!like' => $search);
                    }
                }
            }

            $search = array('!or' => $dtsearch);

        }else{
            $search = array();
        }

        return $search;
    }

    public function orderRequest() {
        $schema = $this->collection->schema();
        $request = $this->request->get('order');
        $field = $this->generateField($schema);
        $merged = array_merge(array('id'), array_keys($field));
        
        $sort = 1;
        if($request[0]['dir'] == 'desc') {
            $sort = -1;
        }

        $requested = array($merged[$request[0]['column']] => $sort);
        return $requested;
    }

    public function getSort(){
        $sort = parent::getSort();
        if(empty($sort)){
            $sort['$created_time'] = -1;
        }
        return $sort;
    }

    public function getCriteria(){
        $criteria = parent::getCriteria();
        
        if (empty($criteria)) {
            $criteria['status'] = '1';
        }

        $criteria = array_merge($criteria,$this->getSearch());

        return $criteria;
    }

    public function export_csv(){

        $entries = $this->collection
                    ->find($this->getCriteria())
                    ->limit($this->getLimitExport())
                    ->skip($this->getSkip())
                    ->sort($this->getSort());

        $schema = $this->collection->schema();
        $field = $this->generateField($schema);
        $f = fopen('php://memory', 'w'); 

        fputcsv($f,array_values($field),',');
        
        foreach ($entries as $key => $value) {
            fputcsv($f,$this->generateRow($value,$field),',');
        }

        fseek($f, 0);

        header('Content-Type: application/csv');
        // tell the browser we want to save it instead of displaying it
        header('Content-Disposition: attachment; filename="'.$this->clazz.'.csv";');
        fpassthru($f);

        exit();
        

    }

    private function generateField($schema){

        $field = array();

        foreach ($schema as $key => $value) {
            if ($value['list-column']) {
                $field[$key]=str_replace('$','',str_replace('*','',$value->label(true)));
            }
        }

        return $field;
    }

    private function generateRow($model,$field){
        $row = array();
        foreach ($field as $name => $value) {
            try{
                if($model->format($name) instanceof \Norm\Type\DateTime){
                    $row[] = date('Y-m-d H:i:s' ,strtotime($model->format($name)->__toString()));
                    continue;
                }
                $row[] = $model->format($name);
            }catch(\Exception $e){
                $row[] ='';
            }
        }
        return $row;
    }

    public function search()
    {
        $criteria = $this->getCriteria();
        $entry = $this->collection->newInstance()->set($this->getCriteria());

        // $criteria['status'] = '1';
        
        // $entries = $this->collection->find($criteria)
        // ->match($this->getMatch())
        // ->sort($this->getSort())
        // ->skip($this->getSkip())
        // ->limit($this->getLimit());
        
        // $this->data['entries'] = $entries;
        $this->data['entry'] = $entry;
    }

    public function trashed()
    {
        $criteria = $this->getCriteria();
        $entry = $this->collection->newInstance()->set($this->getCriteria());
        $this->data['entry'] = $entry;
    }

    public function create()
    {
        $entry = $this->collection->newInstance()->set($this->getCriteria());

        $this->data['entry'] = $entry;

        if ($this->request->isPost()) {
            try {
                $body = $this->request->getBody();
                $body['status'] = '1';
                
                $result = $entry->set($body)->save();

                h('notification.info', $this->clazz.' created.');

                h('controller.create.success', array(
                    'model' => $entry
                ));
            } catch (Stop $e) {
                throw $e;
            } catch (Exception $e) {
                h('controller.create.error', array(
                    'model' => $entry,
                    'error' => $e,
                ));

                // rethrow error to make sure notificationmiddleware know what todo
                throw $e;
            }
        }
    }

    public function trash($id)
    {
        $id = explode(',', $id);

        if ($this->request->isPost() || $this->request->isDelete()) {
            $single = false;
            if (count($id) === 1) {
                $single = true;
            }

            try {
                $this->data['entries'] = array();

                foreach ($id as $value) {
                    $model = $this->collection->findOne($value);

                    if (is_null($model)) {
                        if ($single) {
                            $this->app->notFound();
                        }

                        continue;
                    }

                    $model->set(['status' => '0'])->save(['filter' => false]);

                    $this->data['entries'][] = $model;
                }

                h('notification.info', $this->clazz.' deleted.');

                h('controller.delete.success', array(
                    'models' => $this->data['entries'],
                ));

            } catch (Stop $e) {
                throw $e;
            } catch (Exception $e) {
                h('notification.error', $e);

                if (empty($model)) {
                    $model = null;
                }

                h('controller.delete.error', array(
                    'error' => $e,
                    'model' => $model,
                ));
            }

        }
    }

    public function rollback($id)
    {
        $id = explode(',', $id);

        if ($this->request->isPost() || $this->request->isDelete()) {
            $single = false;
            if (count($id) === 1) {
                $single = true;
            }

            try {
                $this->data['entries'] = array();

                foreach ($id as $value) {
                    $model = $this->collection->findOne($value);

                    if (is_null($model)) {
                        if ($single) {
                            $this->app->notFound();
                        }

                        continue;
                    }

                    $model->set(['status' => '1'])->save(['filter' => false]);

                    $this->data['entries'][] = $model;
                }
                exit();

            } catch (Stop $e) {
                throw $e;
            } catch (Exception $e) {
                h('notification.error', $e);

                if (empty($model)) {
                    $model = null;
                }

                h('controller.delete.error', array(
                    'error' => $e,
                    'model' => $model,
                ));
            }

        }
    }

    public function rowsArray($sql, $params = array())
    {
        $connection = \Norm::getConnection('mysql')->getRaw();
        $statement = $connection->prepare($sql);

        if (empty($params)) {
            $statement->execute();
        } else {
            $statement->execute($params);
        }

        $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $results;
    }
    
    public function exportExcel ()
    {
        $schemas =  $this->collection->schema();
        $request = $this->searchRequest();
        $advance = $this->request->get('param');
        $status = $this->request->get('status');
        $param = array();
        if(!empty($advance)) {
            foreach($advance as $key => $value) {
                if(!empty($value)) {
                    $param[$key] = $value;
                }
            }
        }
        
        $criteria = array_merge($param, $request, array('status' => $status));

        $collection = $this->collection->find($criteria);
        $collection->sort($this->orderRequest());

        if($collection->count() <= 0) {
            $this->data['status'] = 201;
            $this->data['message'] = 'Empty';
            $this->data['text'] = 'No rows to export';
            return;
        }

        $schema = [];
        foreach ($schemas as $key => $field) {
            if($field->get('list-column')) {
                $schema[$key] = $field;
            }
        }

        $field = $this->generateField($schemas);

        $index = array_keys($field);
        $header = array_values($field);

        $alfa = array();
        for ($i=0; $i < count($header) ; $i++) { 
            $alfa[] = $this->getAlfabet($i);
        }

        $spreadsheet = new Spreadsheet();
        foreach ($header as $key => $value) {
            $spreadsheet->getActiveSheet()->getColumnDimension($alfa[$key])->setAutoSize(true);
            $spreadsheet->getActiveSheet()->SetCellValue($alfa[$key].'1', $value);
        }

        $count = 2;
        foreach ($collection as $value) {
            $i = 0;
            foreach ($schema as $name => $field) {
                try {
                    $val = $field->format('plain', $value[$name], $value);
                    $spreadsheet->getActiveSheet()->SetCellValue($alfa[$i].$count, $val);
                } catch (\Exception $e) {
                    $spreadsheet->getActiveSheet()->SetCellValue($alfa[$i].$count, 'xxx');
                }
                $i++;
            }
            $count ++;
        }

        $writer = new Xlsx($spreadsheet);
        $path = dirname(dirname(dirname(__DIR__))).'/www/data/'.$this->collection->getClass();
        if (!file_exists($path)) {
            mkdir($path, 755, true);
		}
        $directory = $path.'/'.$this->collection->getName().'-'.date('Y-m-d').'.xlsx';
        
        $writer->save($directory);

        $status = 404;
        $pesan = "Not Found";
        $text = "File Not Found : " . \Theme::base("data") ."/". $this->collection->getClass().'/'.$this->collection->getName().'-'.date('Y-m-d').'.xlsx';
        if (file_exists($directory)) {
            $status = 200;
            $pesan = "success";
            $text = \Theme::base("data") ."/". $this->collection->getClass().'/'.$this->collection->getName().'-'.date('Y-m-d').'.xlsx';
        }

        $this->data['status'] = $status;
        $this->data['message'] = $pesan;
        $this->data['text'] = $text;
    }

    public function getAlfabet ($number) {
        $result = "";
        while ($number >= 0) {
            $result = chr($number % 26 + 65) . $result;
            $number = floor($number / 26) - 1;
        }
        return $result;
    }
}
