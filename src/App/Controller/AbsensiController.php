<?php

namespace App\Controller;

use \Norm\Controller\NormController;
use Norm\Norm;

class AbsensiController extends AppController
{
    public function create()
    {
        $entry = $this->collection->newInstance()->set($this->getCriteria());

        $this->data['entry'] = $entry;

        if ($this->request->isPost()) {
            try {
                $entry->set($this->request->getBody())->save();

                h('notification.info', $this->clazz.' created.');

                h('controller.create.success', array(
                    'model' => $entry
                ));
                $this->redirect(\URL::site('absensi').'/'.$entry->getId().'/update');
            } catch (Stop $e) {
                throw $e;
            } catch (Exception $e) {
                // no more set notification.error since notificationmiddleware will
                // write this later
                // h('notification.error', $e);

                h('controller.create.error', array(
                    'model' => $entry,
                    'error' => $e,
                ));

                // rethrow error to make sure notificationmiddleware know what todo
                throw $e;
            }
        }

    }

    public function update($id)
    {
        try {
            $entry = $this->collection->findOne($id);
            $getUser = $this->sqlQuery("
                    SELECT u.id, CONCAT(u.first_name, ' ', u.last_name) AS nama FROM user u 
                    WHERE u.id != 5 
                    AND u.status = 1
                    ORDER BY u.first_name
                ");

            $dataParticipants = $this->sqlQuery("
                    SELECT 
                        ap.id AS 'id_participant',
                        ap.nama AS 'id',
                        CONCAT(u.first_name, ' ', u.last_name) AS nama,
                        ap.status_kehadiran,
                        ap.keterangan
                    FROM absensi_participants ap 
                    INNER JOIN user u ON u.id = ap.nama AND u.status = 1
                    WHERE ap.id_absensi = $id
                    ORDER BY u.first_name
                ");
            if (!empty($dataParticipants)) {
                $this->data['participant'] = $dataParticipants;
            } else {
                $this->data['participant'] = $getUser;
            }
            
        } catch (Exception $e) {
            // noop
        }
        
        if (is_null($entry)) {
            return $this->app->notFound();
        }

        if ($this->request->isPost() || $this->request->isPut()) {
            try {
                $merged = array_merge(
                    isset($entry) ? $entry->dump() : array(),
                    $this->request->getBody() ?: array()
                );

                $entry->set($merged)->save();

                h('notification.info', $this->clazz.' updated');

                h('controller.update.success', array(
                    'model' => $entry,
                ));

                $this->redirect('absensi');
            } catch (Stop $e) {
                throw $e;
            } catch (Exception $e) {
                h('notification.error', $e);

                if (empty($entry)) {
                    $model = null;
                }

                h('controller.update.error', array(
                    'error' => $e,
                    'model' => $entry,
                ));
            }
        }

        $this->data['entry'] = $entry;
    }

    private function sqlQuery ($sql) {
        $connection = \Norm::getConnection('mysql')->getRaw();
        $statement = $connection->prepare($sql);

        if (empty($params)) {
            $statement->execute();
        } else {
            $statement->execute($params);
        }

        $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $results;
    }

}