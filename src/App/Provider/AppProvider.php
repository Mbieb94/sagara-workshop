<?php

namespace App\Provider;

use \Norm\Filter\Filter;
use Norm\Filter\FilterException;
use App\Library\Notification;
use Norm\Norm;

class AppProvider extends \Bono\Provider\Provider
{
    public function initialize()
    {
        $app = $this->app;

        $app->get('/', function() use ($app){
            $periode = \Norm::factory('Periode');
            $dataPeriode = $periode->find();
            $currentPeriode = $periode->findOne(array('workshop_status' => 1));
            $date = date('Y');
            $name = $_SESSION['user']['$id'];
            $data = 0;
            if ($_SESSION['user']['role'][0] != 5) {
                $query = $this->sqlQuery("
                        SELECT SUM(tn.value) AS jum
                        FROM table_nilai tn
                        WHERE tn.periode = '".$currentPeriode['code']."'
                        AND tn.name = $name
                    ");
                $data = $query[0]['jum'];
            }

            if ($_SESSION['user']['$id'] == 5) {
                $where = 'WHERE u.id != 5';
            } else {
                $where = 'WHERE u.id = '.$_SESSION['user']['$id'];
            }

            $absensi = $this->sqlQuery("
                    SELECT 
                        CONCAT(u.first_name, ' ', u.last_name ) AS nama,
                        (
                            SELECT COUNT(*) FROM absensi_participants ap
                            WHERE ap.nama = u.id AND ap.status_kehadiran = 1
                            AND ap.periode = '".$currentPeriode['code']."'
                        ) AS on_time,
                        (
                            SELECT COUNT(*) FROM absensi_participants ap
                            WHERE ap.nama = u.id AND ap.status_kehadiran = 2
                            AND ap.periode = '".$currentPeriode['code']."'
                        ) AS telat,
                        (
                            SELECT COUNT(*) FROM absensi_participants ap
                            WHERE ap.nama = u.id AND ap.status_kehadiran = 3
                            AND ap.periode = '".$currentPeriode['code']."'
                        ) AS tidak_masuk
                    FROM user u 
                    $where
                    ORDER BY u.first_name
                ");
            
            $app->response->data('periode', $dataPeriode);
            $app->response->data('jumlah', $data);
            $app->response->data('absensi', $absensi);
            if($_SESSION['user']['role'][0] == 5) {
                $app->response->template('static/index');
            } else {
                $app->response->template('dashboard/index');
            }
        });

        $app->get('/ajax_request', function () use ($app){
            $img = array();
            if (isset($_GET['nama'])) {
                $this->sqlQuery("UPDATE user SET is_selected = 1 WHERE first_name = '".$_GET['nama']."' ");
                $request = $this->sqlQuery("
                        SELECT u.photo
                        FROM user u
                        WHERE u.first_name = '".$_GET['nama']."'
                    ");

                $data = $request[0]['photo'] != '' ? $request[0]['photo'] : array();
                $img = array();
                if (count($data) > 0 ) {
                    $img = json_decode($data);
                }
            }
            
            $app->response->data('img', $img);
        });

        $app->get('/reset_draw', function () use ($app){
            $this->sqlQuery("UPDATE user SET is_selected = 0");
        });

        $app->get('/tnc', function () use ($app){
            $app->response->template('tnc/tnc');
        });

        $app->post('/saran', function () use ($app){
            $post = $this->app->request->post();

            if ($post) {
                $post['pesan'] = htmlspecialchars($post['pesan']);
                try {
                    $this->sqlQuery("
                            INSERT INTO kotak_saran (pengirim, penerima, pesan) VALUES ('".$post['pengirim']."', '".$post['penerima']."', '".$post['pesan']."')
                        ");       
                    h('notification.info', ' Saran anda sudah terkirim, Terimakasih :D ');
                } catch (Exception $e) {
                    h('notification.info', ' Maaf pesan gagal dikirim, silahkan coba lagi ');
                }
            }
            $this->app->redirect('kotak_saran');
        });

        $app->get('/data_user', function () use ($app){
            $data = $this->sqlQuery("
                    SELECT u.id, CONCAT(u.first_name, u.last_name) AS text 
                    FROM user u ORDER BY u.id
                ");

            $array = array("result" => $data, "pagination" => array("more" => true));
            
            $app->response->data('data', $array);
        });

        $app->get('/saran', function () use ($app){
            $app->response->template('saran/kotaksaran');
        });

        // DATA TABLE SERVER SIDE
        $app->get('/get_data_user', function () use ($app){
            $colum = array('id', 'first_name', 'last_name', 'username');
            $sql = $this->sqlQuery("SELECT COUNT(*) AS total FROM user");

            $totalRows = $sql[0]['total'];
            $totalFilter = $totalRows;

            $query = "SELECT id AS '0', first_name AS '1', last_name AS '2', username AS '3' FROM user WHERE 1";

            $request = $_REQUEST;
            if (!empty($request['search']['value'])) {
                foreach ($colum as $key => $value) {
                    if ($key == 0) {
                        $q = " AND";
                    } else {
                        $q = " OR";
                    }

                    $query .= $q." ".$value." LIKE '%".$request['search']['value']."%'";
                }
            }

            if (!empty($request['order'][0]['column'])) {
                $query .= " ORDER BY ".$colum[$request['order'][0]['column']]."   ".$request['order'][0]['dir'];
            }

            if (!empty($request['start'])) {
                $query .= " LIMIT ".$request['start']."  ,".$request['length'];
            } else {
                $query .= " LIMIT ".$request['length'];
                // $query .= " LIMIT 10";
            }

            $querySql = $this->sqlQuery($query);
            $totalData = count($querySql);
            
            $datas = array();
            foreach ($querySql as $key => $value) {
                $dt = array();
                foreach ($value as $keys => $values) {
                    $dt[$keys] = $values;
                    $dt[] = '<button type="button" id="getEdit" class="btn btn-primary btn-xs" data-id="'.$value[0].'" style="padding: 5px;"><i class="fa fa-edit"></i>Edit</button>
                <button type="button" id="getDelete" class="btn btn-danger btn-xs" data-id="'.$value[0].'" style="padding: 5px;"><i class="fa fa-trash"></i>Delete</button>';
                    // $dt[] = "<a type='button' class='btn btn-primary btn-xs' href='user/".$value[0]."/edit'><i class='fa fa-edit'></i>edit</a><a type='button' class='btn btn-danger btn-xs' href='user/".$value[0]."/delete'><i class='fa fa-trash'></i> Delete</a>";
                }
                $datas[] = $dt;
            }

            $draw = 1;
            if (!empty($request['draw'])) {
                $draw = $request['draw'];
            }
            $data = array(
                "draw" => $draw,
                "recordsTotal" => $totalRows,
                "recordsFiltered" => $totalFilter,
                "data" => $datas
            );

            echo json_encode($data);
            exit;
        });

        $app->get('/data_saran', function () use ($app){
            $colum = array('u.first_name', 'u.last_name', 'ks.pesan');
            $sql = $this->sqlQuery("SELECT COUNT(*) AS total FROM kotak_saran");

            $totalRows = $sql[0]['total'];
            $totalFilter = $totalRows;

            $query = "SELECT 
                        CONCAT(u.first_name, ' ', u.last_name) AS '0',
                        us.username AS '1',
                        ks.pesan AS '2' 
                    FROM kotak_saran ks
                    INNER JOIN user u ON u.id = ks.pengirim
                    INNER JOIN user us ON us.id = ks.penerima
                    WHERE 1";

            $request = $_REQUEST;
            if (!empty($request['search']['value'])) {
                foreach ($colum as $key => $value) {
                    if ($key == 0) {
                        $q = " AND";
                    } else {
                        $q = " OR";
                    }

                    $query .= $q." ".$value." LIKE '%".$request['search']['value']."%'";
                }
            }

            if (!empty($request['order'][0]['column'])) {
                $query .= " ORDER BY ".$colum[$request['order'][0]['column']]."   ".$request['order'][0]['dir'];
            }

            if (!empty($request['start'])) {
                $query .= " LIMIT ".$request['start']."  ,".$request['length'];
            } else {
                $query .= " LIMIT ".$request['length'];
                // $query .= " LIMIT 10";
            }

            $querySql = $this->sqlQuery($query);
            $totalData = count($querySql);

            $draw = 1;
            if (!empty($request['draw'])) {
                $draw = $request['draw'];
            }
            $data = array(
                "draw" => $draw,
                "recordsTotal" => $totalRows,
                "recordsFiltered" => $totalFilter,
                "data" => $querySql
            );

            echo json_encode($data);
            exit;
        });

        $app->get("/delete_user", function () use ($app){
            $result = $this->sqlQuery("DELETE FROM user WHERE id = ".$_GET['id']);

            $massage = 'Berhasil';
            if ($result) {
                $massage = 'Error';
            }

            echo $massage;
        });

        $app->get('/form_absensi', function () use ($app){
            $app->response->template('template.absensi');
        });

        $app->post('/absensi', function () use ($app){
            $post = $app->request->post();
            foreach ($post as $key => $value) {
                echo '<pre>';
                print_r($value[0].' '.$value[1]);
            }
            exit;
        });

        $app->get('/form_step', function () use ($app){
            $app->response->template('static.form_step');
        });

        $app->get('/generate_report', function() use ($app) {
            $periode = $app->request->get('periode');
            if(empty($periode)) {
                $app->response->data('status', 404);
                $app->response->data('message', 'error');
                $app->response->data('text', 'Periode is not selected !');

                return;
            }

            $masterPeriode = Norm::factory('Periode')->findOne(array('code' => $periode));
            if($masterPeriode['workshop_status'] == 1) {
                $app->response->data('status', 201);
                $app->response->data('message', 'In Progress');
                $app->response->data('text', 'Workshop is still in progress !');

                return;
            }

            $exist = Norm::factory('Reporting')->find(array('periode' => $periode));
            if($exist->count() > 0) {
                $app->response->data('status', 201);
                $app->response->data('message', 'Exist');
                $app->response->data('text', 'Data is exist !');

                return;
            }

            $query = "SELECT 
                        CONCAT(us.first_name, ' ', us.last_name) AS name,
                        (
                            SELECT COUNT(*) FROM absensi_participants AS ap 
                            WHERE ap.nama = us.id AND ap.status_kehadiran = 2
                        ) AS total_terlambat,
                        (
                            (SELECT COUNT(*) FROM absensi_participants AS ap WHERE ap.nama = us.id AND ap.status_kehadiran = 2) * 5000
                        ) AS denda_terlambat,
                        (
                            SELECT SUM(tn.value) FROM table_nilai AS tn WHERE tn.name = us.id GROUP BY tn.name
                        ) AS nilai_akumulasi
                    FROM user AS us 
                    WHERE us.status = 1 
                    AND us.role NOT LIKE '%5%' ";

            $exec = $this->sqlQuery($query);

            if(empty($exec)) {
                $app->response->data('status', 404);
                $app->response->data('message', 'empty');
                $app->response->data('text', 'Data is not exist !');

                return;
            }

            $data = [];
            foreach ($exec as $key => $value) {
                $data['periode'] = $periode;
                $data['nama'] = $value['name'];
                $data['total_terlambat'] = $value['total_terlambat'];
                $data['denda_keterlambatan'] = number_format($value['denda_terlambat']);
                $data['denda_tidak_siap'] = 0;
                $data['denda_akumulasi_nilai'] = number_format($this->hitungDenda($value['nilai_akumulasi']));
                $total = ($value['denda_terlambat'] + $this->hitungDenda($value['nilai_akumulasi']));
                $data['total_denda'] = number_format($total);

                $newData = Norm::factory('Reporting')->newInstance();
                $newData->set($data);
                $newData->save();
            }
            
            $app->response->data('status', 200);
            $app->response->data('message', 'success');
            $app->response->data('text', 'Data has been generated !');
        });
    }

    public function generateToken($length = 20) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function notFound ($status = '404', $reason = 'The page you are looking for does not exist.')
    {
        $data = array(
            'status' => $status,
            'reason' => $reason
        );
        $this->app->response->data('data', $data);
    	$this->app->response->template('notfound.404');
    }

    public function sqlQuery ($sql) {
        $connection = \Norm::getConnection('mysql')->getRaw();
        $statement = $connection->prepare($sql);

        if (empty($params)) {
            $statement->execute();
        } else {
            $statement->execute($params);
        }

        $results = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $results;
    }

    public function hitungDenda ($value) {
        $denda = 0;
        if($value < 0) {
            $hitung = ($value / 5);
            $total = floor($hitung) * (-1);
            $denda = $total * 5000;
        }

        return $denda;
    }

    public function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}
