<?php

namespace App\Provider;

class ChartProvider extends AppProvider
{
	public function initialize()
    {
    	$app = $this->app;

    	$app->get('/chart', function () use ($app){
            $currentPeriod = \Norm::factory('Periode')->findOne(array('workshop_status' => 1));
        	$periode = $currentPeriod['code'];
        	$level = '';

            if (!empty($app->request->get('periode'))) {
                $periode = $app->request->get('periode');
            }

            if (isset($_GET['level'])) {
            	if ($_GET['level'] == 2) {
	            	$level = "AND u.level = 2";
            	} else {
            		$level = '';
            	}
            }
        	$sql = "
        		SELECT 
        			CONCAT(u.first_name, ' ', u.last_name) AS name, 
				    (
				        SELECT SUM(tn.value)
				        FROM table_nilai tn
				        WHERE tn.name = u.id
				        AND tn.periode = '$periode'
				    ) AS y 
				FROM user u 
				WHERE u.id != 5 
				".$level."
				GROUP BY u.id
				ORDER BY y DESC
        	";
        	$data = $this->sqlQuery($sql);

        	$app->response->data('data', $data);
        });
    }
}