<?php

namespace App\Schema;

class Editor extends \Norm\Schema\UnsafeText
{
    public function formatReadonly($value, $entry = null)
    {
        // return html_entity_decode(parent::formatReadonly(nl2br($value), $entry));
        $app = \App::getInstance();

        return $app->theme->partial("_schema/editor/readonly", array(
            'label'    => $this['label'],
            'name'     => $this['name'],
            'value'    => $value,
            'readonly' => true,
            'entry'    => $entry,
        ));
    }

    public function formatInput($value, $entry = null)
    {
        $app = \App::getInstance();

        return $app->theme->partial("_schema/editor/input", array(
            'label'    => $this['label'],
            'name'     => $this['name'],
            'value'    => $value,
            'readonly' => false,
            'entry'    => $entry,
        ));
    }
}
