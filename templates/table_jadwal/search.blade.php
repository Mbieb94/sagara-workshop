@extends('layout')
<?php
use ROH\Util\Inflector;
use App\Library\Pagination;
?>
<?php
$schema = array();
foreach (f('controller')->schema() as $key => $field) {
    if ($field['list-column']) {
        $schema[$key] = $field;
    }
}

?>
@section('pagetitle')
   {{ Inflector::pluralize(Inflector::humanize(f('controller')->getClass())) }}
@stop

@section('sub-header')
<!--begin::Subheader-->
<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <!--begin::Info-->
        <div class="d-flex align-items-center flex-wrap mr-2">
            <!--begin::Page Title-->
            <a href="{{ f('controller.url') }}">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">{{ l('{0}', Inflector::humanize(f('controller')->getClass())) }}</h5>
            </a>
            <!--end::Page Title-->
            <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>

            <!--begin::Search Form-->
            <div class="d-flex align-items-center" id="kt_subheader_search">
                <span class="text-dark-50 font-weight-bold" id="kt_subheader_total">Listing</span>
                
            </div>
            <!--end::Search Form-->
        </div>
        <!--end::Info-->
        <!--begin::Toolbar-->
        <div class="d-flex align-items-center">
            <!--begin::Button-->
            <a href="#" class=""></a>
            <!--end::Button-->
            <!--begin::Button-->
            @if(f('auth.allowed', '/null/create'))
            <a href="{{ f('controller.url', '/null/create') }}" class="btn btn-light-primary font-weight-bold ml-2">
                <span class="svg-icon">
                    <!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-02-01-052524/theme/html/demo1/dist/../src/media/svg/icons/Navigation/Plus.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect fill="#000000" x="4" y="11" width="16" height="2" rx="1"/>
                            <rect fill="#000000" opacity="0.3" transform="translate(12.000000, 12.000000) rotate(-270.000000) translate(-12.000000, -12.000000) " x="4" y="11" width="16" height="2" rx="1"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                Add {{ l('{0}', Inflector::humanize(f('controller')->getClass())) }}
            </a>
            @endif
            @if(f('auth.allowed', '/:mutliid/trash'))
            <a href="{{f('controller.url','/:mutliid/trash')}}" data-title="Delete Selected Record" data-text="Are you sure want to delete selected data ?" class="btn btn-light-danger font-weight-bold ml-2 btn-multi-delete">
                <span class="svg-icon">
                    <!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-02-01-052524/theme/html/demo1/dist/../src/media/svg/icons/Navigation/Check.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <polygon points="0 0 24 0 24 24 0 24"/>
                            <path d="M6.26193932,17.6476484 C5.90425297,18.0684559 5.27315905,18.1196257 4.85235158,17.7619393 C4.43154411,17.404253 4.38037434,16.773159 4.73806068,16.3523516 L13.2380607,6.35235158 C13.6013618,5.92493855 14.2451015,5.87991302 14.6643638,6.25259068 L19.1643638,10.2525907 C19.5771466,10.6195087 19.6143273,11.2515811 19.2474093,11.6643638 C18.8804913,12.0771466 18.2484189,12.1143273 17.8356362,11.7474093 L14.0997854,8.42665306 L6.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.999995, 12.000002) rotate(-180.000000) translate(-11.999995, -12.000002) "/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>
                Delete Selected
            </a>
            @endif
            @if(f('auth.allowed', '/null/trashed'))
            <a href="{{f('controller.url','/null/trashed')}}" class="btn btn-light-danger font-weight-bold ml-2">
                <span class="svg-icon">
                    <!--begin::Svg Icon | path:/var/www/preview.keenthemes.com/metronic/releases/2021-05-14-112058/theme/html/demo1/dist/../src/media/svg/icons/Home/Trash.svg--><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                        <rect x="0" y="0" width="24" height="24"/>
                        <path d="M6,8 L18,8 L17.106535,19.6150447 C17.04642,20.3965405 16.3947578,21 15.6109533,21 L8.38904671,21 C7.60524225,21 6.95358004,20.3965405 6.89346498,19.6150447 L6,8 Z M8,10 L8.45438229,14.0894406 L15.5517885,14.0339036 L16,10 L8,10 Z" fill="#000000" fill-rule="nonzero"/>
                        <path d="M14,4.5 L14,3.5 C14,3.22385763 13.7761424,3 13.5,3 L10.5,3 C10.2238576,3 10,3.22385763 10,3.5 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>
                    </g>
                </svg><!--end::Svg Icon--></span>
            </a>
            @endif
        </div>
        <!--end::Toolbar-->
    </div>
</div>
<!--end::Subheader-->
@stop

@section('fields')
<div class="card card-custom">
    <div class="card-body">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap pl-0 pr-0 mb-5">
            <div class="d-flex">
                <a class="btn btn-light-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-filter"></i> Filter
                </a>
            </div>
            <div class="d-flex">
                @if(f('auth.allowed', '/table_jadwal/random_peserta/:id'))
                <a class="btn btn-light-primary mr-2" data-url="{{ f('controller.url','/null/export_excel.json') }}" id="random-peserta">
                    <i class="fa fa-play"></i> Run
                </a>
                @endif
                <a class="btn btn-light-success" data-url="{{ f('controller.url','/null/export_excel.json') }}" id="export-excel">
                    <i class="fa fa-file-excel"></i> Excel
                </a>
            </div>
        </div>
        <div class="collapse pb-5" id="collapseExample">
            <div class="card card-body">
                <form class="kt-form kt-form--label-right" id="searchform">
                    <div class="form-group row">
                    <?php $count = 0; ?>
                    @foreach (f('controller')->schema() as $name => $field)
                        @if ($field['searchable'])
                        <div class="col-lg-3 col-xl-3">
                            {{ $field->label() }}
                            {{ $entry->format($name, 'input') }}
                            <?php $count++; ?>
                        </div>
                        @endif
                    @endforeach
                    @if($count <= 0)
                        <h3>NO ADVANCE SEARCH</h3> 
                    @endif
                    </div>
                </form>
            </div>
        </div>
        @section('table.search')
        <div class="table-responsive">
            <table id="datatable" class="table table-striped" style="width:100%">
                <thead>
                    <tr>
                        @if (count($schema))
                            <th class="sorting_disabled">
                                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                                    <input type="checkbox" class="group-checkable"/>
                                    <span></span>
                                </label>
                            </th>
                            @foreach ($schema as $key => $field)
                            <th>
                                {{ $field['label'] }}
                            </th>
                            @endforeach
                            <th class="sorting_disabled">&nbsp; </th>
                        @else
                            <th>Data</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        @show
    </div>
</div>
@stop
@section('customjs')
<script src="{{ Theme::base('assets/js/api.js') }}"></script>
<script>
    var data = $('form#searchform').serializeArray();
    var src = {};
    $.map(data, function(item, value){
        src[item['name']] = item['value'];
    });

    $('form#searchform').on("keyup change", function(e){
        var obj = api.serializeObject($(this).serializeArray());
        src = obj;
        datatable.ajax.reload();
    });

    $(document).on('click', '.input-title', function(e){
        var url = $(this).data('remote');
        Swal.fire({
            icon: 'question',
            title: 'Set Your Title',
            input: 'text',
            inputAttributes: {
                autocapitalize: 'off'
            },
            showCancelButton: true,
            confirmButtonText: 'Save',
            showLoaderOnConfirm: true,
            preConfirm: function(result) {
                if(!result) {
                    Swal.showValidationMessage(
                        `This field is required`
                    )
                } else {
                    $.ajax({
                        url: url,
                        data: {judul: result},
                        type: "POST",
                        success: function(response) {
                            switch(response.status){
                                case 200:
                                    Swal.fire({
                                        icon: 'success',
                                        title: response.message,
                                        text: response.text,
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                    datatable.ajax.reload();
                                    break;
                                case 404:
                                    Swal.fire({
                                        icon: 'info',
                                        title: response.message,
                                        text: response.text,
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                    break;
                                case 401:
                                    Swal.fire({
                                        icon: 'warning',
                                        title: response.message,
                                        text: response.text,
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                    break;
                                case 500:
                                    Swal.fire({
                                        icon: 'error',
                                        title: response.message,
                                        text: response.text,
                                        showConfirmButton: false,
                                        timer: 1500
                                    });
                                    break;
                                default:
                                    break;
                            }  
                        },
                        error: function(error) {
                            Swal.showValidationMessage(
                                `Request failed: ${error}`
                            )
                        }
                    })
                }
            },
            allowOutsideClick: function() { !Swal.isLoading() }
        });
    });

    var datatable = $('#datatable').DataTable({
        "scrollX": true,
        "processing": true,
        "serverSide": true,
        "sPaginationType": "full_numbers",
        "columnDefs": [ 
            {
                "targets": 0,
                "orderable": false
            },
            {
                "targets": -1,
                "orderable": false
            } 
        ],
        "ajax": {
            'url': "{{ f('controller.url', '/null/data_table.json') }}",
            "data":  function ( data ) {
                data.param = src;
                data.status = 1;
                data.search = data.search.value;
            },
            "complete": function(response) {
                $(".dataTable").find("th:first-child").removeClass("sorting_asc");
                $("select[name=datatable_length]").select2({width: '50%'});
            }
        },
        createdRow: function ( row, data, index ) {
            var id = data[0];
            $(row).find('td:first-child').html('');
            $(row).find('td:first-child').css('width', '3%');
            $(row).find('td:first-child').append(`
                <label class="checkbox checkbox-outline checkbox-outline-2x checkbox-primary">
                    <input type="checkbox" class="checkboxes search-checkbox" value="${id}"/>
                    <span></span>
                </label>
            `);
            $(row).find('td:last-child').append(`
                <div class="card-toolbar float-right">
                    <a data-remote="{{ f('controller.url', '/change_title/${id}.json') }}" class="btn btn-icon btn-light btn-hover-primary btn-sm input-title" style="cursor: pointer">
                        <span class="svg-icon svg-icon-md svg-icon-primary">
                            <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Settings-1.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"/>
                                    <path d="M3,19 L5,19 L5,21 L3,21 L3,19 Z M8,19 L10,19 L10,21 L8,21 L8,19 Z M13,19 L15,19 L15,21 L13,21 L13,19 Z M18,19 L20,19 L20,21 L18,21 L18,19 Z" fill="#000000" opacity="0.3"/>
                                    <path d="M10.504,3.256 L12.466,3.256 L17.956,16 L15.364,16 L14.176,13.084 L8.65000004,13.084 L7.49800004,16 L4.96000004,16 L10.504,3.256 Z M13.384,11.14 L11.422,5.956 L9.42400004,11.14 L13.384,11.14 Z" fill="#000000"/>
                                </g>
                            </svg>
                            <!--end::Svg Icon-->
                        </span>
                    </a>
                    @if(f('auth.allowed', f('controller')->getBaseUri().'/:id'))
                        <a href="{{ f('controller.url', '/${id}') }}" class="btn btn-icon btn-light btn-hover-primary btn-sm ml-1">
                            <span class="svg-icon svg-icon-md svg-icon-primary">
                                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Settings-1.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"></rect>
                                        <path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"></path>
                                        <path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"></path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </a>
                    @endif
                    @if(f('auth.allowed', f('controller')->getBaseUri().'/:id/update'))
                        <a href="{{ f('controller.url', '/${id}/update') }}" class="btn btn-icon btn-light btn-hover-primary btn-sm ml-1">
                            <span class="svg-icon svg-icon-md svg-icon-primary">
                                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Write.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"></rect>
                                        <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953)"></path>
                                        <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </a>
                    @endif
                    @if(f('auth.allowed', f('controller')->getBaseUri().'/:id/trash'))
                        <a data-title="Delete Record" data-text="Are you sure want to delete this data ?" data-remote="{{ f('controller.url', '/${id}/trash') }}" class="btn btn-icon btn-light btn-hover-primary btn-sm delete-record ml-1" title="Delete record" style="cursor: pointer">
                            <span class="svg-icon svg-icon-md svg-icon-primary">
                                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Trash.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"></rect>
                                        <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
                                        <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                        </a>
                    @endif
                </div>
            `);
        }
    });

    var getParams = {};
    datatable.on( 'xhr', function () {
        getParams = datatable.ajax.params();
    });

    $("#export-excel").on("click", function(e){
        e.preventDefault();
        var url = $(this).data('url');
        console.log(url)
        $.ajax({
            url: url,
            type: "GET",
            data: getParams,
            success: function (result) {
                switch (result.status) {
                    case 200:
                        window.location.href = result.text;    
                        break;
                    case 201:
                        Swal.fire({
                            icon: 'warning',
                            title: result.message,
                            text: result.text
                        });
                        break;
                    case 404:
                        Swal.fire({
                            icon: 'error',
                            title: result.message,
                            text: result.text
                        });
                        break;
                    default:
                        break;
                }
            },
            error: function (error) {
                switch(error.status) {
                    case 401:
                        Swal.fire({
                            icon: 'warning',
                            title: error.statusText,
                            text: 'You are not allowed this action'
                        });
                        break;
                    default:
                        Swal.fire({
                            icon: 'error',
                            title: error.statusText,
                            text: 'Error status : ' + error.status
                        });
                        break;
                }
            }
        });
    });

    $("#random-peserta").on("click", function(){
        var icon = $(this).find('i');
        icon.removeClass('fa-play');
        icon.addClass('fa-pause');
        if(typeof src.periode == 'undefined') {
            Swal.fire({
                icon: 'error',
                title: 'Opss ..',
                text: 'Periode workshop harus diisi !',
                // showConfirmButton: false,
                // timer: 1500
            });
            icon.removeClass('fa-pause');
            icon.addClass('fa-play');
            return;
        }
        var url = "{{ URL::site('table_jadwal/random_peserta') }}" + '/' +  src.periode + '.json';
        $.ajax({
            url: url,
            type: 'GET',
            success: function (res) {
                switch (res.status) {
                    case 200:
                        let timerInterval
                        Swal.fire({
                          imageUrl: res.data.photo,
                          imageHeight: 200,
                          imageAlt: 'A tall image',
                          title: res.data.first_name + ' ' + res.data.last_name,
                          showConfirmButton: false,
                          timer: 3500,
                          timerProgressBar: true,
                          willClose: () => {
                            clearInterval(timerInterval)
                          }
                        }).then(function(res){
                            datatable.ajax.reload();
                            setTimeout(function(){
                                $("#random-peserta").click();
                            }, 2000);
                        });

                        break;
                    case 201:
                        Swal.fire({
                            icon: 'info',
                            title: res.pesan
                        });
                        icon.removeClass('fa-pause');
                        icon.addClass('fa-play');
                        break;
                    case 202:
                        Swal.fire({
                            icon: 'success',
                            title: res.pesan
                        });
                        icon.removeClass('fa-pause');
                        icon.addClass('fa-play');
                        break;
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    });
</script>
@stop
@section('customcss')
@stop
