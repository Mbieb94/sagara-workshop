-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 05 Agu 2021 pada 15.04
-- Versi server: 10.4.16-MariaDB
-- Versi PHP: 7.3.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `workshop`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi`
--

CREATE TABLE `absensi` (
  `id` int(11) NOT NULL,
  `nama` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `periode` varchar(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `absensi`
--

INSERT INTO `absensi` (`id`, `nama`, `tanggal`, `periode`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(1, 12, '2020-02-15 00:00:00', 'W-001', 1, '5', '5', '2020-03-06 08:28:36', '2021-07-08 21:59:22'),
(2, 24, '2020-02-21 00:00:00', 'W-001', 1, '5', '5', '2020-03-06 08:32:05', '2020-03-06 23:11:46'),
(3, 26, '2020-02-28 00:00:00', 'W-001', 1, '5', '5', '2020-03-06 08:36:40', '2020-03-06 23:11:40'),
(4, 17, '2020-03-06 00:00:00', 'W-001', 1, '5', '5', '2020-03-06 08:45:44', '2020-03-06 23:11:31'),
(5, 16, '2020-03-13 00:00:00', 'W-001', 1, '5', '5', '2020-03-13 09:03:46', '2020-03-13 12:15:29'),
(6, 22, '2020-06-12 00:00:00', 'W-001', 1, '5', '5', '2020-06-12 07:02:22', '2020-06-12 12:41:21'),
(7, 10, '2020-06-19 00:00:00', 'W-001', 1, '5', '5', '2020-06-19 08:45:12', '2020-06-19 08:50:27'),
(8, 8, '2020-06-26 00:00:00', 'W-001', 1, '5', '5', '2020-06-26 08:50:16', '2020-06-26 08:51:48'),
(9, 19, '2020-07-10 00:00:00', 'W-001', 1, '5', '5', '2020-07-10 08:11:59', '2020-07-10 10:35:17'),
(10, 14, '2020-07-17 00:00:00', 'W-001', 1, '5', '5', '2020-07-17 08:27:20', '2020-07-17 12:54:33'),
(11, 20, '2020-07-24 00:00:00', 'W-001', 1, '5', '5', '2020-07-24 08:01:43', '2020-07-24 13:39:03'),
(12, 15, '2020-08-14 00:00:00', 'W-001', 1, '5', '5', '2020-08-14 08:40:42', '2020-08-14 08:45:22'),
(13, 9, '2020-08-28 00:00:00', 'W-001', 1, '5', '5', '2020-08-28 08:23:12', '2020-08-28 08:45:47'),
(14, 21, '2020-11-06 00:00:00', 'W-001', 1, '5', '5', '2020-11-06 08:28:42', '2020-11-06 12:57:55'),
(15, 23, '2020-11-20 00:00:00', 'W-001', 1, '5', '5', '2020-11-20 08:35:45', '2020-11-20 12:42:12'),
(16, 13, '2020-12-11 00:00:00', 'W-001', 1, '5', '5', '2020-12-11 07:57:57', '2020-12-11 11:28:53'),
(18, 11, '2021-01-08 00:00:00', 'W-001', 1, '5', '5', '2021-01-08 08:38:01', '2021-01-08 09:08:17'),
(19, 6, '2021-01-18 00:00:00', 'W-001', 1, '5', '5', '2021-01-18 19:41:27', '2021-01-18 19:41:46'),
(20, 7, '2021-01-18 00:00:00', 'W-001', 1, '5', '5', '2021-01-18 19:55:46', '2021-06-11 22:43:18'),
(21, 5, '2021-04-07 00:00:00', 'W-001', 1, '5', '5', '2021-04-07 15:30:48', '2021-06-11 22:42:49'),
(22, 5, '2021-07-08 00:00:00', 'W-001', 1, '5', '5', '2021-07-08 21:50:39', '2021-07-08 21:50:39'),
(23, 5, '2021-07-08 00:00:00', 'W-001', 1, '5', '5', '2021-07-08 21:51:10', '2021-07-08 21:51:10'),
(24, 5, '2021-07-08 00:00:00', 'W-001', 1, '5', '5', '2021-07-08 21:51:38', '2021-07-08 21:52:36'),
(25, 6, '2021-07-08 00:00:00', 'W-001', 1, '5', '5', '2021-07-08 22:03:59', '2021-07-08 22:03:59'),
(26, 6, '2021-07-08 00:00:00', 'W-001', 1, '5', '5', '2021-07-08 22:04:14', '2021-07-08 22:04:14'),
(27, 6, '2021-07-08 00:00:00', 'W-001', 1, '5', '5', '2021-07-08 22:04:32', '2021-07-08 22:04:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi_participants`
--

CREATE TABLE `absensi_participants` (
  `id` int(11) NOT NULL,
  `id_absensi` int(11) NOT NULL,
  `pembicara` int(11) NOT NULL,
  `nama` int(11) NOT NULL,
  `status_kehadiran` int(11) NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `absensi_participants`
--

INSERT INTO `absensi_participants` (`id`, `id_absensi`, `pembicara`, `nama`, `status_kehadiran`, `keterangan`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(24, 2, 24, 26, 2, '', 1, NULL, NULL, NULL, NULL),
(25, 2, 24, 6, 1, '', 1, NULL, NULL, NULL, NULL),
(26, 2, 24, 28, 3, 'Belum Aktif', 1, NULL, NULL, NULL, NULL),
(27, 2, 24, 7, 3, 'Sakit', 1, NULL, NULL, NULL, NULL),
(28, 2, 24, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(29, 2, 24, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(30, 2, 24, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(31, 2, 24, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(32, 2, 24, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(33, 2, 24, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(34, 2, 24, 14, 3, 'Belum Aktif', 1, NULL, NULL, NULL, NULL),
(35, 2, 24, 15, 3, 'Sakit', 1, NULL, NULL, NULL, NULL),
(36, 2, 24, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(37, 2, 24, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(38, 2, 24, 19, 2, '', 1, NULL, NULL, NULL, NULL),
(39, 2, 24, 17, 1, '', 1, NULL, NULL, NULL, NULL),
(40, 2, 24, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(41, 2, 24, 21, 2, '', 1, NULL, NULL, NULL, NULL),
(42, 2, 24, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(43, 2, 24, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(44, 2, 24, 24, 2, '', 1, NULL, NULL, NULL, NULL),
(45, 2, 24, 27, 3, 'X Sagara', 1, NULL, NULL, NULL, NULL),
(46, 2, 24, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(47, 3, 26, 26, 1, '', 1, NULL, NULL, NULL, NULL),
(48, 3, 26, 6, 2, '', 1, NULL, NULL, NULL, NULL),
(49, 3, 26, 28, 3, 'Belum Aktif', 1, NULL, NULL, NULL, NULL),
(50, 3, 26, 7, 2, '', 1, NULL, NULL, NULL, NULL),
(51, 3, 26, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(52, 3, 26, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(53, 3, 26, 10, 2, '', 1, NULL, NULL, NULL, NULL),
(54, 3, 26, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(55, 3, 26, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(56, 3, 26, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(57, 3, 26, 14, 3, 'Belum Aktif', 1, NULL, NULL, NULL, NULL),
(58, 3, 26, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(59, 3, 26, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(60, 3, 26, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(61, 3, 26, 19, 2, '', 1, NULL, NULL, NULL, NULL),
(62, 3, 26, 17, 1, '', 1, NULL, NULL, NULL, NULL),
(63, 3, 26, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(64, 3, 26, 21, 1, '', 1, NULL, NULL, NULL, NULL),
(65, 3, 26, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(66, 3, 26, 23, 3, 'Meeting (Kesibukan Kantor)', 1, NULL, NULL, NULL, NULL),
(67, 3, 26, 24, 3, 'Cuti', 1, NULL, NULL, NULL, NULL),
(68, 3, 26, 27, 3, 'X Sagara', 1, NULL, NULL, NULL, NULL),
(69, 3, 26, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(70, 4, 17, 26, 2, '', 1, NULL, NULL, NULL, NULL),
(71, 4, 17, 6, 1, '', 1, NULL, NULL, NULL, NULL),
(72, 4, 17, 28, 1, '', 1, NULL, NULL, NULL, NULL),
(73, 4, 17, 7, 1, '', 1, NULL, NULL, NULL, NULL),
(74, 4, 17, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(75, 4, 17, 9, 2, '', 1, NULL, NULL, NULL, NULL),
(76, 4, 17, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(77, 4, 17, 11, 3, 'Meeting (Kesibukan Kantor) ', 1, NULL, NULL, NULL, NULL),
(78, 4, 17, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(79, 4, 17, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(80, 4, 17, 14, 3, 'Belum Aktif', 1, NULL, NULL, NULL, NULL),
(81, 4, 17, 15, 3, 'Meeting (Kesibukan Kantor)', 1, NULL, NULL, NULL, NULL),
(82, 4, 17, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(83, 4, 17, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(84, 4, 17, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(85, 4, 17, 17, 1, '', 1, NULL, NULL, NULL, NULL),
(86, 4, 17, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(87, 4, 17, 21, 1, '', 1, NULL, NULL, NULL, NULL),
(88, 4, 17, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(89, 4, 17, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(90, 4, 17, 24, 3, 'Cuti', 1, NULL, NULL, NULL, NULL),
(91, 4, 17, 27, 3, 'X Sagara', 1, NULL, NULL, NULL, NULL),
(92, 4, 17, 25, 3, 'Cuti', 1, NULL, NULL, NULL, NULL),
(93, 5, 16, 26, 1, '', 1, NULL, NULL, NULL, NULL),
(94, 5, 16, 6, 2, '', 1, NULL, NULL, NULL, NULL),
(95, 5, 16, 28, 1, '', 1, NULL, NULL, NULL, NULL),
(96, 5, 16, 7, 1, '', 1, NULL, NULL, NULL, NULL),
(97, 5, 16, 8, 3, 'Cuti', 1, NULL, NULL, NULL, NULL),
(98, 5, 16, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(99, 5, 16, 10, 2, '', 1, NULL, NULL, NULL, NULL),
(100, 5, 16, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(101, 5, 16, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(102, 5, 16, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(103, 5, 16, 14, 3, 'Belum Aktif', 1, NULL, NULL, NULL, NULL),
(104, 5, 16, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(105, 5, 16, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(106, 5, 16, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(107, 5, 16, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(108, 5, 16, 17, 3, 'Cuti', 1, NULL, NULL, NULL, NULL),
(109, 5, 16, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(110, 5, 16, 21, 3, 'Cuti', 1, NULL, NULL, NULL, NULL),
(111, 5, 16, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(112, 5, 16, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(113, 5, 16, 24, 3, 'Cuti', 1, NULL, NULL, NULL, NULL),
(114, 5, 16, 27, 3, 'X Sagara', 1, NULL, NULL, NULL, NULL),
(115, 5, 16, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(116, 6, 22, 26, 2, '', 1, NULL, NULL, NULL, NULL),
(117, 6, 22, 6, 2, '', 1, NULL, NULL, NULL, NULL),
(118, 6, 22, 28, 2, '', 1, NULL, NULL, NULL, NULL),
(119, 6, 22, 7, 1, '', 1, NULL, NULL, NULL, NULL),
(120, 6, 22, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(121, 6, 22, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(122, 6, 22, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(123, 6, 22, 11, 2, '', 1, NULL, NULL, NULL, NULL),
(124, 6, 22, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(125, 6, 22, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(126, 6, 22, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(127, 6, 22, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(128, 6, 22, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(129, 6, 22, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(130, 6, 22, 19, 3, '', 1, NULL, NULL, NULL, NULL),
(131, 6, 22, 17, 1, '', 1, NULL, NULL, NULL, NULL),
(132, 6, 22, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(133, 6, 22, 21, 1, '', 1, NULL, NULL, NULL, NULL),
(134, 6, 22, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(135, 6, 22, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(136, 6, 22, 24, 2, '', 1, NULL, NULL, NULL, NULL),
(137, 6, 22, 27, 3, '', 1, NULL, NULL, NULL, NULL),
(138, 6, 22, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(139, 7, 10, 26, 2, '', 1, NULL, NULL, NULL, NULL),
(140, 7, 10, 6, 1, '', 1, NULL, NULL, NULL, NULL),
(141, 7, 10, 28, 2, '', 1, NULL, NULL, NULL, NULL),
(142, 7, 10, 7, 2, '', 1, NULL, NULL, NULL, NULL),
(143, 7, 10, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(144, 7, 10, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(145, 7, 10, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(146, 7, 10, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(147, 7, 10, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(148, 7, 10, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(149, 7, 10, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(150, 7, 10, 15, 2, '', 1, NULL, NULL, NULL, NULL),
(151, 7, 10, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(152, 7, 10, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(153, 7, 10, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(154, 7, 10, 17, 1, '', 1, NULL, NULL, NULL, NULL),
(155, 7, 10, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(156, 7, 10, 21, 1, '', 1, NULL, NULL, NULL, NULL),
(157, 7, 10, 22, 2, '', 1, NULL, NULL, NULL, NULL),
(158, 7, 10, 23, 3, '', 1, NULL, NULL, NULL, NULL),
(159, 7, 10, 24, 2, '', 1, NULL, NULL, NULL, NULL),
(160, 7, 10, 27, 3, '', 1, NULL, NULL, NULL, NULL),
(161, 7, 10, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(162, 8, 8, 26, 1, '', 1, NULL, NULL, NULL, NULL),
(163, 8, 8, 6, 2, '', 1, NULL, NULL, NULL, NULL),
(164, 8, 8, 28, 1, '', 1, NULL, NULL, NULL, NULL),
(165, 8, 8, 7, 2, '', 1, NULL, NULL, NULL, NULL),
(166, 8, 8, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(167, 8, 8, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(168, 8, 8, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(169, 8, 8, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(170, 8, 8, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(171, 8, 8, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(172, 8, 8, 14, 2, '', 1, NULL, NULL, NULL, NULL),
(173, 8, 8, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(174, 8, 8, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(175, 8, 8, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(176, 8, 8, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(177, 8, 8, 17, 1, '', 1, NULL, NULL, NULL, NULL),
(178, 8, 8, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(179, 8, 8, 21, 1, '', 1, NULL, NULL, NULL, NULL),
(180, 8, 8, 22, 2, '', 1, NULL, NULL, NULL, NULL),
(181, 8, 8, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(182, 8, 8, 24, 1, '', 1, NULL, NULL, NULL, NULL),
(183, 8, 8, 27, 3, '', 1, NULL, NULL, NULL, NULL),
(184, 8, 8, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(185, 9, 19, 26, 1, '', 1, NULL, NULL, NULL, NULL),
(186, 9, 19, 6, 3, '', 1, NULL, NULL, NULL, NULL),
(187, 9, 19, 28, 1, '', 1, NULL, NULL, NULL, NULL),
(188, 9, 19, 7, 2, '', 1, NULL, NULL, NULL, NULL),
(189, 9, 19, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(190, 9, 19, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(191, 9, 19, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(192, 9, 19, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(193, 9, 19, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(194, 9, 19, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(195, 9, 19, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(196, 9, 19, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(197, 9, 19, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(198, 9, 19, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(199, 9, 19, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(200, 9, 19, 17, 1, '', 1, NULL, NULL, NULL, NULL),
(201, 9, 19, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(202, 9, 19, 21, 2, '', 1, NULL, NULL, NULL, NULL),
(203, 9, 19, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(204, 9, 19, 23, 3, '', 1, NULL, NULL, NULL, NULL),
(205, 9, 19, 24, 3, '', 1, NULL, NULL, NULL, NULL),
(206, 9, 19, 27, 3, '', 1, NULL, NULL, NULL, NULL),
(207, 9, 19, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(208, 10, 14, 26, 1, '', 1, NULL, NULL, NULL, NULL),
(209, 10, 14, 6, 1, '', 1, NULL, NULL, NULL, NULL),
(210, 10, 14, 28, 1, '', 1, NULL, NULL, NULL, NULL),
(211, 10, 14, 7, 2, '', 1, NULL, NULL, NULL, NULL),
(212, 10, 14, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(213, 10, 14, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(214, 10, 14, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(215, 10, 14, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(216, 10, 14, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(217, 10, 14, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(218, 10, 14, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(219, 10, 14, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(220, 10, 14, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(221, 10, 14, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(222, 10, 14, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(223, 10, 14, 17, 2, '', 1, NULL, NULL, NULL, NULL),
(224, 10, 14, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(225, 10, 14, 21, 2, '', 1, NULL, NULL, NULL, NULL),
(226, 10, 14, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(227, 10, 14, 23, 3, 'Tidak masuk', 1, NULL, NULL, NULL, NULL),
(228, 10, 14, 24, 3, '', 1, NULL, NULL, NULL, NULL),
(229, 10, 14, 27, 3, '', 1, NULL, NULL, NULL, NULL),
(230, 10, 14, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(231, 11, 20, 26, 1, '', 1, NULL, NULL, NULL, NULL),
(232, 11, 20, 6, 2, '', 1, NULL, NULL, NULL, NULL),
(233, 11, 20, 28, 1, '', 1, NULL, NULL, NULL, NULL),
(234, 11, 20, 7, 2, '', 1, NULL, NULL, NULL, NULL),
(235, 11, 20, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(236, 11, 20, 9, 3, '', 1, NULL, NULL, NULL, NULL),
(237, 11, 20, 10, 2, '', 1, NULL, NULL, NULL, NULL),
(238, 11, 20, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(239, 11, 20, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(240, 11, 20, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(241, 11, 20, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(242, 11, 20, 15, 2, '', 1, NULL, NULL, NULL, NULL),
(243, 11, 20, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(244, 11, 20, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(245, 11, 20, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(246, 11, 20, 17, 1, '', 1, NULL, NULL, NULL, NULL),
(247, 11, 20, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(248, 11, 20, 21, 1, '', 1, NULL, NULL, NULL, NULL),
(249, 11, 20, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(250, 11, 20, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(251, 11, 20, 24, 3, '', 1, NULL, NULL, NULL, NULL),
(252, 11, 20, 27, 3, '', 1, NULL, NULL, NULL, NULL),
(253, 11, 20, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(254, 12, 15, 26, 2, '', 1, NULL, NULL, NULL, NULL),
(255, 12, 15, 6, 2, '', 1, NULL, NULL, NULL, NULL),
(256, 12, 15, 28, 1, '', 1, NULL, NULL, NULL, NULL),
(257, 12, 15, 7, 2, '', 1, NULL, NULL, NULL, NULL),
(258, 12, 15, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(259, 12, 15, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(260, 12, 15, 29, 1, '', 1, NULL, NULL, NULL, NULL),
(261, 12, 15, 10, 3, '', 1, NULL, NULL, NULL, NULL),
(262, 12, 15, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(263, 12, 15, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(264, 12, 15, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(265, 12, 15, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(266, 12, 15, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(267, 12, 15, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(268, 12, 15, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(269, 12, 15, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(270, 12, 15, 17, 2, '', 1, NULL, NULL, NULL, NULL),
(271, 12, 15, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(272, 12, 15, 21, 1, '', 1, NULL, NULL, NULL, NULL),
(273, 12, 15, 22, 2, '', 1, NULL, NULL, NULL, NULL),
(274, 12, 15, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(275, 12, 15, 24, 3, '', 1, NULL, NULL, NULL, NULL),
(276, 12, 15, 27, 3, '', 1, NULL, NULL, NULL, NULL),
(277, 12, 15, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(278, 13, 9, 26, 2, '', 1, NULL, NULL, NULL, NULL),
(279, 13, 9, 6, 1, '', 1, NULL, NULL, NULL, NULL),
(280, 13, 9, 28, 2, '', 1, NULL, NULL, NULL, NULL),
(281, 13, 9, 7, 2, '', 1, NULL, NULL, NULL, NULL),
(282, 13, 9, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(283, 13, 9, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(284, 13, 9, 29, 1, '', 1, NULL, NULL, NULL, NULL),
(285, 13, 9, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(286, 13, 9, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(287, 13, 9, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(288, 13, 9, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(289, 13, 9, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(290, 13, 9, 15, 3, '', 1, NULL, NULL, NULL, NULL),
(291, 13, 9, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(292, 13, 9, 18, 3, '', 1, NULL, NULL, NULL, NULL),
(293, 13, 9, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(294, 13, 9, 17, 2, '', 1, NULL, NULL, NULL, NULL),
(295, 13, 9, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(296, 13, 9, 21, 2, '', 1, NULL, NULL, NULL, NULL),
(297, 13, 9, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(298, 13, 9, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(299, 13, 9, 24, 3, '', 1, NULL, NULL, NULL, NULL),
(300, 13, 9, 27, 3, '', 1, NULL, NULL, NULL, NULL),
(301, 13, 9, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(302, 14, 21, 26, 1, '', 1, NULL, NULL, NULL, NULL),
(303, 14, 21, 6, 2, '', 1, NULL, NULL, NULL, NULL),
(304, 14, 21, 28, 3, '', 1, NULL, NULL, NULL, NULL),
(305, 14, 21, 7, 2, '', 1, NULL, NULL, NULL, NULL),
(306, 14, 21, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(307, 14, 21, 9, 3, 'Cuti', 1, NULL, NULL, NULL, NULL),
(308, 14, 21, 29, 1, '', 1, NULL, NULL, NULL, NULL),
(309, 14, 21, 10, 2, '', 1, NULL, NULL, NULL, NULL),
(310, 14, 21, 11, 2, '', 1, NULL, NULL, NULL, NULL),
(311, 14, 21, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(312, 14, 21, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(313, 14, 21, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(314, 14, 21, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(315, 14, 21, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(316, 14, 21, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(317, 14, 21, 19, 2, '', 1, NULL, NULL, NULL, NULL),
(318, 14, 21, 17, 2, '', 1, NULL, NULL, NULL, NULL),
(319, 14, 21, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(320, 14, 21, 21, 1, '', 1, NULL, NULL, NULL, NULL),
(321, 14, 21, 22, 2, '', 1, NULL, NULL, NULL, NULL),
(322, 14, 21, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(323, 14, 21, 24, 3, '', 1, NULL, NULL, NULL, NULL),
(324, 14, 21, 30, 1, '', 1, NULL, NULL, NULL, NULL),
(325, 14, 21, 27, 3, '', 1, NULL, NULL, NULL, NULL),
(326, 14, 21, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(327, 15, 23, 26, 1, '', 1, NULL, NULL, NULL, NULL),
(328, 15, 23, 6, 1, '', 1, NULL, NULL, NULL, NULL),
(329, 15, 23, 28, 3, '', 1, NULL, NULL, NULL, NULL),
(330, 15, 23, 7, 1, '', 1, NULL, NULL, NULL, NULL),
(331, 15, 23, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(332, 15, 23, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(333, 15, 23, 29, 1, '', 1, NULL, NULL, NULL, NULL),
(334, 15, 23, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(335, 15, 23, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(336, 15, 23, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(337, 15, 23, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(338, 15, 23, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(339, 15, 23, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(340, 15, 23, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(341, 15, 23, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(342, 15, 23, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(343, 15, 23, 17, 1, '', 1, NULL, NULL, NULL, NULL),
(344, 15, 23, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(345, 15, 23, 21, 1, '', 1, NULL, NULL, NULL, NULL),
(346, 15, 23, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(347, 15, 23, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(348, 15, 23, 24, 3, '', 1, NULL, NULL, NULL, NULL),
(349, 15, 23, 30, 1, '', 1, NULL, NULL, NULL, NULL),
(350, 15, 23, 27, 3, '', 1, NULL, NULL, NULL, NULL),
(351, 15, 23, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(352, 16, 13, 26, 1, '', 1, NULL, NULL, NULL, NULL),
(353, 16, 13, 6, 3, '', 1, NULL, NULL, NULL, NULL),
(354, 16, 13, 28, 2, '', 1, NULL, NULL, NULL, NULL),
(355, 16, 13, 7, 2, '', 1, NULL, NULL, NULL, NULL),
(356, 16, 13, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(357, 16, 13, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(358, 16, 13, 29, 2, '', 1, NULL, NULL, NULL, NULL),
(359, 16, 13, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(360, 16, 13, 11, 2, '', 1, NULL, NULL, NULL, NULL),
(361, 16, 13, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(362, 16, 13, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(363, 16, 13, 14, 2, '', 1, NULL, NULL, NULL, NULL),
(364, 16, 13, 15, 2, '', 1, NULL, NULL, NULL, NULL),
(365, 16, 13, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(366, 16, 13, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(367, 16, 13, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(368, 16, 13, 17, 2, '', 1, NULL, NULL, NULL, NULL),
(369, 16, 13, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(370, 16, 13, 21, 2, '', 1, NULL, NULL, NULL, NULL),
(371, 16, 13, 22, 2, '', 1, NULL, NULL, NULL, NULL),
(372, 16, 13, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(373, 16, 13, 24, 3, '', 1, NULL, NULL, NULL, NULL),
(374, 16, 13, 30, 1, '', 1, NULL, NULL, NULL, NULL),
(375, 16, 13, 27, 3, '', 1, NULL, NULL, NULL, NULL),
(376, 16, 13, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(377, 18, 11, 26, 1, '', 1, NULL, NULL, NULL, NULL),
(378, 18, 11, 6, 1, '', 1, NULL, NULL, NULL, NULL),
(379, 18, 11, 28, 2, '', 1, NULL, NULL, NULL, NULL),
(380, 18, 11, 7, 2, '', 1, NULL, NULL, NULL, NULL),
(381, 18, 11, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(382, 18, 11, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(383, 18, 11, 29, 2, '', 1, NULL, NULL, NULL, NULL),
(384, 18, 11, 10, 3, '', 1, NULL, NULL, NULL, NULL),
(385, 18, 11, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(386, 18, 11, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(387, 18, 11, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(388, 18, 11, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(389, 18, 11, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(390, 18, 11, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(391, 18, 11, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(392, 18, 11, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(393, 18, 11, 17, 3, '', 1, NULL, NULL, NULL, NULL),
(394, 18, 11, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(395, 18, 11, 21, 3, '', 1, NULL, NULL, NULL, NULL),
(396, 18, 11, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(397, 18, 11, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(398, 18, 11, 24, 3, '', 1, NULL, NULL, NULL, NULL),
(399, 18, 11, 30, 1, '', 1, NULL, NULL, NULL, NULL),
(400, 18, 11, 27, 3, '', 1, NULL, NULL, NULL, NULL),
(401, 18, 11, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(402, 19, 6, 26, 1, '', 1, NULL, NULL, NULL, NULL),
(403, 19, 6, 6, 1, '', 1, NULL, NULL, NULL, NULL),
(404, 19, 6, 28, 1, '', 1, NULL, NULL, NULL, NULL),
(405, 19, 6, 7, 1, '', 1, NULL, NULL, NULL, NULL),
(406, 19, 6, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(407, 19, 6, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(408, 19, 6, 29, 1, '', 1, NULL, NULL, NULL, NULL),
(409, 19, 6, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(410, 19, 6, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(411, 19, 6, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(412, 19, 6, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(413, 19, 6, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(414, 19, 6, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(415, 19, 6, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(416, 19, 6, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(417, 19, 6, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(418, 19, 6, 17, 1, '', 1, NULL, NULL, NULL, NULL),
(419, 19, 6, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(420, 19, 6, 21, 1, '', 1, NULL, NULL, NULL, NULL),
(421, 19, 6, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(422, 19, 6, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(423, 19, 6, 24, 1, '', 1, NULL, NULL, NULL, NULL),
(424, 19, 6, 30, 1, '', 1, NULL, NULL, NULL, NULL),
(425, 19, 6, 27, 1, '', 1, NULL, NULL, NULL, NULL),
(426, 19, 6, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(427, 21, 5, 26, 2, '', 1, NULL, NULL, NULL, NULL),
(428, 21, 5, 6, 1, '', 1, NULL, NULL, NULL, NULL),
(429, 21, 5, 28, 1, '', 1, NULL, NULL, NULL, NULL),
(430, 21, 5, 7, 1, '', 1, NULL, NULL, NULL, NULL),
(431, 21, 5, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(432, 21, 5, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(433, 21, 5, 29, 1, '', 1, NULL, NULL, NULL, NULL),
(434, 21, 5, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(435, 21, 5, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(436, 21, 5, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(437, 21, 5, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(438, 21, 5, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(439, 21, 5, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(440, 21, 5, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(441, 21, 5, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(442, 21, 5, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(443, 21, 5, 17, 1, '', 1, NULL, NULL, NULL, NULL),
(444, 21, 5, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(445, 21, 5, 21, 1, '', 1, NULL, NULL, NULL, NULL),
(446, 21, 5, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(447, 21, 5, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(448, 21, 5, 30, 1, '', 1, NULL, NULL, NULL, NULL),
(449, 21, 5, 27, 1, '', 1, NULL, NULL, NULL, NULL),
(450, 21, 5, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(451, 20, 7, 26, 2, '', 1, NULL, NULL, NULL, NULL),
(452, 20, 7, 6, 1, '', 1, NULL, NULL, NULL, NULL),
(453, 20, 7, 28, 1, '', 1, NULL, NULL, NULL, NULL),
(454, 20, 7, 7, 1, '', 1, NULL, NULL, NULL, NULL),
(455, 20, 7, 8, 1, '', 1, NULL, NULL, NULL, NULL),
(456, 20, 7, 9, 1, '', 1, NULL, NULL, NULL, NULL),
(457, 20, 7, 29, 1, '', 1, NULL, NULL, NULL, NULL),
(458, 20, 7, 10, 1, '', 1, NULL, NULL, NULL, NULL),
(459, 20, 7, 11, 1, '', 1, NULL, NULL, NULL, NULL),
(460, 20, 7, 12, 1, '', 1, NULL, NULL, NULL, NULL),
(461, 20, 7, 13, 1, '', 1, NULL, NULL, NULL, NULL),
(462, 20, 7, 14, 1, '', 1, NULL, NULL, NULL, NULL),
(463, 20, 7, 15, 1, '', 1, NULL, NULL, NULL, NULL),
(464, 20, 7, 16, 1, '', 1, NULL, NULL, NULL, NULL),
(465, 20, 7, 18, 1, '', 1, NULL, NULL, NULL, NULL),
(466, 20, 7, 19, 1, '', 1, NULL, NULL, NULL, NULL),
(467, 20, 7, 17, 1, '', 1, NULL, NULL, NULL, NULL),
(468, 20, 7, 20, 1, '', 1, NULL, NULL, NULL, NULL),
(469, 20, 7, 21, 1, '', 1, NULL, NULL, NULL, NULL),
(470, 20, 7, 22, 1, '', 1, NULL, NULL, NULL, NULL),
(471, 20, 7, 23, 1, '', 1, NULL, NULL, NULL, NULL),
(472, 20, 7, 30, 1, '', 1, NULL, NULL, NULL, NULL),
(473, 20, 7, 27, 1, '', 1, NULL, NULL, NULL, NULL),
(474, 20, 7, 25, 1, '', 1, NULL, NULL, NULL, NULL),
(523, 1, 12, 26, 2, 'anu', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(524, 1, 12, 6, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(525, 1, 12, 28, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(526, 1, 12, 7, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(527, 1, 12, 8, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(528, 1, 12, 9, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(529, 1, 12, 29, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(530, 1, 12, 10, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(531, 1, 12, 11, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(532, 1, 12, 12, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(533, 1, 12, 13, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(534, 1, 12, 14, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(535, 1, 12, 15, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(536, 1, 12, 16, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(537, 1, 12, 18, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(538, 1, 12, 19, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(539, 1, 12, 17, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(540, 1, 12, 20, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(541, 1, 12, 21, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(542, 1, 12, 22, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(543, 1, 12, 23, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(544, 1, 12, 30, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(545, 1, 12, 27, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23'),
(546, 1, 12, 25, 1, '', 1, '5', '5', '2021-07-08 21:59:23', '2021-07-08 21:59:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `audit_trail`
--

CREATE TABLE `audit_trail` (
  `id` int(11) NOT NULL,
  `user` varchar(50) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `data` text DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `response` varchar(255) DEFAULT NULL,
  `activity` varchar(255) DEFAULT NULL,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `finance`
--

CREATE TABLE `finance` (
  `id` int(11) NOT NULL,
  `periode` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  `type` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `finance`
--

INSERT INTO `finance` (`id`, `periode`, `date`, `type`, `amount`, `description`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(1, 'W-001', '2020-02-06 00:00:00', 2, 200000, 'Uang Kas Bulanan', 1, '5', '5', '2021-06-13 12:53:01', '2021-06-13 12:53:01'),
(2, 'W-001', '2020-02-07 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 12:53:35', '2021-06-13 12:53:35'),
(3, 'W-001', '2020-02-14 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 12:53:51', '2021-06-13 12:53:51'),
(4, 'W-001', '2020-02-19 00:00:00', 2, 310000, 'Investasi Pengurus Workshop Tahun 2019', 1, '5', '5', '2021-06-13 12:54:34', '2021-06-13 12:54:34'),
(5, 'W-001', '2020-02-21 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 12:55:09', '2021-06-13 12:55:09'),
(6, 'W-001', '2020-02-21 00:00:00', 1, 310000, 'Beli Pizza Hut', 1, '5', '5', '2021-06-13 12:55:45', '2021-06-13 12:55:45'),
(7, 'W-001', '2020-02-28 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 12:56:16', '2021-06-13 12:56:16'),
(8, 'W-001', '2020-03-05 00:00:00', 2, 200000, 'Uang Kas Workshop', 1, '5', '5', '2021-06-13 12:57:11', '2021-06-13 12:57:11'),
(9, 'W-001', '2020-03-06 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 12:57:45', '2021-06-13 12:57:45'),
(10, 'W-001', '2020-03-13 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 12:58:12', '2021-06-13 12:58:12'),
(11, 'W-001', '2020-06-05 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 12:58:20', '2021-06-13 12:58:20'),
(12, 'W-001', '2020-06-12 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 12:58:25', '2021-06-13 12:58:25'),
(13, 'W-001', '2020-06-18 00:00:00', 2, 180000, 'Uang Kas Workshop', 1, '5', '5', '2021-06-13 12:59:22', '2021-06-13 12:59:22'),
(14, 'W-001', '2020-06-19 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:00:02', '2021-06-13 13:00:02'),
(15, 'W-001', '2020-06-26 00:00:00', 1, 75000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:00:16', '2021-06-13 13:00:16'),
(16, 'W-001', '2020-07-09 00:00:00', 2, 180000, 'Uang Kas Workshop', 1, '5', '5', '2021-06-13 13:00:52', '2021-06-13 13:00:52'),
(17, 'W-001', '2020-07-10 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:01:24', '2021-06-13 13:01:24'),
(18, 'W-001', '2020-07-17 00:00:00', 1, 100000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:01:39', '2021-06-13 13:01:39'),
(19, 'W-001', '2020-07-23 00:00:00', 2, 180000, 'Uang Kas Workshop', 1, '5', '5', '2021-06-13 13:02:04', '2021-06-13 13:02:30'),
(20, 'W-001', '2020-07-24 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:03:39', '2021-06-13 13:03:39'),
(21, 'W-001', '2020-08-14 00:00:00', 1, 60000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:04:28', '2021-06-13 13:04:28'),
(22, 'W-001', '2020-08-28 00:00:00', 1, 60000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:04:38', '2021-06-13 13:04:38'),
(23, 'W-001', '2020-10-15 00:00:00', 2, 200000, 'Uang Kas Workshop', 1, '5', '5', '2021-06-13 13:05:06', '2021-06-13 13:05:06'),
(24, 'W-001', '2020-10-16 00:00:00', 1, 100000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:05:31', '2021-06-13 13:05:31'),
(25, 'W-001', '2020-10-23 00:00:00', 1, 25000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:05:46', '2021-06-13 13:05:46'),
(26, 'W-001', '2020-11-06 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:06:34', '2021-06-13 13:06:34'),
(27, 'W-001', '2020-11-20 00:00:00', 1, 100000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:06:48', '2021-06-13 13:06:48'),
(28, 'W-001', '2020-11-26 00:00:00', 2, 200000, 'Uang Kas Workshop', 1, '5', '5', '2021-06-13 13:07:14', '2021-06-13 13:07:14'),
(29, 'W-001', '2020-11-27 00:00:00', 1, 100000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:07:33', '2021-06-13 13:07:33'),
(30, 'W-001', '2020-12-11 00:00:00', 1, 50000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:07:50', '2021-06-13 13:07:50'),
(31, 'W-001', '2021-01-07 00:00:00', 2, 130000, 'Uang Kas Workshop', 1, '5', '5', '2021-06-13 13:08:21', '2021-06-13 13:08:21'),
(32, 'W-001', '2021-01-08 00:00:00', 1, 60000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:08:53', '2021-06-13 13:08:53'),
(33, 'W-001', '2021-02-26 00:00:00', 1, 60000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:09:12', '2021-06-13 13:09:12'),
(34, 'W-001', '2021-03-10 00:00:00', 2, 200000, 'Uang Kas Workshop', 1, '5', '5', '2021-06-13 13:09:41', '2021-06-13 13:09:41'),
(35, 'W-001', '2021-03-26 00:00:00', 1, 60000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:10:30', '2021-06-13 13:10:30'),
(36, 'W-001', '2021-04-09 00:00:00', 1, 65000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:10:48', '2021-06-13 13:10:48'),
(37, 'W-001', '2021-05-21 00:00:00', 1, 60000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:11:11', '2021-06-13 13:11:11'),
(38, 'W-001', '2021-05-27 00:00:00', 2, 100000, 'Uang Kas Workshop', 1, '5', '5', '2021-06-13 13:11:34', '2021-06-13 13:11:34'),
(39, 'W-001', '2021-05-28 00:00:00', 1, 70000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:12:28', '2021-06-13 13:12:28'),
(40, 'W-001', '2021-06-04 00:00:00', 1, 60000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:12:48', '2021-06-13 13:12:48'),
(41, 'W-001', '2021-06-11 00:00:00', 1, 60000, 'Konsumsi Workshop', 1, '5', '5', '2021-06-13 13:13:03', '2021-06-13 13:13:03'),
(42, 'W-001', '2021-06-11 00:00:00', 2, 200000, 'Uang Kas Workshop', 1, '5', '5', '2021-06-13 13:13:21', '2021-06-13 13:13:21');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kotak_saran`
--

CREATE TABLE `kotak_saran` (
  `id` int(11) NOT NULL,
  `pengirim` int(11) NOT NULL,
  `penerima` int(11) NOT NULL,
  `pesan` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `kotak_saran`
--

INSERT INTO `kotak_saran` (`id`, `pengirim`, `penerima`, `pesan`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(1, 13, 5, 'TESTING', 1, NULL, NULL, NULL, NULL),
(2, 25, 5, 'Skor kelamaan tampilnya', 1, NULL, NULL, NULL, NULL),
(3, 22, 5, 'tolong foto saya tidak muncul padahal sudah update', 1, NULL, NULL, NULL, NULL),
(4, 10, 5, 'bib dashboard nya agak kelamaan nih', 1, NULL, NULL, NULL, NULL),
(5, 25, 5, 'User guide', 1, NULL, NULL, NULL, NULL),
(6, 25, 5, 'Edit user masih semua\r\n', 1, NULL, NULL, NULL, NULL),
(7, 25, 5, 'Skor benerin kalo mau pake animasi, default jangan nol', 1, NULL, NULL, NULL, NULL),
(8, 11, 5, 'Mantoel kalian', 1, NULL, NULL, NULL, NULL),
(9, 26, 5, 'hallo kaka,\r\n\r\nbagaimana kabarnya?', 1, NULL, NULL, NULL, NULL),
(10, 26, 5, 'Jam kedatangan terhitung dari mana?\r\npintu depan Sagara atak dari mana?', 1, NULL, NULL, NULL, NULL),
(11, 10, 5, 'Habib kalo abis workshop score di bacakan oleh MC\r\n', 1, NULL, NULL, NULL, NULL),
(12, 26, 5, 'Makanannya jangan gorengan terus\r\n', 1, NULL, NULL, NULL, NULL),
(13, 11, 5, 'mantab', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `periode`
--

CREATE TABLE `periode` (
  `id` int(11) NOT NULL,
  `code` varchar(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `periode`
--

INSERT INTO `periode` (`id`, `code`, `name`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(1, 'W-001', 'PANDEMI', 1, '5', '5', '2021-06-13 10:09:29', '2021-06-13 16:09:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `previleges`
--

CREATE TABLE `previleges` (
  `id` int(11) NOT NULL,
  `module` varchar(255) DEFAULT NULL,
  `submodule` varchar(255) DEFAULT NULL,
  `ordering` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `uri` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `previleges`
--

INSERT INTO `previleges` (`id`, `module`, `submodule`, `ordering`, `action`, `uri`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(10, 'Admin', 'Admin', '1', 'all', '*', 1, NULL, NULL, '2020-02-13 18:09:24', '2020-02-13 18:09:24'),
(11, 'dashboard', 'dashboard', '1', 'dashboard', '/', 1, '5', '5', '2020-02-14 15:22:35', '2020-02-14 15:22:35'),
(12, 'User', 'nilai', '1', 'create', '/table_nilai/null/create', 1, '5', '5', '2020-02-15 13:47:47', '2020-02-15 13:51:21'),
(13, 'User', 'nilai', '2', 'update', '/table_nilai/:id/update', 1, '5', '5', '2020-02-15 13:48:11', '2020-02-15 13:51:12'),
(14, 'User', 'nilai', '3', 'delete', '/table_nilai/:id/delete', 1, '5', '5', '2020-02-15 13:48:44', '2020-02-15 13:51:03'),
(15, 'User', 'nilai', '4', 'search', '/table_nilai', 1, '5', '5', '2020-02-15 13:48:59', '2020-02-15 13:50:44'),
(16, 'User', 'nilai', '5', 'read', '/table_nilai/:id', 1, '5', '5', '2020-02-15 13:49:32', '2020-02-15 13:50:52'),
(17, 'User', 'keuangan', '1', 'search', '/table_keuangan', 1, '5', '5', '2020-02-15 13:51:52', '2020-02-15 13:52:08'),
(18, 'User', 'keuangan', '2', 'read', '/table_keuangan/:id', 1, '5', '5', '2020-02-15 13:52:27', '2020-02-15 13:52:27'),
(19, 'User', 'keuangan', '3', 'update', '/table_keuangan/:id/update', 1, '5', '5', '2020-02-15 13:52:41', '2020-02-15 13:52:41'),
(20, 'User', 'keuangan', '4', 'delete', '/table_keuangan/:id/delete', 1, '5', '5', '2020-02-15 13:52:50', '2020-02-15 13:53:01'),
(21, 'User', 'keuangan', '5', 'create', '/table_keuangan/null/create', 1, '5', '5', '2020-02-15 13:53:23', '2020-02-15 13:53:44'),
(22, 'User', 'users', '1', 'create', '/user/null/create', 1, '5', '5', '2020-02-15 14:02:27', '2020-02-15 14:02:27'),
(23, 'User', 'users', '2', 'read', '/user/:id', 1, '5', '5', '2020-02-15 14:02:51', '2020-02-15 14:02:51'),
(24, 'User', 'users', '3', 'update', '/user/:id/update', 1, '5', '5', '2020-02-15 14:03:00', '2020-02-15 14:03:24'),
(25, 'User', 'users', '4', 'delete', '/user/:id/delete', 1, '5', '5', '2020-02-15 14:03:07', '2020-02-15 14:03:33'),
(26, 'User', 'users', '5', 'search', '/user', 1, '5', '5', '2020-02-15 14:03:53', '2020-02-15 14:03:53'),
(27, 'User', 'jadwal', '1', 'search', '/table_jadwal', 1, '5', '5', '2020-02-19 10:37:08', '2020-02-19 11:49:36'),
(28, 'User', 'jadwal', '2', 'update', '/table_jadwal/:id/update', 1, '5', '5', '2020-02-19 10:37:25', '2020-02-19 11:49:44'),
(29, 'User', 'jadwal', '3', 'create', '/table_jadwal/null/create', 1, '5', '5', '2020-02-19 10:37:50', '2020-02-19 11:49:51'),
(30, 'User', 'jadwal', '4', 'delete', '/table_jadwal/:id/delete', 1, '5', '5', '2020-02-19 10:38:02', '2020-02-19 11:49:59'),
(31, 'User', 'jadwal', '5', 'read', '/table_jadwal/:id', 1, '5', '5', '2020-02-19 10:38:15', '2020-02-19 11:50:05'),
(32, 'User', 't & C', '1', 'read', '/tnc', 1, '5', '5', '2020-02-19 15:22:33', '2020-02-19 15:22:33'),
(33, 'User', 'saran', '1', 'listing', '/kotak_saran', 1, '5', '5', '2020-02-20 07:28:13', '2021-07-07 08:04:04'),
(34, 'User', 'saran', '2', 'prosess', '/saran', 0, '5', '5', '2020-02-20 07:28:31', '2021-07-07 08:08:10'),
(35, 'User', 'jadwal', '6', 'datatable', '/table_jadwal/null/data_table', 1, '5', '5', '2021-07-07 07:53:50', '2021-07-07 07:53:50'),
(36, 'User', 'jadwal', '7', 'change title', '/table_jadwal/change_title/:id', 1, '5', '5', '2021-07-07 07:54:59', '2021-07-07 07:54:59'),
(37, 'User', 'saran', '2', 'detail', '/kotak_saran/:id', 1, '5', '5', '2021-07-07 08:04:40', '2021-07-07 08:04:40'),
(38, 'User', 'saran', '3', 'trash', '/kotak_saran/:id/trash', 1, '5', '5', '2021-07-07 08:05:34', '2021-07-07 08:05:34'),
(39, 'User', 'saran', '4', 'create', '/kotak_saran/null/create', 1, '5', '5', '2021-07-07 08:05:59', '2021-07-07 08:05:59'),
(40, 'User', 'score', '1', 'listing', '/table_nilai', 1, '5', '5', '2021-07-07 08:06:39', '2021-07-07 08:06:39'),
(41, 'User', 'score', '2', 'datatable', '/table_nilai/null/data_table', 1, '5', '5', '2021-07-07 08:07:17', '2021-07-07 08:07:17'),
(42, 'User', 'saran', '5', 'datatable', '/kotak_saran/null/data_table', 1, '5', '5', '2021-07-07 08:08:53', '2021-07-07 08:08:53'),
(43, 'User', 'Absensi Participant', '1', 'listing', '/absensi_participants', 1, '5', '5', '2021-07-08 09:33:03', '2021-07-08 09:39:46'),
(44, 'User', 'Absensi Participant', '2', 'detail', '/absensi_participants/:id', 1, '5', '5', '2021-07-08 09:33:18', '2021-07-08 09:39:54'),
(45, 'User', 'Absensi Participant', '3', 'update', '/absensi_participants/:id/update', 1, '5', '5', '2021-07-08 09:33:27', '2021-07-08 09:40:02'),
(46, 'User', 'Absensi Participant', '4', 'delete', '/absensi_participants/:id/delete', 1, '5', '5', '2021-07-08 09:33:38', '2021-07-08 09:40:08'),
(47, 'User', 'Absensi Participant', '5', 'create', '/absensi_participants/null/create', 1, '5', '5', '2021-07-08 09:33:54', '2021-07-08 09:40:17'),
(48, 'User', 'Absensi Participant', '6', 'datatable', '/absensi_participants/null/data_table', 1, '5', '5', '2021-07-08 09:34:12', '2021-07-08 09:40:24'),
(49, 'User', 'Absensi Participant', '7', 'trash', '/absensi_participants/:id/trash', 1, '5', '5', '2021-07-08 09:34:34', '2021-07-08 09:40:32'),
(50, 'User', 'Absensi Participant', '8', 'trash list', '/absensi_participants/null/trashed', 1, '5', '5', '2021-07-08 09:35:04', '2021-07-08 09:40:39'),
(51, 'User', 'Absensi Participant', '9', 'export excel', '/absensi_participants/null/export_excel', 1, '5', '5', '2021-07-08 09:49:58', '2021-07-08 09:49:58'),
(52, 'User', 'score', '3', 'export excel', '/table_nilai/null/export_excel', 1, '5', '5', '2021-07-08 09:51:03', '2021-07-08 09:51:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `reporting`
--

CREATE TABLE `reporting` (
  `id` int(11) NOT NULL,
  `periode` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `total_terlambat` varchar(100) NOT NULL,
  `denda_keterlambatan` varchar(100) NOT NULL,
  `denda_tidak_siap` varchar(100) NOT NULL,
  `denda_akumulasi_nilai` varchar(100) NOT NULL,
  `total_denda` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `reporting`
--

INSERT INTO `reporting` (`id`, `periode`, `nama`, `total_terlambat`, `denda_keterlambatan`, `denda_tidak_siap`, `denda_akumulasi_nilai`, `total_denda`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(1, 'W-001', 'Apriyanto Pramana Putra', '7', '35,000', '0', '0', '35,000', 1, '5', '5', '2021-07-06 10:08:45', '2021-07-06 10:08:45'),
(2, 'W-001', 'Dewa Rajasa Katong', '11', '55,000', '0', '0', '55,000', 1, '5', '5', '2021-07-06 10:08:45', '2021-07-06 10:08:45'),
(3, 'W-001', 'Dewi Purwati Ningsih', '0', '0', '0', '0', '0', 1, '5', '5', '2021-07-06 10:08:45', '2021-07-06 10:08:45'),
(4, 'W-001', 'Dio Allan Kosasih', '1', '5,000', '0', '0', '5,000', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(5, 'W-001', 'Faisal Firaz', '4', '20,000', '0', '0', '20,000', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(6, 'W-001', 'Farid Hidayat', '3', '15,000', '0', '0', '15,000', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(7, 'W-001', 'Ganesa Dwi Muharso', '0', '0', '0', '0', '0', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(8, 'W-001', 'Habib Udin', '0', '0', '0', '0', '0', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(9, 'W-001', 'Iqlima Iqlima', '2', '10,000', '0', '0', '10,000', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(10, 'W-001', 'Januar Siregar', '4', '20,000', '0', '0', '20,000', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(11, 'W-001', 'Klara Puspita', '0', '0', '0', '0', '0', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(12, 'W-001', 'Muhammad Nurdin', '5', '25,000', '0', '0', '25,000', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(13, 'W-001', 'M Rizky Avesena', '0', '0', '0', '0', '0', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(14, 'W-001', 'Moch Ichsan', '3', '15,000', '0', '0', '15,000', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(15, 'W-001', 'Nur Alam', '0', '0', '0', '0', '0', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(16, 'W-001', 'Pendi Setiawan', '5', '25,000', '0', '0', '25,000', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(17, 'W-001', 'Ririn Risda Pangestu', '5', '25,000', '0', '0', '25,000', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(18, 'W-001', 'Rosita Dewi', '0', '0', '0', '0', '0', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(19, 'W-001', 'Wahyu Taufik', '0', '0', '0', '0', '0', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(20, 'W-001', 'Abdul Rasman', '9', '45,000', '0', '0', '45,000', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(21, 'W-001', 'Subhan Toba', '0', '0', '0', '0', '0', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(22, 'W-001', 'Bayu Eka', '5', '25,000', '0', '5,000', '30,000', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(23, 'W-001', 'Doni Rahmat', '2', '10,000', '0', '0', '10,000', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46'),
(24, 'W-001', 'Sony Setiawan', '0', '0', '0', '0', '0', 1, '5', '5', '2021-07-06 10:08:46', '2021-07-06 10:08:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `role`
--

INSERT INTO `role` (`id`, `name`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(5, 'Admin', 1, NULL, NULL, '2020-02-13 18:07:48', '2020-02-13 18:10:03'),
(6, 'User', 1, NULL, '5', '2020-02-13 18:09:47', '2021-07-08 09:51:17'),
(7, 'keuangan', 1, '5', '5', '2020-02-15 16:13:02', '2020-02-15 16:13:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_previleges`
--

CREATE TABLE `role_previleges` (
  `id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `rule` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `role_previleges`
--

INSERT INTO `role_previleges` (`id`, `role`, `type`, `rule`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(25, '5', 'uri', '*', 1, NULL, NULL, '2020-02-13 18:10:03', '2020-02-13 18:10:03'),
(42, '7', 'uri', '/table_keuangan', 1, '5', '5', '2020-02-15 16:13:02', '2020-02-15 16:13:02'),
(43, '7', 'uri', '/table_keuangan/:id', 1, '5', '5', '2020-02-15 16:13:02', '2020-02-15 16:13:02'),
(44, '7', 'uri', '/table_keuangan/:id/update', 1, '5', '5', '2020-02-15 16:13:02', '2020-02-15 16:13:02'),
(45, '7', 'uri', '/table_keuangan/:id/delete', 1, '5', '5', '2020-02-15 16:13:02', '2020-02-15 16:13:02'),
(46, '7', 'uri', '/table_keuangan/null/create', 1, '5', '5', '2020-02-15 16:13:02', '2020-02-15 16:13:02'),
(155, '6', 'uri', '/', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(156, '6', 'uri', '/table_nilai', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(157, '6', 'uri', '/user/:id', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(158, '6', 'uri', '/user/:id/update', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(159, '6', 'uri', '/user', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(160, '6', 'uri', '/table_jadwal', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(161, '6', 'uri', '/table_jadwal/null/data_table', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(162, '6', 'uri', '/table_jadwal/change_title/:id', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(163, '6', 'uri', '/tnc', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(164, '6', 'uri', '/kotak_saran', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(165, '6', 'uri', '/saran', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(166, '6', 'uri', '/kotak_saran/:id', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(167, '6', 'uri', '/kotak_saran/:id/trash', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(168, '6', 'uri', '/kotak_saran/null/create', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(169, '6', 'uri', '/kotak_saran/null/data_table', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(170, '6', 'uri', '/table_nilai', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(171, '6', 'uri', '/table_nilai/null/data_table', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(172, '6', 'uri', '/table_nilai/null/export_excel', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(173, '6', 'uri', '/absensi_participants', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(174, '6', 'uri', '/absensi_participants/null/data_table', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17'),
(175, '6', 'uri', '/absensi_participants/null/export_excel', 1, '5', '5', '2021-07-08 09:51:17', '2021-07-08 09:51:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sequence`
--

CREATE TABLE `sequence` (
  `id` int(11) NOT NULL,
  `table_name` varchar(50) NOT NULL,
  `year` int(11) NOT NULL,
  `sequence` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `sequence`
--

INSERT INTO `sequence` (`id`, `table_name`, `year`, `sequence`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(1, 'Periode', 2021, 1, 1, '5', '5', '2021-06-13 10:09:29', '2021-06-13 10:09:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `slogan`
--

CREATE TABLE `slogan` (
  `id` int(11) NOT NULL,
  `kata_motovasi` varchar(150) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sysparam`
--

CREATE TABLE `sysparam` (
  `id` int(11) NOT NULL,
  `groups` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `long_value` text DEFAULT NULL,
  `order_param` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `_created_by` int(11) DEFAULT NULL,
  `_updated_by` int(11) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `sysparam`
--

INSERT INTO `sysparam` (`id`, `groups`, `key`, `value`, `long_value`, `order_param`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(6, 'gender', '1', 'Laki-Laki', 'Laki-Laki', 0, 1, 5, 5, '2020-02-13 20:17:35', '2020-02-13 20:17:35'),
(7, 'gender', '2', 'Perempuan', 'Perempuan', 0, 1, 5, 5, '2020-02-13 20:17:54', '2020-02-13 20:17:54'),
(8, 'semester', '1', 'Ganjil', 'Ganjil', 0, 1, 5, 5, '2020-02-13 20:30:31', '2020-02-13 20:30:52'),
(9, 'semester', '2', 'Genap', 'Genap', 0, 1, 5, 5, '2020-02-13 20:30:44', '2020-02-13 20:30:44'),
(10, 'level', '1', 'Manager', 'Manager', 0, 1, 5, 5, '2020-02-14 09:09:32', '2020-02-14 09:09:32'),
(11, 'level', '2', 'Karyawan', 'Karyawan', 0, 1, 5, 5, '2020-02-14 09:09:44', '2020-02-14 09:09:44'),
(12, 'keuangan', '1', 'Pemasukan', 'Pemasukan', 0, 1, 5, 5, '2020-02-14 15:29:05', '2020-02-14 15:29:05'),
(13, 'keuangan', '2', 'Pengeluaran', 'Pengeluaran', 0, 1, 5, 5, '2020-02-14 15:29:22', '2020-02-14 15:29:22'),
(14, 'status_kehadiran', '1', 'On Time', 'On Time', 1, 1, 5, 5, '2020-03-06 08:41:55', '2020-03-06 08:41:55'),
(15, 'status_kehadiran', '2', 'Telat', 'Telat', 2, 1, 5, 5, '2020-03-06 08:42:20', '2020-03-06 08:42:20'),
(16, 'status_kehadiran', '3', 'Tidak Masuk', 'Tidak Masuk', 3, 1, 5, 5, '2020-03-06 08:42:34', '2020-03-06 08:42:34'),
(17, 'status_kehadiran', '4', 'Berkepentingan', 'Berkepentingan (Telat/Tidak Masuk)', 4, 1, 5, 5, '2021-01-08 08:29:04', '2021-01-08 08:29:04'),
(18, 'finance', '1', 'DEBIT', 'Pengeluaran', 1, 1, 5, 5, '2021-06-13 12:44:10', '2021-06-13 12:51:04'),
(19, 'finance', '2', 'CREDIT', 'Pemasukan', 2, 1, 5, 5, '2021-06-13 12:44:19', '2021-06-13 12:50:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_jadwal`
--

CREATE TABLE `table_jadwal` (
  `id` int(11) NOT NULL,
  `no_urut` int(11) NOT NULL,
  `nama` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tahun` int(11) NOT NULL,
  `periode` varchar(11) NOT NULL,
  `tanggal` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `table_jadwal`
--

INSERT INTO `table_jadwal` (`id`, `no_urut`, `nama`, `judul`, `tahun`, `periode`, `tanggal`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(1, 1, 12, 'Tuhan: Realitas Tunggal', 2020, 'W-001', '2020-02-14 00:00:00', 1, '5', '5', '2020-02-19 07:16:25', '2020-02-19 07:16:25'),
(2, 2, 24, 'Mental Disorder', 2020, 'W-001', '2020-02-21 00:00:00', 1, '5', '5', '2020-02-19 07:17:56', '2020-02-19 15:29:21'),
(3, 3, 26, 'ADL #4', 2020, 'W-001', '2020-02-28 00:00:00', 1, '5', '26', '2020-02-19 07:18:25', '2020-02-24 10:01:58'),
(4, 4, 17, 'Pengecut', 2020, 'W-001', '2020-03-06 00:00:00', 1, '5', '5', '2020-02-19 07:18:36', '2020-03-06 23:17:42'),
(5, 5, 16, 'Psikoanalisis', 2020, 'W-001', '2020-03-13 00:00:00', 1, '5', '5', '2020-02-19 07:20:10', '2020-03-14 16:34:21'),
(6, 6, 22, 'self care', 2020, 'W-001', '2020-06-12 00:00:00', 1, '5', '22', '2020-02-19 07:20:54', '2020-06-15 13:58:36'),
(7, 7, 10, 'bone', 2020, 'W-001', '2020-06-19 00:00:00', 1, '5', '10', '2020-02-19 07:21:03', '2020-06-17 05:28:32'),
(8, 8, 8, 'Gaya', 2020, 'W-001', '2020-07-03 00:00:00', 1, '5', '5', '2020-02-19 07:21:11', '2020-07-10 13:06:42'),
(9, 9, 19, 'Plus Minus (+ -)', 2020, 'W-001', '2020-07-10 00:00:00', 1, '5', '5', '2020-02-19 07:21:23', '2020-07-10 12:56:20'),
(10, 17, 11, 'Bisnis Model Canvas', 2020, 'W-001', NULL, 1, '5', '5', '2020-02-19 07:21:33', '2020-11-06 09:01:18'),
(11, 11, 20, 'Doa', 2020, 'W-001', NULL, 1, '5', '20', '2020-02-19 07:21:44', '2020-07-20 22:44:41'),
(12, 12, 15, 'Memasak', 2020, 'W-001', NULL, 1, '5', '5', '2020-02-19 07:21:53', '2020-10-12 16:04:55'),
(13, 13, 9, 'Alam Bawah Sadar', 2020, 'W-001', NULL, 1, '5', '9', '2020-02-19 07:22:02', '2020-07-22 09:55:44'),
(14, 14, 21, 'testing judul', 2020, 'W-001', NULL, 1, '5', '5', '2020-02-19 07:22:18', '2021-07-07 07:45:49'),
(15, 15, 23, '', 2020, 'W-001', NULL, 1, '5', '5', '2020-02-19 07:22:27', '2020-02-19 07:22:27'),
(16, 16, 13, 'Lughotul Arobiyah', 2020, 'W-001', NULL, 1, '5', '5', '2020-02-19 07:22:33', '2020-12-04 21:49:55'),
(17, 10, 14, 'Belajar', 2020, 'W-001', '2020-07-17 00:00:00', 1, '5', '14', '2020-02-19 07:22:42', '2020-07-19 17:58:48'),
(18, 18, 6, 'Putra Part I', 2020, 'W-001', NULL, 1, '5', '6', '2020-02-19 07:22:52', '2020-02-20 11:04:42'),
(19, 19, 7, '', 2020, 'W-001', NULL, 1, '5', '5', '2020-02-19 07:23:00', '2020-02-19 07:23:00'),
(20, 20, 25, '', 2020, 'W-001', NULL, 1, '5', '5', '2020-02-19 07:23:09', '2020-02-19 07:23:09'),
(21, 25, 18, '', 2020, 'W-001', NULL, 1, '5', '5', '2020-02-19 07:23:20', '2020-11-06 08:58:29'),
(24, 24, 27, '', 2020, 'W-001', NULL, 1, '5', '5', '2020-02-19 18:02:45', '2020-11-06 08:58:29'),
(25, 23, 28, '', 2020, 'W-001', NULL, 1, '5', '5', '2020-02-27 19:59:20', '2020-08-12 09:41:19'),
(26, 21, 29, '', 2020, 'W-001', NULL, 1, '5', '5', '2020-08-12 09:40:28', '2020-08-12 09:40:59'),
(27, 22, 30, '', 2020, 'W-001', NULL, 1, '5', '5', '2020-11-06 08:56:24', '2020-11-06 08:57:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_keuangan`
--

CREATE TABLE `table_keuangan` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `type` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `saldo` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `table_keuangan`
--

INSERT INTO `table_keuangan` (`id`, `date`, `type`, `jumlah`, `keterangan`, `saldo`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(1, '2020-02-06 00:00:00', 1, 200000, 'Uang Konsumsi Bulanan', 200000, 1, '5', '5', '2020-02-14 20:44:52', '2020-02-14 20:45:55'),
(2, '2020-02-07 00:00:00', 2, 50000, 'Beli Gorengan', 150000, 1, '5', '5', '2020-02-14 20:49:14', '2020-02-14 20:49:14'),
(3, '2020-02-14 00:00:00', 2, 50000, 'Beli Gorengan', 100000, 1, '5', '5', '2020-02-14 20:57:57', '2020-02-14 20:57:57'),
(4, '2020-02-19 00:00:00', 1, 310000, 'Inventaris dari pengurus workshop 2019', 410000, 1, '5', '5', '2020-02-19 09:44:46', '2020-02-19 09:44:46'),
(5, '2020-02-21 00:00:00', 2, 50000, 'Beli Gorengan', 360000, 1, '5', '5', '2020-02-21 08:25:39', '2020-02-21 08:27:06'),
(6, '2020-02-21 00:00:00', 2, 310000, 'Beli Pizza Hut', 50000, 1, '5', '5', '2020-02-21 19:32:07', '2020-02-21 19:32:07'),
(7, '2020-02-27 00:00:00', 2, 50000, 'Beli Gorengan', 0, 1, '5', '5', '2020-03-05 09:23:02', '2020-03-06 13:40:22'),
(8, '2020-03-05 00:00:00', 1, 200000, 'Uang Bulanan Workshop', 200000, 1, '5', '5', '2020-03-05 14:42:36', '2020-03-05 14:42:36'),
(9, '2020-03-06 00:00:00', 2, 50000, 'Beli Gorengan', 150000, 1, '5', '5', '2020-03-06 08:21:53', '2020-03-06 08:21:53'),
(10, '2020-03-13 00:00:00', 2, 50000, 'Beli Konsumsi Workshop', 100000, 1, '5', '5', '2020-03-13 08:44:40', '2020-03-13 08:44:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_nilai`
--

CREATE TABLE `table_nilai` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `year` int(11) NOT NULL,
  `periode` varchar(11) NOT NULL,
  `name` int(11) NOT NULL,
  `value` varchar(50) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `table_nilai`
--

INSERT INTO `table_nilai` (`id`, `date`, `year`, `periode`, `name`, `value`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(1, '2020-02-14 00:00:00', 2020, 'W-001', 6, '3', 1, '5', '5', '2020-02-14 13:13:17', '2020-02-14 13:13:17'),
(2, '2020-02-14 00:00:00', 2020, 'W-001', 7, '2', 1, '5', '5', '2020-02-14 13:13:41', '2020-02-14 13:13:41'),
(3, '2020-02-14 00:00:00', 2020, 'W-001', 8, '3', 1, '5', '5', '2020-02-14 13:13:52', '2020-02-14 13:13:52'),
(4, '2020-02-14 00:00:00', 2020, 'W-001', 9, '2', 1, '5', '5', '2020-02-14 13:14:03', '2020-02-14 13:14:03'),
(5, '2020-02-14 00:00:00', 2020, 'W-001', 10, '1', 1, '5', '5', '2020-02-14 13:14:12', '2020-02-14 13:14:12'),
(6, '2020-02-14 00:00:00', 2020, 'W-001', 11, '2', 1, '5', '5', '2020-02-14 13:14:27', '2020-02-14 13:14:27'),
(7, '2020-02-14 00:00:00', 2020, 'W-001', 12, '5', 1, '5', '5', '2020-02-14 13:14:37', '2020-02-14 13:14:37'),
(8, '2020-02-14 00:00:00', 2020, 'W-001', 13, '4', 1, '5', '5', '2020-02-14 13:14:48', '2020-02-14 13:14:48'),
(9, '2020-02-14 00:00:00', 2020, 'W-001', 14, '0', 1, '5', '5', '2020-02-14 13:15:01', '2020-02-14 13:15:01'),
(10, '2020-02-14 00:00:00', 2020, 'W-001', 15, '1', 1, '5', '5', '2020-02-14 13:15:18', '2020-02-14 13:15:18'),
(11, '2020-02-14 00:00:00', 2020, 'W-001', 16, '1', 1, '5', '5', '2020-02-14 13:15:26', '2020-02-14 13:15:26'),
(12, '2020-02-14 00:00:00', 2020, 'W-001', 17, '4', 1, '5', '5', '2020-02-14 13:15:37', '2020-02-14 13:15:37'),
(13, '2020-02-14 00:00:00', 2020, 'W-001', 18, '3', 1, '5', '5', '2020-02-14 13:15:49', '2020-02-14 13:15:49'),
(14, '2020-02-14 00:00:00', 2020, 'W-001', 19, '0', 1, '5', '5', '2020-02-14 13:16:00', '2020-02-14 13:16:00'),
(15, '2020-02-14 00:00:00', 2020, 'W-001', 20, '1', 1, '5', '5', '2020-02-14 13:16:16', '2020-02-14 13:16:16'),
(16, '2020-02-14 00:00:00', 2020, 'W-001', 21, '1', 1, '5', '5', '2020-02-14 13:16:23', '2020-02-14 13:16:23'),
(17, '2020-02-14 00:00:00', 2020, 'W-001', 22, '1', 1, '5', '5', '2020-02-14 13:16:27', '2020-02-14 13:16:27'),
(18, '2020-02-14 00:00:00', 2020, 'W-001', 23, '1', 1, '5', '5', '2020-02-14 13:16:34', '2020-02-14 13:16:34'),
(19, '2020-02-14 00:00:00', 2020, 'W-001', 24, '3', 1, '5', '5', '2020-02-14 13:16:40', '2020-02-19 18:52:14'),
(20, '2020-02-14 00:00:00', 2020, 'W-001', 25, '1', 1, '5', '5', '2020-02-14 13:16:51', '2020-02-14 13:16:51'),
(21, '2020-02-14 00:00:00', 2020, 'W-001', 26, '0', 1, '5', '5', '2020-02-14 14:10:19', '2020-02-14 14:13:27'),
(22, '2020-02-14 00:00:00', 2020, 'W-001', 27, '0', 1, '5', '5', '2020-02-19 18:03:55', '2020-02-19 18:03:55'),
(23, '2020-02-21 00:00:00', 2020, 'W-001', 6, '10', 1, '5', '5', '2020-02-21 13:39:51', '2020-02-21 13:39:51'),
(24, '2020-02-21 00:00:00', 2020, 'W-001', 7, '0', 1, '5', '5', '2020-02-21 13:40:24', '2020-02-21 13:40:24'),
(25, '2020-02-21 00:00:00', 2020, 'W-001', 8, '8', 1, '5', '5', '2020-02-21 13:40:39', '2020-02-21 13:40:39'),
(26, '2020-02-21 00:00:00', 2020, 'W-001', 9, '4', 1, '5', '5', '2020-02-21 13:48:43', '2020-02-21 13:48:43'),
(27, '2020-02-21 00:00:00', 2020, 'W-001', 10, '-5', 1, '5', '5', '2020-02-21 13:50:09', '2020-02-21 13:50:09'),
(28, '2020-02-21 00:00:00', 2020, 'W-001', 11, '2', 1, '5', '5', '2020-02-21 13:50:22', '2020-02-21 13:50:22'),
(29, '2020-02-21 00:00:00', 2020, 'W-001', 12, '7', 1, '5', '5', '2020-02-21 13:52:50', '2020-02-21 13:52:50'),
(30, '2020-02-21 00:00:00', 2020, 'W-001', 13, '2', 1, '5', '5', '2020-02-21 13:53:02', '2020-02-21 13:53:02'),
(31, '2020-02-21 00:00:00', 2020, 'W-001', 14, '0', 1, '5', '5', '2020-02-21 13:53:12', '2020-02-21 13:53:12'),
(32, '2020-02-21 00:00:00', 2020, 'W-001', 15, '0', 1, '5', '5', '2020-02-21 13:53:22', '2020-02-21 13:53:22'),
(33, '2020-02-21 00:00:00', 2020, 'W-001', 16, '1', 1, '5', '5', '2020-02-21 13:53:42', '2020-02-21 13:53:42'),
(34, '2020-02-21 00:00:00', 2020, 'W-001', 17, '1', 1, '5', '5', '2020-02-21 13:54:05', '2020-02-21 13:54:05'),
(35, '2020-02-21 00:00:00', 2020, 'W-001', 18, '9', 1, '5', '5', '2020-02-21 13:54:14', '2020-02-21 13:54:14'),
(36, '2020-02-21 00:00:00', 2020, 'W-001', 19, '3', 1, '5', '5', '2020-02-21 13:54:26', '2020-02-21 13:54:26'),
(37, '2020-02-21 00:00:00', 2020, 'W-001', 20, '4', 1, '5', '5', '2020-02-21 13:54:39', '2020-02-21 13:54:39'),
(38, '2020-02-21 00:00:00', 2020, 'W-001', 21, '-5', 1, '5', '5', '2020-02-21 13:54:49', '2020-02-21 13:54:49'),
(39, '2020-02-21 00:00:00', 2020, 'W-001', 22, '1', 1, '5', '5', '2020-02-21 13:55:04', '2020-02-21 13:55:04'),
(40, '2020-02-21 00:00:00', 2020, 'W-001', 23, '3', 1, '5', '5', '2020-02-21 13:55:13', '2020-02-21 13:55:13'),
(41, '2020-02-21 00:00:00', 2020, 'W-001', 24, '5', 1, '5', '5', '2020-02-21 13:55:21', '2020-02-21 13:55:21'),
(42, '2020-02-21 00:00:00', 2020, 'W-001', 25, '2', 1, '5', '5', '2020-02-21 13:56:05', '2020-02-21 13:56:05'),
(43, '2020-02-21 00:00:00', 2020, 'W-001', 26, '3', 1, '5', '5', '2020-02-21 13:56:14', '2020-02-21 13:56:14'),
(44, '2020-02-21 00:00:00', 2020, 'W-001', 27, '0', 1, '5', '5', '2020-02-21 13:56:21', '2020-02-21 13:56:21'),
(45, '2020-02-28 00:00:00', 2020, 'W-001', 6, '4', 1, '5', '5', '2020-02-28 12:56:09', '2020-02-28 12:56:09'),
(46, '2020-02-28 00:00:00', 2020, 'W-001', 7, '5', 1, '5', '5', '2020-02-28 12:56:22', '2020-02-28 12:56:22'),
(47, '2020-02-28 00:00:00', 2020, 'W-001', 8, '2', 1, '5', '5', '2020-02-28 12:56:36', '2020-02-28 12:56:36'),
(48, '2020-02-28 00:00:00', 2020, 'W-001', 9, '2', 1, '5', '5', '2020-02-28 12:57:07', '2020-02-28 12:57:07'),
(49, '2020-02-28 00:00:00', 2020, 'W-001', 10, '2', 1, '5', '5', '2020-02-28 12:57:17', '2020-02-28 12:57:17'),
(50, '2020-02-28 00:00:00', 2020, 'W-001', 11, '1', 1, '5', '5', '2020-02-28 12:58:50', '2020-02-28 12:58:50'),
(51, '2020-02-28 00:00:00', 2020, 'W-001', 12, '2', 1, '5', '5', '2020-02-28 12:59:04', '2020-02-28 12:59:04'),
(52, '2020-02-28 00:00:00', 2020, 'W-001', 13, '2', 1, '5', '5', '2020-02-28 12:59:10', '2020-02-28 12:59:10'),
(53, '2020-02-28 00:00:00', 2020, 'W-001', 14, '0', 1, '5', '5', '2020-02-28 13:00:44', '2020-02-28 13:00:44'),
(54, '2020-02-28 00:00:00', 2020, 'W-001', 15, '1', 1, '5', '5', '2020-02-28 13:00:53', '2020-02-28 13:00:53'),
(55, '2020-02-28 00:00:00', 2020, 'W-001', 16, '1', 1, '5', '5', '2020-02-28 13:11:36', '2020-02-28 13:11:36'),
(56, '2020-02-28 00:00:00', 2020, 'W-001', 17, '3', 1, '5', '5', '2020-02-28 13:11:43', '2020-02-28 13:11:43'),
(57, '2020-02-28 00:00:00', 2020, 'W-001', 18, '4', 1, '5', '5', '2020-02-28 13:11:55', '2020-02-28 13:11:55'),
(58, '2020-02-28 00:00:00', 2020, 'W-001', 19, '2', 1, '5', '5', '2020-02-28 13:12:04', '2020-02-28 13:12:04'),
(59, '2020-02-28 00:00:00', 2020, 'W-001', 20, '2', 1, '5', '5', '2020-02-28 13:12:12', '2020-02-28 13:12:12'),
(60, '2020-02-28 00:00:00', 2020, 'W-001', 21, '2', 1, '5', '5', '2020-02-28 13:12:16', '2020-02-28 13:12:16'),
(61, '2020-02-28 00:00:00', 2020, 'W-001', 22, '2', 1, '5', '5', '2020-02-28 13:12:25', '2020-02-28 13:12:25'),
(62, '2020-02-28 00:00:00', 2020, 'W-001', 23, '0', 1, '5', '5', '2020-02-28 13:12:32', '2020-02-28 13:12:32'),
(63, '2020-02-28 00:00:00', 2020, 'W-001', 24, '0', 1, '5', '5', '2020-02-28 13:12:37', '2020-02-28 13:12:37'),
(64, '2020-02-28 00:00:00', 2020, 'W-001', 25, '2', 1, '5', '5', '2020-02-28 13:12:46', '2020-02-28 13:12:46'),
(65, '2020-02-28 00:00:00', 2020, 'W-001', 26, '5', 1, '5', '5', '2020-02-28 13:12:58', '2020-02-28 13:12:58'),
(66, '2020-03-06 00:00:00', 2020, 'W-001', 26, '1', 1, '5', '5', '2020-03-06 12:35:00', '2020-03-06 12:35:00'),
(67, '2020-03-06 00:00:00', 2020, 'W-001', 6, '-5', 1, '5', '5', '2020-03-06 12:35:23', '2020-03-06 12:35:23'),
(68, '2020-03-06 00:00:00', 2020, 'W-001', 7, '2', 1, '5', '5', '2020-03-06 12:35:33', '2020-03-06 12:35:33'),
(69, '2020-03-06 00:00:00', 2020, 'W-001', 8, '-5', 1, '5', '5', '2020-03-06 12:35:43', '2020-03-06 12:35:43'),
(70, '2020-03-06 00:00:00', 2020, 'W-001', 9, '1', 1, '5', '5', '2020-03-06 12:35:52', '2020-03-06 12:35:52'),
(71, '2020-03-06 00:00:00', 2020, 'W-001', 10, '1', 1, '5', '5', '2020-03-06 12:35:57', '2020-03-06 12:35:57'),
(72, '2020-03-06 00:00:00', 2020, 'W-001', 11, '0', 1, '5', '5', '2020-03-06 12:36:10', '2020-03-06 12:36:10'),
(73, '2020-03-06 00:00:00', 2020, 'W-001', 12, '3', 1, '5', '5', '2020-03-06 12:36:22', '2020-03-06 12:36:22'),
(74, '2020-03-06 00:00:00', 2020, 'W-001', 13, '1', 1, '5', '5', '2020-03-06 12:36:32', '2020-03-06 12:36:32'),
(75, '2020-03-06 00:00:00', 2020, 'W-001', 14, '0', 1, '5', '5', '2020-03-06 12:36:41', '2020-03-06 12:36:41'),
(76, '2020-03-06 00:00:00', 2020, 'W-001', 15, '0', 1, '5', '5', '2020-03-06 12:36:48', '2020-03-06 12:36:48'),
(77, '2020-03-06 00:00:00', 2020, 'W-001', 16, '3', 1, '5', '5', '2020-03-06 12:36:58', '2020-03-06 12:36:58'),
(78, '2020-03-06 00:00:00', 2020, 'W-001', 17, '5', 1, '5', '5', '2020-03-06 12:37:08', '2020-03-06 12:37:08'),
(79, '2020-03-06 00:00:00', 2020, 'W-001', 18, '4', 1, '5', '5', '2020-03-06 12:37:33', '2020-03-06 12:37:33'),
(80, '2020-03-06 00:00:00', 2020, 'W-001', 19, '3', 1, '5', '5', '2020-03-06 12:37:39', '2020-03-06 12:37:39'),
(81, '2020-03-06 00:00:00', 2020, 'W-001', 20, '3', 1, '5', '5', '2020-03-06 12:37:47', '2020-03-06 12:37:47'),
(82, '2020-03-06 00:00:00', 2020, 'W-001', 21, '2', 1, '5', '5', '2020-03-06 12:37:57', '2020-03-06 12:37:57'),
(83, '2020-03-06 00:00:00', 2020, 'W-001', 22, '-5', 1, '5', '5', '2020-03-06 12:38:09', '2020-03-06 12:38:09'),
(84, '2020-03-06 00:00:00', 2020, 'W-001', 23, '-5', 1, '5', '5', '2020-03-06 12:38:19', '2020-03-06 12:38:19'),
(85, '2020-03-06 00:00:00', 2020, 'W-001', 24, '0', 1, '5', '5', '2020-03-06 12:38:34', '2020-03-06 12:38:34'),
(86, '2020-03-06 00:00:00', 2020, 'W-001', 25, '0', 1, '5', '5', '2020-03-06 12:38:43', '2020-03-06 12:38:43'),
(87, '2020-03-06 00:00:00', 2020, 'W-001', 27, '0', 1, '5', '5', '2020-03-06 12:38:59', '2020-03-06 12:38:59'),
(88, '2020-03-06 00:00:00', 2020, 'W-001', 28, '2', 1, '5', '5', '2020-03-06 12:39:08', '2020-03-06 12:39:08'),
(89, '2020-03-13 00:00:00', 2020, 'W-001', 26, '1', 1, '5', '5', '2020-03-13 12:16:14', '2020-03-13 12:16:14'),
(90, '2020-03-13 00:00:00', 2020, 'W-001', 6, '3', 1, '5', '5', '2020-03-13 12:16:28', '2020-03-13 12:16:28'),
(91, '2020-03-13 00:00:00', 2020, 'W-001', 7, '2', 1, '5', '5', '2020-03-13 12:16:48', '2020-03-13 12:16:48'),
(92, '2020-03-13 00:00:00', 2020, 'W-001', 8, '0', 1, '5', '5', '2020-03-13 12:16:57', '2020-03-13 12:16:57'),
(93, '2020-03-13 00:00:00', 2020, 'W-001', 9, '2', 1, '5', '5', '2020-03-13 12:17:13', '2020-03-13 12:17:13'),
(94, '2020-03-13 00:00:00', 2020, 'W-001', 10, '-5', 1, '5', '5', '2020-03-13 12:17:23', '2020-03-13 12:17:23'),
(95, '2020-03-13 00:00:00', 2020, 'W-001', 11, '2', 1, '5', '5', '2020-03-13 12:17:35', '2020-03-13 12:17:35'),
(96, '2020-03-13 00:00:00', 2020, 'W-001', 12, '3', 1, '5', '5', '2020-03-13 12:17:46', '2020-03-13 12:17:46'),
(97, '2020-03-13 00:00:00', 2020, 'W-001', 13, '2', 1, '5', '5', '2020-03-13 12:17:53', '2020-03-13 12:17:53'),
(98, '2020-03-13 00:00:00', 2020, 'W-001', 14, '0', 1, '5', '5', '2020-03-13 12:18:03', '2020-03-13 12:18:03'),
(99, '2020-03-13 00:00:00', 2020, 'W-001', 15, '2', 1, '5', '5', '2020-03-13 12:18:22', '2020-03-13 12:18:22'),
(100, '2020-03-13 00:00:00', 2020, 'W-001', 16, '5', 1, '5', '5', '2020-03-13 12:18:30', '2020-03-13 12:18:30'),
(101, '2020-03-13 00:00:00', 2020, 'W-001', 17, '0', 1, '5', '5', '2020-03-13 12:18:40', '2020-03-13 12:18:40'),
(102, '2020-03-13 00:00:00', 2020, 'W-001', 18, '4', 1, '5', '5', '2020-03-13 12:18:54', '2020-03-13 12:18:54'),
(103, '2020-03-13 00:00:00', 2020, 'W-001', 19, '2', 1, '5', '5', '2020-03-13 12:19:02', '2020-03-13 12:19:02'),
(104, '2020-03-13 00:00:00', 2020, 'W-001', 20, '2', 1, '5', '5', '2020-03-13 12:19:12', '2020-03-13 12:19:12'),
(105, '2020-03-13 00:00:00', 2020, 'W-001', 21, '0', 1, '5', '5', '2020-03-13 12:19:23', '2020-03-13 12:19:23'),
(106, '2020-03-13 00:00:00', 2020, 'W-001', 22, '3', 1, '5', '5', '2020-03-13 12:19:36', '2020-03-13 12:19:36'),
(107, '2020-03-13 00:00:00', 2020, 'W-001', 23, '1', 1, '5', '5', '2020-03-13 12:19:48', '2020-03-13 12:19:48'),
(108, '2020-03-13 00:00:00', 2020, 'W-001', 24, '0', 1, '5', '5', '2020-03-13 12:19:59', '2020-03-13 12:19:59'),
(109, '2020-03-13 00:00:00', 2020, 'W-001', 25, '1', 1, '5', '5', '2020-03-13 12:20:19', '2020-03-13 12:20:19'),
(110, '2020-03-13 00:00:00', 2020, 'W-001', 28, '2', 1, '5', '5', '2020-03-13 12:20:31', '2020-03-13 12:20:31'),
(111, '2020-06-12 00:00:00', 2020, 'W-001', 26, '2', 1, '5', '5', '2020-06-12 12:36:24', '2020-06-12 12:36:24'),
(112, '2020-06-12 00:00:00', 2020, 'W-001', 6, '3', 1, '5', '5', '2020-06-12 12:36:43', '2020-06-12 12:36:43'),
(113, '2020-06-12 00:00:00', 2020, 'W-001', 7, '3', 1, '5', '5', '2020-06-12 12:36:53', '2020-06-12 12:36:53'),
(114, '2020-06-12 00:00:00', 2020, 'W-001', 8, '3', 1, '5', '5', '2020-06-12 12:37:04', '2020-06-12 12:37:04'),
(115, '2020-06-12 00:00:00', 2020, 'W-001', 9, '-5', 1, '5', '5', '2020-06-12 12:37:19', '2020-06-12 12:37:19'),
(116, '2020-06-12 00:00:00', 2020, 'W-001', 10, '3', 1, '5', '5', '2020-06-12 12:37:30', '2020-06-12 12:37:30'),
(117, '2020-06-12 00:00:00', 2020, 'W-001', 11, '2', 1, '5', '5', '2020-06-12 12:37:40', '2020-06-12 12:37:40'),
(118, '2020-06-12 00:00:00', 2020, 'W-001', 12, '1', 1, '5', '5', '2020-06-12 12:37:55', '2020-06-12 12:37:55'),
(119, '2020-06-12 00:00:00', 2020, 'W-001', 13, '1', 1, '5', '5', '2020-06-12 12:38:08', '2020-06-12 12:38:08'),
(120, '2020-06-12 00:00:00', 2020, 'W-001', 14, '1', 1, '5', '5', '2020-06-12 12:38:17', '2020-06-12 12:38:17'),
(121, '2020-06-12 00:00:00', 2020, 'W-001', 15, '1', 1, '5', '5', '2020-06-12 12:38:25', '2020-06-12 12:38:25'),
(122, '2020-06-12 00:00:00', 2020, 'W-001', 16, '3', 1, '5', '5', '2020-06-12 12:38:35', '2020-06-12 12:38:35'),
(123, '2020-06-12 00:00:00', 2020, 'W-001', 17, '-5', 1, '5', '5', '2020-06-12 12:38:47', '2020-06-12 12:38:47'),
(124, '2020-06-12 00:00:00', 2020, 'W-001', 18, '5', 1, '5', '5', '2020-06-12 12:39:00', '2020-06-12 12:39:00'),
(125, '2020-06-12 00:00:00', 2020, 'W-001', 19, '0', 1, '5', '5', '2020-06-12 12:39:13', '2020-06-12 12:39:13'),
(126, '2020-06-12 00:00:00', 2020, 'W-001', 20, '4', 1, '5', '5', '2020-06-12 12:39:21', '2020-06-12 12:39:21'),
(127, '2020-06-12 00:00:00', 2020, 'W-001', 21, '-5', 1, '5', '5', '2020-06-12 12:39:32', '2020-06-12 12:39:32'),
(128, '2020-06-12 00:00:00', 2020, 'W-001', 22, '5', 1, '5', '5', '2020-06-12 12:39:41', '2020-06-12 12:39:41'),
(129, '2020-06-12 00:00:00', 2020, 'W-001', 23, '-5', 1, '5', '5', '2020-06-12 12:39:51', '2020-06-12 12:39:51'),
(130, '2020-06-12 00:00:00', 2020, 'W-001', 24, '-5', 1, '5', '5', '2020-06-12 12:40:00', '2020-06-12 12:40:00'),
(131, '2020-06-12 00:00:00', 2020, 'W-001', 25, '-5', 1, '5', '5', '2020-06-12 12:40:08', '2020-06-12 12:40:08'),
(132, '2020-06-12 00:00:00', 2020, 'W-001', 28, '-5', 1, '5', '5', '2020-06-12 12:40:16', '2020-06-12 12:40:16'),
(133, '2020-06-12 00:00:00', 2020, 'W-001', 27, '0', 1, '5', '5', '2020-06-12 12:40:34', '2020-06-12 12:40:34'),
(134, '2020-06-19 00:00:00', 2020, 'W-001', 26, '4', 1, '5', '5', '2020-06-19 12:43:48', '2020-06-19 12:43:48'),
(135, '2020-06-19 00:00:00', 2020, 'W-001', 6, '8', 1, '5', '5', '2020-06-19 12:44:09', '2020-06-19 12:44:09'),
(136, '2020-06-19 00:00:00', 2020, 'W-001', 7, '5', 1, '5', '5', '2020-06-19 12:44:24', '2020-06-19 12:44:24'),
(137, '2020-06-19 00:00:00', 2020, 'W-001', 8, '3', 1, '5', '5', '2020-06-19 12:44:31', '2020-06-19 12:44:31'),
(138, '2020-06-19 00:00:00', 2020, 'W-001', 9, '1', 1, '5', '5', '2020-06-19 12:44:40', '2020-06-19 12:44:40'),
(139, '2020-06-19 00:00:00', 2020, 'W-001', 10, '5', 1, '5', '5', '2020-06-19 12:44:50', '2020-06-19 12:44:50'),
(140, '2020-06-19 00:00:00', 2020, 'W-001', 11, '3', 1, '5', '5', '2020-06-19 12:45:16', '2020-06-19 12:45:16'),
(141, '2020-06-19 00:00:00', 2020, 'W-001', 12, '2', 1, '5', '5', '2020-06-19 12:46:12', '2020-06-19 12:46:12'),
(142, '2020-06-19 00:00:00', 2020, 'W-001', 13, '1', 1, '5', '5', '2020-06-19 12:46:42', '2020-06-19 12:46:42'),
(143, '2020-06-19 00:00:00', 2020, 'W-001', 14, '-5', 1, '5', '5', '2020-06-19 12:46:53', '2020-06-19 12:46:53'),
(144, '2020-06-19 00:00:00', 2020, 'W-001', 15, '3', 1, '5', '5', '2020-06-19 12:47:05', '2020-06-19 12:47:05'),
(145, '2020-06-19 00:00:00', 2020, 'W-001', 16, '1', 1, '5', '5', '2020-06-19 12:47:13', '2020-06-19 12:47:13'),
(146, '2020-06-19 00:00:00', 2020, 'W-001', 17, '1', 1, '5', '5', '2020-06-19 12:47:22', '2020-06-19 12:47:22'),
(147, '2020-06-19 00:00:00', 2020, 'W-001', 18, '1', 1, '5', '5', '2020-06-19 12:47:31', '2020-06-19 12:47:31'),
(148, '2020-06-19 00:00:00', 2020, 'W-001', 19, '2', 1, '5', '5', '2020-06-19 12:47:43', '2020-06-19 12:47:43'),
(149, '2020-06-19 00:00:00', 2020, 'W-001', 20, '2', 1, '5', '5', '2020-06-19 12:47:54', '2020-06-19 12:47:54'),
(150, '2020-06-19 00:00:00', 2020, 'W-001', 21, '3', 1, '5', '5', '2020-06-19 12:48:14', '2020-06-19 12:48:14'),
(151, '2020-06-19 00:00:00', 2020, 'W-001', 22, '1', 1, '5', '5', '2020-06-19 12:48:28', '2020-06-19 12:48:28'),
(152, '2020-06-19 00:00:00', 2020, 'W-001', 23, '0', 1, '5', '5', '2020-06-19 12:48:37', '2020-06-19 12:48:37'),
(153, '2020-06-19 00:00:00', 2020, 'W-001', 24, '2', 1, '5', '5', '2020-06-19 12:48:51', '2020-06-19 12:48:51'),
(154, '2020-06-19 00:00:00', 2020, 'W-001', 25, '2', 1, '5', '5', '2020-06-19 12:48:59', '2020-06-19 12:48:59'),
(155, '2020-06-19 00:00:00', 2020, 'W-001', 28, '0', 1, '5', '5', '2020-06-19 12:49:08', '2020-06-19 12:49:08'),
(156, '2020-06-26 00:00:00', 2020, 'W-001', 26, '5', 1, '5', '5', '2020-06-26 12:47:11', '2020-06-26 12:47:11'),
(157, '2020-06-26 00:00:00', 2020, 'W-001', 6, '3', 1, '5', '5', '2020-06-26 12:47:27', '2020-06-26 12:47:27'),
(158, '2020-06-26 00:00:00', 2020, 'W-001', 7, '3', 1, '5', '5', '2020-06-26 12:47:41', '2020-06-26 12:47:41'),
(159, '2020-06-26 00:00:00', 2020, 'W-001', 8, '5', 1, '5', '5', '2020-06-26 12:47:52', '2020-06-26 12:47:52'),
(160, '2020-06-26 00:00:00', 2020, 'W-001', 9, '1', 1, '5', '5', '2020-06-26 12:48:00', '2020-06-26 12:48:00'),
(161, '2020-06-26 00:00:00', 2020, 'W-001', 10, '3', 1, '5', '5', '2020-06-26 12:48:13', '2020-06-26 12:48:13'),
(162, '2020-06-26 00:00:00', 2020, 'W-001', 11, '5', 1, '5', '5', '2020-06-26 12:48:39', '2020-06-26 12:48:39'),
(163, '2020-06-26 00:00:00', 2020, 'W-001', 12, '2', 1, '5', '5', '2020-06-26 12:48:46', '2020-06-26 12:48:46'),
(164, '2020-06-26 00:00:00', 2020, 'W-001', 13, '2', 1, '5', '5', '2020-06-26 12:48:52', '2020-06-26 12:48:52'),
(165, '2020-06-26 00:00:00', 2020, 'W-001', 14, '1', 1, '5', '5', '2020-06-26 12:48:59', '2020-06-26 12:48:59'),
(166, '2020-06-26 00:00:00', 2020, 'W-001', 15, '2', 1, '5', '5', '2020-06-26 12:49:05', '2020-06-26 12:49:05'),
(167, '2020-06-26 00:00:00', 2020, 'W-001', 16, '3', 1, '5', '5', '2020-06-26 12:49:23', '2020-06-26 12:49:23'),
(168, '2020-06-26 00:00:00', 2020, 'W-001', 17, '2', 1, '5', '5', '2020-06-26 12:49:31', '2020-06-26 12:49:31'),
(169, '2020-06-26 00:00:00', 2020, 'W-001', 18, '5', 1, '5', '5', '2020-06-26 12:49:49', '2020-06-26 12:49:49'),
(170, '2020-06-26 00:00:00', 2020, 'W-001', 19, '3', 1, '5', '5', '2020-06-26 12:50:01', '2020-06-26 12:50:01'),
(171, '2020-06-26 00:00:00', 2020, 'W-001', 20, '4', 1, '5', '5', '2020-06-26 12:50:11', '2020-06-26 12:50:11'),
(172, '2020-06-26 00:00:00', 2020, 'W-001', 21, '1', 1, '5', '5', '2020-06-26 12:50:19', '2020-06-26 12:50:19'),
(173, '2020-06-26 00:00:00', 2020, 'W-001', 22, '1', 1, '5', '5', '2020-06-26 12:50:25', '2020-06-26 12:50:25'),
(174, '2020-06-26 00:00:00', 2020, 'W-001', 23, '1', 1, '5', '5', '2020-06-26 12:50:29', '2020-06-26 12:50:29'),
(175, '2020-06-26 00:00:00', 2020, 'W-001', 24, '1', 1, '5', '5', '2020-06-26 12:50:42', '2020-06-26 12:50:42'),
(176, '2020-06-26 00:00:00', 2020, 'W-001', 25, '5', 1, '5', '5', '2020-06-26 12:50:53', '2020-06-26 12:50:53'),
(177, '2020-06-26 00:00:00', 2020, 'W-001', 28, '-5', 1, '5', '5', '2020-06-26 12:51:03', '2020-06-26 12:51:03'),
(178, '2020-07-10 00:00:00', 2020, 'W-001', 26, '2', 1, '5', '5', '2020-07-10 12:50:11', '2020-07-10 12:50:11'),
(179, '2020-07-10 00:00:00', 2020, 'W-001', 7, '6', 1, '5', '5', '2020-07-10 12:51:05', '2020-07-10 12:51:05'),
(180, '2020-07-10 00:00:00', 2020, 'W-001', 8, '2', 1, '5', '5', '2020-07-10 12:51:16', '2020-07-10 12:51:16'),
(181, '2020-07-10 00:00:00', 2020, 'W-001', 9, '1', 1, '5', '5', '2020-07-10 12:51:25', '2020-07-10 12:51:25'),
(182, '2020-07-10 00:00:00', 2020, 'W-001', 10, '1', 1, '5', '5', '2020-07-10 12:51:35', '2020-07-10 12:51:35'),
(183, '2020-07-10 00:00:00', 2020, 'W-001', 11, '2', 1, '5', '5', '2020-07-10 12:51:43', '2020-07-10 12:51:43'),
(184, '2020-07-10 00:00:00', 2020, 'W-001', 12, '2', 1, '5', '5', '2020-07-10 12:52:34', '2020-07-10 12:52:34'),
(185, '2020-07-10 00:00:00', 2020, 'W-001', 13, '1', 1, '5', '5', '2020-07-10 12:52:42', '2020-07-10 12:52:42'),
(186, '2020-07-10 00:00:00', 2020, 'W-001', 14, '3', 1, '5', '5', '2020-07-10 12:52:47', '2020-07-10 12:52:47'),
(187, '2020-07-10 00:00:00', 2020, 'W-001', 15, '1', 1, '5', '5', '2020-07-10 12:52:56', '2020-07-10 12:52:56'),
(188, '2020-07-10 00:00:00', 2020, 'W-001', 16, '1', 1, '5', '5', '2020-07-10 12:53:02', '2020-07-10 12:53:02'),
(189, '2020-07-10 00:00:00', 2020, 'W-001', 17, '4', 1, '5', '5', '2020-07-10 12:53:10', '2020-07-10 12:53:10'),
(190, '2020-07-10 00:00:00', 2020, 'W-001', 18, '5', 1, '5', '5', '2020-07-10 12:53:24', '2020-07-10 12:53:24'),
(191, '2020-07-10 00:00:00', 2020, 'W-001', 19, '5', 1, '5', '5', '2020-07-10 12:53:34', '2020-07-10 12:53:34'),
(192, '2020-07-10 00:00:00', 2020, 'W-001', 20, '6', 1, '5', '5', '2020-07-10 12:53:46', '2020-07-10 12:53:46'),
(193, '2020-07-10 00:00:00', 2020, 'W-001', 21, '1', 1, '5', '5', '2020-07-10 12:54:06', '2020-07-10 12:54:06'),
(194, '2020-07-10 00:00:00', 2020, 'W-001', 22, '1', 1, '5', '5', '2020-07-10 12:54:14', '2020-07-10 12:54:14'),
(195, '2020-07-10 00:00:00', 2020, 'W-001', 25, '1', 1, '5', '5', '2020-07-10 12:54:30', '2020-07-10 12:54:30'),
(196, '2020-07-10 00:00:00', 2020, 'W-001', 28, '1', 1, '5', '5', '2020-07-10 12:54:39', '2020-07-10 12:54:39'),
(197, '2020-07-17 00:00:00', 2020, 'W-001', 26, '1', 1, '5', '5', '2020-07-17 12:48:57', '2020-07-17 12:48:57'),
(198, '2020-07-17 00:00:00', 2020, 'W-001', 6, '5', 1, '5', '5', '2020-07-17 12:49:08', '2020-07-17 12:49:08'),
(199, '2020-07-17 00:00:00', 2020, 'W-001', 7, '7', 1, '5', '5', '2020-07-17 12:49:18', '2020-07-17 12:49:18'),
(200, '2020-07-17 00:00:00', 2020, 'W-001', 8, '2', 1, '5', '5', '2020-07-17 12:49:26', '2020-07-17 12:49:26'),
(201, '2020-07-17 00:00:00', 2020, 'W-001', 9, '3', 1, '5', '5', '2020-07-17 12:49:39', '2020-07-17 12:49:39'),
(202, '2020-07-17 00:00:00', 2020, 'W-001', 10, '1', 1, '5', '5', '2020-07-17 12:49:47', '2020-07-17 12:49:47'),
(203, '2020-07-17 00:00:00', 2020, 'W-001', 11, '2', 1, '5', '5', '2020-07-17 12:49:57', '2020-07-17 12:49:57'),
(204, '2020-07-17 00:00:00', 2020, 'W-001', 12, '1', 1, '5', '5', '2020-07-17 12:50:07', '2020-07-17 12:50:07'),
(205, '2020-07-17 00:00:00', 2020, 'W-001', 13, '3', 1, '5', '5', '2020-07-17 12:50:14', '2020-07-17 12:50:14'),
(206, '2020-07-17 00:00:00', 2020, 'W-001', 14, '5', 1, '5', '5', '2020-07-17 12:50:19', '2020-07-17 12:50:19'),
(207, '2020-07-17 00:00:00', 2020, 'W-001', 15, '3', 1, '5', '5', '2020-07-17 12:50:31', '2020-07-17 12:50:31'),
(208, '2020-07-17 00:00:00', 2020, 'W-001', 16, '1', 1, '5', '5', '2020-07-17 12:50:45', '2020-07-17 12:50:45'),
(209, '2020-07-17 00:00:00', 2020, 'W-001', 17, '1', 1, '5', '5', '2020-07-17 12:50:55', '2020-07-17 12:50:55'),
(210, '2020-07-17 00:00:00', 2020, 'W-001', 18, '3', 1, '5', '5', '2020-07-17 12:51:03', '2020-07-17 12:51:03'),
(211, '2020-07-17 00:00:00', 2020, 'W-001', 19, '2', 1, '5', '5', '2020-07-17 12:51:18', '2020-07-17 12:51:18'),
(212, '2020-07-17 00:00:00', 2020, 'W-001', 20, '3', 1, '5', '5', '2020-07-17 12:51:27', '2020-07-17 12:51:27'),
(213, '2020-07-17 00:00:00', 2020, 'W-001', 21, '1', 1, '5', '5', '2020-07-17 12:51:50', '2020-07-17 12:51:50'),
(214, '2020-07-17 00:00:00', 2020, 'W-001', 22, '1', 1, '5', '5', '2020-07-17 12:52:10', '2020-07-17 12:52:10'),
(215, '2020-07-17 00:00:00', 2020, 'W-001', 25, '1', 1, '5', '5', '2020-07-17 12:52:25', '2020-07-17 12:52:25'),
(216, '2020-07-17 00:00:00', 2020, 'W-001', 28, '3', 1, '5', '5', '2020-07-17 12:52:32', '2020-07-17 12:52:32'),
(217, '2020-07-24 00:00:00', 2020, 'W-001', 26, '2', 1, '5', '5', '2020-07-24 13:32:29', '2020-07-24 13:32:29'),
(218, '2020-07-24 00:00:00', 2020, 'W-001', 6, '2', 1, '5', '5', '2020-07-24 13:32:40', '2020-07-24 13:32:40'),
(219, '2020-07-24 00:00:00', 2020, 'W-001', 7, '2', 1, '5', '5', '2020-07-24 13:33:02', '2020-07-24 13:33:02'),
(220, '2020-07-24 00:00:00', 2020, 'W-001', 8, '1', 1, '5', '5', '2020-07-24 13:33:10', '2020-07-24 13:33:10'),
(221, '2020-07-24 00:00:00', 2020, 'W-001', 10, '1', 1, '5', '5', '2020-07-24 13:34:26', '2020-07-24 13:34:26'),
(222, '2020-07-24 00:00:00', 2020, 'W-001', 11, '2', 1, '5', '5', '2020-07-24 13:34:46', '2020-07-24 13:34:46'),
(223, '2020-07-24 00:00:00', 2020, 'W-001', 12, '3', 1, '5', '5', '2020-07-24 13:34:59', '2020-07-24 13:34:59'),
(224, '2020-07-24 00:00:00', 2020, 'W-001', 13, '3', 1, '5', '5', '2020-07-24 13:35:03', '2020-07-24 13:35:03'),
(225, '2020-07-24 00:00:00', 2020, 'W-001', 14, '1', 1, '5', '5', '2020-07-24 13:35:11', '2020-07-24 13:35:11'),
(226, '2020-07-24 00:00:00', 2020, 'W-001', 15, '3', 1, '5', '5', '2020-07-24 13:35:24', '2020-07-24 13:35:24'),
(227, '2020-07-24 00:00:00', 2020, 'W-001', 16, '1', 1, '5', '5', '2020-07-24 13:35:36', '2020-07-24 13:35:36'),
(228, '2020-07-24 00:00:00', 2020, 'W-001', 17, '3', 1, '5', '5', '2020-07-24 13:35:46', '2020-07-24 13:35:46'),
(229, '2020-07-24 00:00:00', 2020, 'W-001', 18, '5', 1, '5', '5', '2020-07-24 13:36:17', '2020-07-24 13:36:17'),
(230, '2020-07-24 00:00:00', 2020, 'W-001', 19, '2', 1, '5', '5', '2020-07-24 13:36:34', '2020-07-24 13:36:34'),
(231, '2020-07-24 00:00:00', 2020, 'W-001', 20, '5', 1, '5', '5', '2020-07-24 13:37:03', '2020-07-24 13:37:03'),
(232, '2020-07-24 00:00:00', 2020, 'W-001', 21, '1', 1, '5', '5', '2020-07-24 13:37:24', '2020-07-24 13:37:24'),
(233, '2020-07-24 00:00:00', 2020, 'W-001', 22, '1', 1, '5', '5', '2020-07-24 13:37:33', '2020-07-24 13:37:33'),
(234, '2020-07-24 00:00:00', 2020, 'W-001', 23, '1', 1, '5', '5', '2020-07-24 13:37:37', '2020-07-24 13:37:37'),
(235, '2020-07-24 00:00:00', 2020, 'W-001', 25, '2', 1, '5', '5', '2020-07-24 13:37:50', '2020-07-24 13:37:50'),
(236, '2020-07-24 00:00:00', 2020, 'W-001', 28, '-5', 1, '5', '5', '2020-07-24 13:38:05', '2020-07-24 13:38:05'),
(237, '2020-08-14 00:00:00', 2020, 'W-001', 26, '2', 1, '5', '5', '2020-08-14 13:51:48', '2020-08-14 13:51:48'),
(238, '2020-08-14 00:00:00', 2020, 'W-001', 6, '6', 1, '5', '5', '2020-08-14 13:51:58', '2020-08-14 13:51:58'),
(239, '2020-08-14 00:00:00', 2020, 'W-001', 7, '8', 1, '5', '5', '2020-08-14 13:52:10', '2020-08-14 13:52:10'),
(240, '2020-08-14 00:00:00', 2020, 'W-001', 8, '6', 1, '5', '5', '2020-08-14 13:52:19', '2020-08-14 13:52:19'),
(241, '2020-08-14 00:00:00', 2020, 'W-001', 9, '3', 1, '5', '5', '2020-08-14 13:52:31', '2020-08-14 13:52:31'),
(242, '2020-08-14 00:00:00', 2020, 'W-001', 11, '5', 1, '5', '5', '2020-08-14 13:52:46', '2020-08-14 13:52:46'),
(243, '2020-08-14 00:00:00', 2020, 'W-001', 12, '1', 1, '5', '5', '2020-08-14 13:52:56', '2020-08-14 13:52:56'),
(244, '2020-08-14 00:00:00', 2020, 'W-001', 13, '4', 1, '5', '5', '2020-08-14 13:53:05', '2020-08-14 13:53:05'),
(245, '2020-08-14 00:00:00', 2020, 'W-001', 14, '2', 1, '5', '5', '2020-08-14 13:53:13', '2020-08-14 13:53:13'),
(246, '2020-08-14 00:00:00', 2020, 'W-001', 15, '5', 1, '5', '5', '2020-08-14 13:53:20', '2020-08-14 13:53:20'),
(247, '2020-08-14 00:00:00', 2020, 'W-001', 16, '3', 1, '5', '5', '2020-08-14 13:53:30', '2020-08-14 13:53:30'),
(248, '2020-08-14 00:00:00', 2020, 'W-001', 17, '1', 1, '5', '5', '2020-08-14 13:53:39', '2020-08-14 13:53:39'),
(249, '2020-08-14 00:00:00', 2020, 'W-001', 18, '6', 1, '5', '5', '2020-08-14 13:53:50', '2020-08-14 13:53:50'),
(250, '2020-08-14 00:00:00', 2020, 'W-001', 19, '5', 1, '5', '5', '2020-08-14 13:54:02', '2020-08-14 13:54:02'),
(251, '2020-08-14 00:00:00', 2020, 'W-001', 20, '4', 1, '5', '5', '2020-08-14 13:54:16', '2020-08-14 13:54:16'),
(252, '2020-08-14 00:00:00', 2020, 'W-001', 21, '1', 1, '5', '5', '2020-08-14 13:54:24', '2020-08-14 13:54:24'),
(253, '2020-08-14 00:00:00', 2020, 'W-001', 22, '1', 1, '5', '5', '2020-08-14 13:54:27', '2020-08-14 13:54:27'),
(254, '2020-08-14 00:00:00', 2020, 'W-001', 23, '1', 1, '5', '5', '2020-08-14 13:54:35', '2020-08-14 13:54:35'),
(255, '2020-08-14 00:00:00', 2020, 'W-001', 25, '4', 1, '5', '5', '2020-08-14 13:54:46', '2020-08-14 13:54:46'),
(256, '2020-08-14 00:00:00', 2020, 'W-001', 28, '6', 1, '5', '5', '2020-08-14 13:55:02', '2020-08-14 13:55:02'),
(257, '2020-08-14 00:00:00', 2020, 'W-001', 29, '1', 1, '5', '5', '2020-08-14 13:55:10', '2020-08-14 13:55:10'),
(258, '2020-08-28 00:00:00', 2020, 'W-001', 26, '6', 1, '5', '5', '2020-08-28 12:40:51', '2020-08-28 12:40:51'),
(259, '2020-08-28 00:00:00', 2020, 'W-001', 6, '9', 1, '5', '5', '2020-08-28 12:41:02', '2020-08-28 12:41:02'),
(260, '2020-08-28 00:00:00', 2020, 'W-001', 7, '8', 1, '5', '5', '2020-08-28 12:41:11', '2020-08-28 12:41:11'),
(261, '2020-08-28 00:00:00', 2020, 'W-001', 8, '6', 1, '5', '5', '2020-08-28 12:41:22', '2020-08-28 12:41:22'),
(262, '2020-08-28 00:00:00', 2020, 'W-001', 9, '5', 1, '5', '5', '2020-08-28 12:41:31', '2020-08-28 12:41:31'),
(263, '2020-08-28 00:00:00', 2020, 'W-001', 10, '3', 1, '5', '5', '2020-08-28 12:41:38', '2020-08-28 12:41:38'),
(264, '2020-08-28 00:00:00', 2020, 'W-001', 11, '4', 1, '5', '5', '2020-08-28 12:41:50', '2020-08-28 12:41:50'),
(265, '2020-08-28 00:00:00', 2020, 'W-001', 12, '4', 1, '5', '5', '2020-08-28 12:42:04', '2020-08-28 12:42:04'),
(266, '2020-08-28 00:00:00', 2020, 'W-001', 13, '7', 1, '5', '5', '2020-08-28 12:42:11', '2020-08-28 12:42:11'),
(267, '2020-08-28 00:00:00', 2020, 'W-001', 14, '1', 1, '5', '5', '2020-08-28 12:42:19', '2020-08-28 12:42:19'),
(268, '2020-08-28 00:00:00', 2020, 'W-001', 16, '1', 1, '5', '5', '2020-08-28 12:42:37', '2020-08-28 12:42:37'),
(269, '2020-08-28 00:00:00', 2020, 'W-001', 17, '-5', 1, '5', '5', '2020-08-28 12:42:44', '2020-08-28 12:42:44'),
(270, '2020-08-28 00:00:00', 2020, 'W-001', 19, '3', 1, '5', '5', '2020-08-28 12:42:59', '2020-08-28 12:42:59'),
(271, '2020-08-28 00:00:00', 2020, 'W-001', 20, '3', 1, '5', '5', '2020-08-28 12:43:02', '2020-08-28 12:43:02'),
(272, '2020-08-28 00:00:00', 2020, 'W-001', 21, '3', 1, '5', '5', '2020-08-28 12:43:11', '2020-08-28 12:43:11'),
(273, '2020-08-28 00:00:00', 2020, 'W-001', 22, '1', 1, '5', '5', '2020-08-28 12:43:20', '2020-08-28 12:43:20'),
(274, '2020-08-28 00:00:00', 2020, 'W-001', 23, '1', 1, '5', '5', '2020-08-28 12:43:27', '2020-08-28 12:43:27'),
(275, '2020-08-28 00:00:00', 2020, 'W-001', 25, '2', 1, '5', '5', '2020-08-28 12:43:40', '2020-08-28 12:43:40'),
(276, '2020-08-28 00:00:00', 2020, 'W-001', 29, '2', 1, '5', '5', '2020-08-28 12:43:52', '2020-08-28 12:43:52'),
(277, '2020-11-06 00:00:00', 2020, 'W-001', 26, '7', 1, '5', '5', '2020-11-06 12:51:33', '2020-11-06 12:51:33'),
(278, '2020-11-06 00:00:00', 2020, 'W-001', 6, '7', 1, '5', '5', '2020-11-06 12:51:51', '2020-11-06 12:51:51'),
(279, '2020-11-06 00:00:00', 2020, 'W-001', 7, '2', 1, '5', '5', '2020-11-06 12:52:02', '2020-11-06 12:52:02'),
(280, '2020-11-06 00:00:00', 2020, 'W-001', 8, '1', 1, '5', '5', '2020-11-06 12:52:21', '2020-11-06 12:52:21'),
(281, '2020-11-06 00:00:00', 2020, 'W-001', 10, '3', 1, '5', '5', '2020-11-06 12:52:39', '2020-11-06 12:52:39'),
(282, '2020-11-06 00:00:00', 2020, 'W-001', 11, '6', 1, '5', '5', '2020-11-06 12:52:54', '2020-11-06 12:52:54'),
(283, '2020-11-06 00:00:00', 2020, 'W-001', 12, '1', 1, '5', '5', '2020-11-06 12:53:03', '2020-11-06 12:53:03'),
(284, '2020-11-06 00:00:00', 2020, 'W-001', 13, '3', 1, '5', '5', '2020-11-06 12:53:26', '2020-11-06 12:53:26'),
(285, '2020-11-06 00:00:00', 2020, 'W-001', 14, '1', 1, '5', '5', '2020-11-06 12:53:37', '2020-11-06 12:53:37'),
(286, '2020-11-06 00:00:00', 2020, 'W-001', 15, '5', 1, '5', '5', '2020-11-06 12:53:46', '2020-11-06 12:53:46'),
(287, '2020-11-06 00:00:00', 2020, 'W-001', 16, '1', 1, '5', '5', '2020-11-06 12:53:56', '2020-11-06 12:53:56'),
(288, '2020-11-06 00:00:00', 2020, 'W-001', 17, '-5', 1, '5', '5', '2020-11-06 12:54:14', '2020-11-06 12:54:14'),
(289, '2020-11-06 00:00:00', 2020, 'W-001', 18, '4', 1, '5', '5', '2020-11-06 12:54:35', '2020-11-06 12:54:35'),
(290, '2020-11-06 00:00:00', 2020, 'W-001', 19, '2', 1, '5', '5', '2020-11-06 12:54:51', '2020-11-06 12:54:51'),
(291, '2020-11-06 00:00:00', 2020, 'W-001', 20, '7', 1, '5', '5', '2020-11-06 12:55:00', '2020-11-06 12:55:00'),
(292, '2020-11-06 00:00:00', 2020, 'W-001', 21, '5', 1, '5', '5', '2020-11-06 12:55:29', '2020-11-06 12:55:29'),
(293, '2020-11-06 00:00:00', 2020, 'W-001', 22, '2', 1, '5', '5', '2020-11-06 12:55:37', '2020-11-06 12:55:37'),
(294, '2020-11-06 00:00:00', 2020, 'W-001', 23, '3', 1, '5', '5', '2020-11-06 12:55:50', '2020-11-06 12:55:50'),
(295, '2020-11-06 00:00:00', 2020, 'W-001', 25, '5', 1, '5', '5', '2020-11-06 12:56:09', '2020-11-06 12:56:09'),
(296, '2020-11-06 00:00:00', 2020, 'W-001', 29, '1', 1, '5', '5', '2020-11-06 12:56:23', '2020-11-06 12:56:23'),
(297, '2020-11-06 00:00:00', 2020, 'W-001', 30, '1', 1, '5', '5', '2020-11-06 12:56:32', '2020-11-06 12:56:32'),
(298, '2020-11-20 00:00:00', 2020, 'W-001', 26, '5', 1, '5', '5', '2020-11-20 12:42:54', '2020-11-20 12:42:54'),
(299, '2020-11-20 00:00:00', 2020, 'W-001', 6, '7', 1, '5', '5', '2020-11-20 12:43:09', '2020-11-20 12:43:09'),
(300, '2020-11-20 00:00:00', 2020, 'W-001', 7, '6', 1, '5', '5', '2020-11-20 12:43:22', '2020-11-20 12:43:22'),
(301, '2020-11-20 00:00:00', 2020, 'W-001', 8, '6', 1, '5', '5', '2020-11-20 12:43:32', '2020-11-20 12:43:32'),
(302, '2020-11-20 00:00:00', 2020, 'W-001', 9, '1', 1, '5', '5', '2020-11-20 12:44:14', '2020-11-20 12:44:14'),
(303, '2020-11-20 00:00:00', 2020, 'W-001', 10, '3', 1, '5', '5', '2020-11-20 12:44:24', '2020-11-20 12:44:24'),
(304, '2020-11-20 00:00:00', 2020, 'W-001', 11, '6', 1, '5', '5', '2020-11-20 12:44:35', '2020-11-20 12:44:35'),
(305, '2020-11-20 00:00:00', 2020, 'W-001', 12, '1', 1, '5', '5', '2020-11-20 12:44:44', '2020-11-20 12:44:44'),
(306, '2020-11-20 00:00:00', 2020, 'W-001', 13, '3', 1, '5', '5', '2020-11-20 12:45:03', '2020-11-20 12:45:03'),
(307, '2020-11-20 00:00:00', 2020, 'W-001', 14, '1', 1, '5', '5', '2020-11-20 12:45:15', '2020-11-20 12:45:15'),
(308, '2020-11-20 00:00:00', 2020, 'W-001', 15, '1', 1, '5', '5', '2020-11-20 12:45:21', '2020-11-20 12:45:21'),
(309, '2020-11-20 00:00:00', 2020, 'W-001', 16, '4', 1, '5', '5', '2020-11-20 12:45:29', '2020-11-20 12:45:29'),
(310, '2020-11-20 00:00:00', 2020, 'W-001', 17, '1', 1, '5', '5', '2020-11-20 12:45:39', '2020-11-20 12:45:39'),
(311, '2020-11-20 00:00:00', 2020, 'W-001', 18, '7', 1, '5', '5', '2020-11-20 12:45:49', '2020-11-20 12:45:49'),
(312, '2020-11-20 00:00:00', 2020, 'W-001', 19, '1', 1, '5', '5', '2020-11-20 12:46:02', '2020-11-20 12:46:02'),
(313, '2020-11-20 00:00:00', 2020, 'W-001', 20, '1', 1, '5', '5', '2020-11-20 12:46:09', '2020-11-20 12:46:09'),
(314, '2020-11-20 00:00:00', 2020, 'W-001', 21, '1', 1, '5', '5', '2020-11-20 12:46:17', '2020-11-20 12:46:17'),
(315, '2020-11-20 00:00:00', 2020, 'W-001', 22, '1', 1, '5', '5', '2020-11-20 12:46:26', '2020-11-20 12:46:26'),
(316, '2020-11-20 00:00:00', 2020, 'W-001', 23, '5', 1, '5', '5', '2020-11-20 12:46:32', '2020-11-20 12:46:32'),
(317, '2020-11-20 00:00:00', 2020, 'W-001', 25, '3', 1, '5', '5', '2020-11-20 12:46:41', '2020-11-20 12:46:41'),
(318, '2020-11-20 00:00:00', 2020, 'W-001', 29, '1', 1, '5', '5', '2020-11-20 12:46:50', '2020-11-20 12:46:50'),
(319, '2020-11-20 00:00:00', 2020, 'W-001', 30, '1', 1, '5', '5', '2020-11-20 12:46:56', '2020-11-20 12:46:56'),
(320, '2020-12-11 00:00:00', 2020, 'W-001', 26, '6', 1, '5', '5', '2020-12-11 12:45:20', '2020-12-11 12:45:20'),
(321, '2020-12-11 00:00:00', 2020, 'W-001', 7, '-5', 1, '5', '5', '2020-12-11 12:45:40', '2020-12-11 12:45:40'),
(322, '2020-12-11 00:00:00', 2020, 'W-001', 8, '3', 1, '5', '5', '2020-12-11 12:45:55', '2020-12-11 12:45:55'),
(323, '2020-12-11 00:00:00', 2020, 'W-001', 9, '2', 1, '5', '5', '2020-12-11 12:46:04', '2020-12-11 12:46:04'),
(324, '2020-12-11 00:00:00', 2020, 'W-001', 10, '1', 1, '5', '5', '2020-12-11 12:46:24', '2020-12-11 12:46:24'),
(325, '2020-12-11 00:00:00', 2020, 'W-001', 11, '3', 1, '5', '5', '2020-12-11 12:46:35', '2020-12-11 12:46:35'),
(326, '2020-12-11 00:00:00', 2020, 'W-001', 12, '1', 1, '5', '5', '2020-12-11 12:47:05', '2020-12-11 12:47:05'),
(327, '2020-12-11 00:00:00', 2020, 'W-001', 13, '5', 1, '5', '5', '2020-12-11 12:47:12', '2020-12-11 12:47:12'),
(328, '2020-12-11 00:00:00', 2020, 'W-001', 14, '1', 1, '5', '5', '2020-12-11 12:47:21', '2020-12-11 12:47:21'),
(329, '2020-12-11 00:00:00', 2020, 'W-001', 15, '1', 1, '5', '5', '2020-12-11 12:47:34', '2020-12-11 12:47:34'),
(330, '2020-12-11 00:00:00', 2020, 'W-001', 16, '2', 1, '5', '5', '2020-12-11 12:47:41', '2020-12-11 12:47:41'),
(331, '2020-12-11 00:00:00', 2020, 'W-001', 17, '6', 1, '5', '5', '2020-12-11 12:47:57', '2020-12-11 12:47:57'),
(332, '2020-12-11 00:00:00', 2020, 'W-001', 18, '6', 1, '5', '5', '2020-12-11 12:48:03', '2020-12-11 12:48:03'),
(333, '2020-12-11 00:00:00', 2020, 'W-001', 19, '4', 1, '5', '5', '2020-12-11 12:48:13', '2020-12-11 12:48:13'),
(334, '2020-12-11 00:00:00', 2020, 'W-001', 20, '1', 1, '5', '5', '2020-12-11 12:48:21', '2020-12-11 12:48:21'),
(335, '2020-12-11 00:00:00', 2020, 'W-001', 21, '1', 1, '5', '5', '2020-12-11 12:48:27', '2020-12-11 12:48:27'),
(336, '2020-12-11 00:00:00', 2020, 'W-001', 22, '1', 1, '5', '5', '2020-12-11 12:48:33', '2020-12-11 12:48:33'),
(337, '2020-12-11 00:00:00', 2020, 'W-001', 23, '1', 1, '5', '5', '2020-12-11 12:48:38', '2020-12-11 12:48:38'),
(338, '2020-12-11 00:00:00', 2020, 'W-001', 25, '7', 1, '5', '5', '2020-12-11 12:48:51', '2020-12-11 12:48:51'),
(339, '2020-12-11 00:00:00', 2020, 'W-001', 29, '2', 1, '5', '5', '2020-12-11 12:49:01', '2020-12-11 12:49:01'),
(340, '2020-12-11 00:00:00', 2020, 'W-001', 30, '2', 1, '5', '5', '2020-12-11 12:49:05', '2020-12-11 12:49:05'),
(341, '2021-01-08 00:00:00', 2020, 'W-001', 6, '3', 1, '5', '5', '2021-01-08 13:23:14', '2021-01-08 13:23:14'),
(342, '2021-01-08 00:00:00', 2020, 'W-001', 7, '2', 1, '5', '5', '2021-01-08 13:23:25', '2021-01-08 13:23:25'),
(343, '2021-01-08 00:00:00', 2020, 'W-001', 8, '6', 1, '5', '5', '2021-01-08 13:23:35', '2021-01-08 13:23:35'),
(344, '2021-01-08 00:00:00', 2020, 'W-001', 9, '2', 1, '5', '5', '2021-01-08 13:23:46', '2021-01-08 13:23:46'),
(345, '2021-01-08 00:00:00', 2020, 'W-001', 11, '5', 1, '5', '5', '2021-01-08 13:23:58', '2021-01-08 13:23:58'),
(346, '2021-01-08 00:00:00', 2020, 'W-001', 12, '3', 1, '5', '5', '2021-01-08 13:24:08', '2021-01-08 13:24:08'),
(347, '2021-01-08 00:00:00', 2020, 'W-001', 13, '5', 1, '5', '5', '2021-01-08 13:24:20', '2021-01-08 13:24:20'),
(348, '2021-01-08 00:00:00', 2020, 'W-001', 14, '6', 1, '5', '5', '2021-01-08 13:24:30', '2021-01-08 13:24:30'),
(349, '2021-01-08 00:00:00', 2020, 'W-001', 15, '-5', 1, '5', '5', '2021-01-08 13:24:39', '2021-01-08 13:24:39'),
(350, '2021-01-08 00:00:00', 2020, 'W-001', 16, '5', 1, '5', '5', '2021-01-08 13:24:51', '2021-01-08 13:24:51'),
(351, '2021-01-08 00:00:00', 2020, 'W-001', 17, '2', 1, '5', '5', '2021-01-08 13:25:00', '2021-01-08 13:25:00'),
(352, '2021-01-08 00:00:00', 2020, 'W-001', 18, '4', 1, '5', '5', '2021-01-08 13:25:12', '2021-01-08 13:25:12'),
(353, '2021-01-08 00:00:00', 2020, 'W-001', 19, '1', 1, '5', '5', '2021-01-08 13:25:30', '2021-01-08 13:25:30'),
(354, '2021-01-08 00:00:00', 2020, 'W-001', 20, '1', 1, '5', '5', '2021-01-08 13:25:40', '2021-01-08 13:25:40'),
(355, '2021-01-08 00:00:00', 2020, 'W-001', 22, '5', 1, '5', '5', '2021-01-08 13:25:54', '2021-01-08 13:25:54'),
(356, '2021-01-08 00:00:00', 2020, 'W-001', 23, '5', 1, '5', '5', '2021-01-08 13:26:03', '2021-01-08 13:26:03'),
(357, '2021-01-08 00:00:00', 2020, 'W-001', 25, '6', 1, '5', '5', '2021-01-08 13:26:22', '2021-01-08 13:26:22'),
(358, '2021-01-08 00:00:00', 2020, 'W-001', 29, '1', 1, '5', '5', '2021-01-08 13:26:39', '2021-01-08 13:26:39'),
(359, '2021-01-08 00:00:00', 2020, 'W-001', 30, '2', 1, '5', '5', '2021-01-08 13:26:52', '2021-01-08 13:26:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `no_urut` int(11) NOT NULL DEFAULT 0,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `photo` text DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `normalized_username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `mobile_phone` varchar(255) DEFAULT NULL,
  `role` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `no_urut`, `first_name`, `last_name`, `level`, `photo`, `username`, `email`, `normalized_username`, `password`, `birthday`, `gender`, `address`, `mobile_phone`, `role`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(5, 0, 'Admin', 'Workshop', 1, '{\"name\":\"photo_2020-02-15 12.58.35.jpeg\",\"filename\":\"fa62ef5aac3be629717d9baca9801372.jpeg\",\"bucket\":\"storage\",\"mime_type\":\"image\\/jpeg\"}', 'ADM-Workshop', 'admin@email.com', '', 'f3b9172cd4fb9690540ce3e1ecaa55ce', NULL, '1', 'Jl. Mampang Prapatan Raya 108, Rukan Buncit Mas, Blok C3A, Lantai 2-3, Duren Tiga, RT.1/RW.1, Duren Tiga, Pancoran, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12760', '', '[\"5\"]', 1, NULL, '5', '2020-02-13 18:11:28', '2020-02-21 14:00:15'),
(6, 1, 'Apriyanto', 'Pramana Putra', 2, NULL, 'putra', 'putra@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:19:10', '2020-02-13 20:19:10'),
(7, 2, 'Dewa', 'Rajasa Katong', 2, NULL, 'katong', 'katong@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:19:43', '2020-02-13 20:19:43'),
(8, 3, 'Dewi', 'Purwati Ningsih', 2, '', 'dewi', 'dewi@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '2', '', '', '[\"6\",\"7\"]', 1, '5', '5', '2020-02-13 20:20:17', '2020-02-15 16:13:44'),
(9, 4, 'Dio', 'Allan Kosasih', 2, '[]', 'dio', 'dio@email.com', '', '09ea0fd59ec0a15c5756280e65f57975', NULL, '1', '', '', '[\"6\"]', 1, '5', '9', '2020-02-13 20:20:47', '2020-03-13 15:01:23'),
(10, 5, 'Faisal', 'Firaz', 2, '{\"name\":\"photo_2020-02-14 19.12.13.jpeg\",\"filename\":\"1a016ce36f946eb7351fb4e1b92c26a3.jpeg\",\"bucket\":\"storage\",\"mime_type\":\"image\\/jpeg\"}', 'faisal', 'faisal@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:21:13', '2020-02-20 09:35:57'),
(11, 6, 'Farid', 'Hidayat', 2, '[]', 'faridlab', 'e.faridhidayat@gmail.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', 'Pondok Cabe Ilir III, no. 118 rt/rw 04/04', '085770900025', '[\"6\"]', 1, '5', '11', '2020-02-13 20:21:39', '2020-02-20 10:35:26'),
(12, 7, 'Ganesa', 'Dwi Muharso', 1, NULL, 'ganesha', 'ganesa@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:22:36', '2020-02-14 14:01:59'),
(13, 8, 'Habib', 'Udin', 2, '{\"name\":\"habib.jpg\",\"filename\":\"3aa1a25a86e1ef6271565156af3a49de.jpg\",\"bucket\":\"storage\",\"mime_type\":\"image\\/jpeg\"}', 'habib', 'madara.uciha94@gmail.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', '1994-03-10', '1', 'Jl. Mampang Prapatan Raya 108, Rukan Buncit Mas, Blok C3A, Lantai 2-3, Duren Tiga, RT.1/RW.1, Duren Tiga, Pancoran, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12760', '081911030265', '[\"6\"]', 1, '5', NULL, '2020-02-13 20:23:07', '2021-06-11 23:33:49'),
(14, 9, 'Iqlima', 'Iqlima', 2, NULL, 'iqlima', 'iqlima@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '2', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:24:13', '2020-02-13 20:24:13'),
(15, 10, 'Januar', 'Siregar', 1, NULL, 'januar', 'januar@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:24:54', '2020-02-14 14:02:19'),
(16, 11, 'Klara', 'Puspita', 2, NULL, 'klara', 'klara@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '2', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:25:18', '2020-02-14 14:01:15'),
(17, 12, 'Muhammad', 'Nurdin', 2, NULL, 'nurdin', 'nurdin@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:25:46', '2020-02-14 14:00:58'),
(18, 13, 'M Rizky', 'Avesena', 1, '[]', 'avesena', 'rizky@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:26:13', '2021-06-13 11:50:25'),
(19, 14, 'Moch', 'Ichsan', 1, NULL, 'ichsan', 'ichsan@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:26:41', '2020-02-14 09:14:41'),
(20, 15, 'Nur', 'Alam', 2, NULL, 'alam', 'alam@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:27:02', '2020-02-14 09:14:18'),
(21, 16, 'Pendi', 'Setiawan', 2, NULL, 'pendi', 'pendi@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:27:31', '2020-02-14 09:14:03'),
(22, 17, 'Ririn', 'Risda Pangestu', 2, '{\"name\":\"2020-02-20 09.48.11.jpg\",\"filename\":\"2e535e7bdca69f1210c2778f462514a9.jpg\",\"bucket\":\"storage\",\"mime_type\":\"image\\/jpeg\"}', 'ririn', 'ririnrisdap@gmail.com', '', '64cb5e8151784642ff4d51accb740f9b', '1999-03-11', '2', 'bekasi', '00000', '[\"6\"]', 1, '5', '22', '2020-02-13 20:27:59', '2020-02-25 13:30:52'),
(23, 18, 'Rosita', 'Dewi', 2, NULL, 'rosita', 'rosita@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '2', '', '', '[\"6\"]', 1, '5', '5', '2020-02-13 20:28:28', '2020-02-14 09:12:56'),
(24, 19, 'Siti', 'Hardianti', 2, '[]', 'siti', 'siti@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '2', '', '', '[\"6\"]', 0, '5', '5', '2020-02-13 20:28:53', '2021-06-11 22:38:19'),
(25, 20, 'Wahyu', 'Taufik', 2, '{\"name\":\"3C44D56D-BA12-409D-8132-4653C389C7D3.jpeg\",\"filename\":\"6921409c87999afa972df51bf2ca6c10.jpeg\",\"bucket\":\"storage\",\"mime_type\":\"image\\/jpeg\"}', 'wahyu', 'wahyu@email.com', '', 'a3546e1f1049138bd70d5fae87daca70', NULL, '1', '', '', '[\"6\"]', 1, '5', '25', '2020-02-13 20:29:16', '2020-02-27 18:43:28'),
(26, 21, 'Abdul', 'Rasman', 2, NULL, 'adul', 'adl@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '2', '', '', '[\"6\"]', 1, '5', '5', '2020-02-14 14:09:42', '2020-02-14 14:09:42'),
(27, 22, 'Subhan', 'Toba', 1, '[]', 'nyenius', 'nyenius@gmail.com', '', 'c46415b7cbb4bc417ddde091b3189d7c', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-02-19 18:02:15', '2020-02-19 18:02:15'),
(28, 23, 'Bayu', 'Eka', 2, '[]', 'bayu', 'bayu@email.com', '', 'ac83d9a92d17d7a88bedaa2b01b77e30', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-02-27 19:18:22', '2020-02-27 19:18:22'),
(29, 24, 'Doni', 'Rahmat', 2, '[]', 'Doni', 'doni@gmail.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-08-12 09:39:42', '2020-08-12 09:39:42'),
(30, 25, 'Sony', 'Setiawan', 2, '[]', 'Sony', 'sony@email.com', '', '0fcfcbbb420d3b6f81cd5a0d70a96579', NULL, '1', '', '', '[\"6\"]', 1, '5', '5', '2020-11-06 08:26:59', '2020-11-06 08:26:59');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_forgot_password`
--

CREATE TABLE `user_forgot_password` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(100) NOT NULL,
  `expired_date` datetime NOT NULL,
  `status` int(11) DEFAULT NULL,
  `_created_by` varchar(255) DEFAULT NULL,
  `_updated_by` varchar(255) DEFAULT NULL,
  `_created_time` datetime DEFAULT NULL,
  `_updated_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_forgot_password`
--

INSERT INTO `user_forgot_password` (`id`, `user_id`, `token`, `expired_date`, `status`, `_created_by`, `_updated_by`, `_created_time`, `_updated_time`) VALUES
(1, 13, '5e016718e04d296e79e5a00340a2ff19', '2021-06-12 00:01:16', 2, NULL, NULL, '2021-06-11 23:01:16', '2021-06-11 23:33:49');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nama` (`nama`),
  ADD KEY `semester` (`periode`);

--
-- Indeks untuk tabel `absensi_participants`
--
ALTER TABLE `absensi_participants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_absensi` (`id_absensi`),
  ADD KEY `nama` (`nama`),
  ADD KEY `status` (`status_kehadiran`);

--
-- Indeks untuk tabel `audit_trail`
--
ALTER TABLE `audit_trail`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `finance`
--
ALTER TABLE `finance`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kotak_saran`
--
ALTER TABLE `kotak_saran`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pengirim` (`pengirim`);

--
-- Indeks untuk tabel `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nama` (`code`);

--
-- Indeks untuk tabel `previleges`
--
ALTER TABLE `previleges`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `reporting`
--
ALTER TABLE `reporting`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `role_previleges`
--
ALTER TABLE `role_previleges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role` (`role`),
  ADD KEY `role_2` (`role`);

--
-- Indeks untuk tabel `sequence`
--
ALTER TABLE `sequence`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `slogan`
--
ALTER TABLE `slogan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sysparam`
--
ALTER TABLE `sysparam`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `table_jadwal`
--
ALTER TABLE `table_jadwal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nama` (`nama`),
  ADD KEY `semester` (`periode`);

--
-- Indeks untuk tabel `table_keuangan`
--
ALTER TABLE `table_keuangan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `table_nilai`
--
ALTER TABLE `table_nilai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `semester` (`periode`),
  ADD KEY `name` (`name`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `level` (`level`),
  ADD KEY `gender` (`gender`),
  ADD KEY `role` (`role`),
  ADD KEY `role_2` (`role`);

--
-- Indeks untuk tabel `user_forgot_password`
--
ALTER TABLE `user_forgot_password`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `absensi_participants`
--
ALTER TABLE `absensi_participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=547;

--
-- AUTO_INCREMENT untuk tabel `audit_trail`
--
ALTER TABLE `audit_trail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `finance`
--
ALTER TABLE `finance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT untuk tabel `kotak_saran`
--
ALTER TABLE `kotak_saran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `periode`
--
ALTER TABLE `periode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `previleges`
--
ALTER TABLE `previleges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT untuk tabel `reporting`
--
ALTER TABLE `reporting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT untuk tabel `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `role_previleges`
--
ALTER TABLE `role_previleges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=176;

--
-- AUTO_INCREMENT untuk tabel `sequence`
--
ALTER TABLE `sequence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `slogan`
--
ALTER TABLE `slogan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `sysparam`
--
ALTER TABLE `sysparam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `table_jadwal`
--
ALTER TABLE `table_jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `table_keuangan`
--
ALTER TABLE `table_keuangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `table_nilai`
--
ALTER TABLE `table_nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=360;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `user_forgot_password`
--
ALTER TABLE `user_forgot_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `absensi`
--
ALTER TABLE `absensi`
  ADD CONSTRAINT `absensi_ibfk_1` FOREIGN KEY (`nama`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `absensi_participants`
--
ALTER TABLE `absensi_participants`
  ADD CONSTRAINT `absensi_participants_ibfk_1` FOREIGN KEY (`nama`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `absensi_participants_ibfk_2` FOREIGN KEY (`id_absensi`) REFERENCES `absensi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `kotak_saran`
--
ALTER TABLE `kotak_saran`
  ADD CONSTRAINT `kotak_saran_ibfk_1` FOREIGN KEY (`pengirim`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `table_jadwal`
--
ALTER TABLE `table_jadwal`
  ADD CONSTRAINT `table_jadwal_ibfk_1` FOREIGN KEY (`nama`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `table_nilai`
--
ALTER TABLE `table_nilai`
  ADD CONSTRAINT `table_nilai_ibfk_1` FOREIGN KEY (`name`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
