# TUGAS
## master kategori
	-> nama

## tabel bantu (untuk code)
	-> nama tabel
	-> tahun
	-> sequence

## master data barang
	-> code (auto generate format B-tahun-urutan ex: B-2021-0)
	-> kategori (reference ke master kategori)
	-> nama barang
	-> stok barang
	-> harga

## tabel order
	-> code (auto generate format O-tahun-urutan ex: O-2021-0)
	-> nama barang (reference ke master data barang)
	-> qty

	* tampilkan stok tersedia ketika nama barang berubah
	* setelah submit stok barang dikurangi dengan qty yang di input


	