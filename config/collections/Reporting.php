<?php

use \Norm\Schema\NormString;
use \App\Schema\SelectTwoReference;
use \App\Schema\DatePicker;
use \App\Schema\SysparamReference;

return array(
    'schema' => array(
    	'periode' => SelectTwoReference::create('periode')->to('Periode', 'code', 'name')->set('list-column', true)->filter('trim|required')->by(array('status' => 1))->set('searchable', true),
    	'nama' => NormString::create('nama')->set('list-column', true),
    	'total_terlambat' => NormString::create('total_terlambat')->set('list-column', true),
    	'denda_keterlambatan' => NormString::create('denda_keterlambatan')->set('list-column', true),
    	'denda_tidak_siap' => NormString::create('denda_tidak_siap')->set('list-column', true),  
    	'denda_akumulasi_nilai' => NormString::create('denda_akumulasi_nilai')->set('list-column', true),  
    	'total_denda' => NormString::create('total_denda')->set('list-column', true),  
    ),
);