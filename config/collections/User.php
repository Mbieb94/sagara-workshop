<?php

use \App\Schema\NormString;
use \App\Schema\Password;
use \App\Schema\MultiReference;
use \App\Schema\Thumbnail;
use \App\Schema\SelectTwoReference;
use App\Schema\SysparamReference;

$hidden = false;
if (!empty($_SESSION['user']) && $_SESSION['user']['role'][0] != 5){
    $hidden = true;
}

return array(
	'observers' => array(
        'App\\Observer\\UserObserver' => null,
    ),
    'schema' => array(
        'photo'   => Thumbnail::create('photo')->set('list-column', true)->set('bucket','storage'),
        'first_name' => NormString::create('first_name')->filter('trim|required')->set('list-column', true),
        'last_name' => NormString::create('last_name')->filter('trim|required')->set('list-column', true),
        'level' => SysparamReference::create('level')->setGroups('level')->filter('required')->set('list-column', true)->set('hidden', $hidden),
        'email' => NormString::create('email')->filter('trim|required|unique:User,email')->set('list-column', true),
        'username' => NormString::create('username')->filter('trim|required|unique:User,username')->set('list-column', true),
        'password' => Password::create('password')->filter('trim|confirmed|salt'),
        'role'    => MultiReference::create('role')->to('Role', '$id', 'name')->set('hidden', $hidden),
    ),
);