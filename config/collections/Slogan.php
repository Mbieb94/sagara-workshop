<?php

use \Norm\Schema\NormString;

return array(
    'schema' => array(
    	'kata_motovasi' => NormString::create('kata_motovasi')->set('list-column', true)->filter('trim|required'),
    ),
);