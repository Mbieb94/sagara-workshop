<?php

use \Norm\Schema\NormString;
use \App\Schema\SelectTwoReference;
use \App\Schema\DatePicker;
use App\Schema\SysparamReference;

return array(
    'schema' => array(
    	'id_absensi' => SelectTwoReference::create('id_absensi')->to('Absensi', '$id', 'nama')->set('list-column', false)->set('hidden', true),
        'periode' => SelectTwoReference::create('periode')->to('Periode', 'code', 'name')->set('list-column', true)->filter('trim|required')->by(array('status' => 1))->set('searchable', true),
    	'pembicara' => SelectTwoReference::create('pembicara')->to('User', '$id', function($user){
    		return $user['first_name'].' '.$user['last_name'];
    	})->set('list-column', true)->filter('trim|required')->by(array('status' => 1))->set('searchable', true),
    	'nama' => SelectTwoReference::create('nama')->to('User', '$id', function($user){
    		return $user['first_name'].' '.$user['last_name'];
    	})->set('list-column', true)->filter('trim|required')->by(array('status' => 1)),
    	'status_kehadiran' => SysparamReference::create('status_kehadiran', 'Status Kedatangan')->setGroups('status_kehadiran')->set('list-column', true)->set('searchable', true),
    	'keterangan' => NormString::create('keterangan')->set('list-column', true)
    ),
);