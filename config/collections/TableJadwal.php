<?php

use \Norm\Schema\NormString;
use \App\Schema\Password;
use \App\Schema\RoleArray;
use \App\Schema\SelectTwoReference;
use \App\Schema\MultiReference;
use \App\Schema\DatePicker;
use \App\Schema\SysparamReference;

return array(
    'schema' => array(
    	'periode' => SelectTwoReference::create('periode')->to('Periode', 'code', 'name')->set('list-column', true)->filter('trim|required')->by(array('status' => 1))->set('searchable', true),
    	'no_urut' => NormString::create('no_urut', 'Urutan')->set('list-column', true)->set('hidden', true),
    	'nama' => SelectTwoReference::create('nama')->to('User', '$id', function($user){
    		return $user['first_name'].' '.$user['last_name'];
    	})->set('list-column', true)->filter('trim|required')->by(array('status' => 1))->set('searchable', true),
    	'tahun' => SelectTwoReference::create('tahun')->to(array('2020' => '2020', '2021' => '2021'))->set('list-column', true),
    	'judul' => NormString::create('judul')->set('list-column', true)->filter('trim'),
    	'tanggal' => DatePicker::create('tanggal')->setformatdate('dd-mm-yyyy')->set('list-column', true),
    ),
);