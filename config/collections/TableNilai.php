<?php

use Norm\Schema\NormString;
use App\Schema\SelectTwoReference;
use App\Schema\DatePicker;
use App\Schema\SysparamReference;

return array(
    'schema' => array(
        'date' => DatePicker::create('date')->setformatdate('dd/mm/yyyy')->set('list-column', false),
        'periode' => SelectTwoReference::create('periode')->to('Periode', 'code', 'name')->set('list-column', true)->filter('trim|required')->by(array('status' => 1)),
        'year'   => SelectTwoReference::create('year')->to(array('2020' => '2020', '2021' => '2021'))->set('list-column', true),
        'name' => SelectTwoReference::create('name')->to('User', function($user){
        	return $user['first_name'].' '.$user['last_name'];
        })->set('list-column', true)->by(array('status' => 1)),
        'value' => NormString::create('value', 'Score')->set('list-column', true)
    ),
);