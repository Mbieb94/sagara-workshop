<?php

use \Norm\Schema\NormString;
use \App\Schema\SelectTwoReference;
use \App\Schema\DatePicker;
use \App\Schema\SysparamReference;

return array(
    'schema' => array(
    	'periode' => SelectTwoReference::create('periode')->to('Periode', 'code', 'name')->set('list-column', true)->filter('trim|required')->by(array('status' => 1)),
    	'date' => DatePicker::create('date')->filter('required')->set('list-column', true),
    	'type' => SysparamReference::create('type')->setGroups('finance')->filter('required')->set('list-column', true),
    	'amount' => NormString::create('amount')->filter('required')->set('list-column', true),
    	'description' => NormString::create('description')->filter('required')->set('list-column', true)
    ),
);