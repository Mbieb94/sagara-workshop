<?php

use \Norm\Schema\NormString;
use \App\Schema\SelectTwoReference;
use \App\Schema\DatePicker;
use App\Schema\SysparamReference;

return array(
	// 'observers' => array(
 //        'App\\Observer\\AbsensiObserver' => null,
 //    ),
    'schema' => array(
    	'nama' => SelectTwoReference::create('nama', 'Pembicara')->to('User', '$id', function($user){
    		return $user['first_name'].' '.$user['last_name'];
    	})->set('list-column', true)->filter('trim|required')->by(array('status' => 1)),
    	'tanggal' => DatePicker::create('tanggal')->setformatdate('dd-mm-yyyy')->set('list-column', true)->filter('trim|required'),
    	'periode' => SelectTwoReference::create('periode')->to('Periode', 'code', 'name')->set('list-column', true)->filter('trim|required')->by(array('status' => 1)),
    ),
);