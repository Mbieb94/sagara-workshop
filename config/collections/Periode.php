<?php

use Norm\Schema\NormString;
use \App\Schema\SysparamReference;

return array(
	'observers' => array(
        'App\\Observer\\SequenceObserver' => null,
    ),
    'schema' => array(
        'code' => NormString::create('code')->set('list-column', true)->set('hidden', true),
        'name' => NormString::create('name')->set('list-column', true),
        'workshop_status' => SysparamReference::create('workshop_status')->setGroups('workshop_status')->set('list-column', true)
    ),
);