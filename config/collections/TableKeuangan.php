<?php

use \Norm\Schema\NormString;
use \App\Schema\Password;
use \App\Schema\RoleArray;
use \App\Schema\SelectTwoReference;
use \App\Schema\MultiReference;
use \App\Schema\DatePicker;
use \App\Schema\SysparamReference;

return array(
    'schema' => array(
        'date' => DatePicker::create('date')->setformatdate('dd/mm/yyyy')->filter('trim')->set('list-column', true),
        'type' => SysparamReference::create('type', 'Pemasukan / Pengeluaran')->setGroups('keuangan')->set('list-column', true),
        'jumlah' => NormString::create('jumlah')->filter('trim|required')->set('list-column', true),
        'keterangan' => NormString::create('keterangan')->set('list-column', true),
        'saldo' => NormString::create('saldo')->set('list-column', true),
    ),
);