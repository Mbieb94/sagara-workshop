<?php

use \Norm\Schema\NormString;
use \Norm\Schema\NormText;
use \App\Schema\Password;
use \App\Schema\RoleArray;
use \App\Schema\SelectTwoReference;
use \App\Schema\MultiReference;
use \App\Schema\DatePicker;
use \App\Schema\SysparamReference;
use \App\Schema\Editor;

return array(
	'observers' => array(
        'App\\Observer\\KotakSaranObserver' => null,
    ),
    'schema' => array(
    	'pengirim' => SelectTwoReference::create('pengirim')->to('User', '$id', function($user){
    		return $user['first_name'].' '.$user['last_name'];
    	})->set('list-column', true)->set('hidden', true),
    	'penerima' => SelectTwoReference::create('penerima')->to('User', '$id', function($user){
    		return $user['first_name'].' '.$user['last_name'];
    	})->filter('required')->set('list-column', true),
    	'pesan' => NormText::create('pesan')->filter('required')->set('list-column', true)
    ),
);