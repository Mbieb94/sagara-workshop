var api = window.api = {
    hit_api: async function(uri, data = {}, type = 'POST', header = {}, auth = true) {
        if (type === 'POST') {
            data = JSON.stringify(data);
        }

        try {
            var result = await $.ajax({
                type: type,
                crossDomain: true,
                url: window.base_url + uri,
                data: data,
                headers: header,
            });
            return result;
        } catch (error) {
            throw error;
        }
    },
    formdata_api: async function(uri, data, type = 'POST') {
        try {

            var result = $.ajax({
                type: type,
                url: window.base_url + uri,
                data: data,
                cache: false,
                contentType: false,
                processData: false,
            });
            return result;
        } catch (error) {
            console.log(error);
            throw error;
        }

    },
    serializeObject: function(temppost) {

        var post = {};

        // change serizlizearray to object
        for (var i = 0; i < temppost.length; i++) {
            if (temppost[i].value !== '') {
                // if (!post[temppost[i].name]) {
                post[temppost[i].name] = temppost[i].value;
                // } else {
                //     if (Array.isArray(post[temppost[i].name])) {
                //         post[temppost[i].name].push(temppost[i].value);
                //     } else {
                //         var temp = post[temppost[i].name];
                //         post[temppost[i].name] = [];
                //         post[temppost[i].name].push(temp);
                //         post[temppost[i].name].push(temppost[i].value);
                //     }
                // }
            }
        }

        return post;

    }
}


var s4 = window.s4 = function() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
};

var guid = window.guid = function() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};